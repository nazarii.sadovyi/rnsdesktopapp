﻿using Prism.Ioc;
using RnsApp.Configuration;
using RnsApp.Core.Services;
using RnsApp.DAL;
using RnsApp.ServerNotification;
using RnsApp.WindowsShellCommand;

namespace RnsApp.Core
{
    public static class CoreDependency
    {
        public static void Register(IContainerRegistry container)
        {
            DalRegister.Register(container);
            ConfigurationDependency.Register(container);
            RegisterServerNotificationDependency.Register(container);

            container.Register<IAppSettingService, AppSettingService>();
            container.Register<IRegisterUserService, RegisterUserService>();
            container.Register<IAppSettingUpdateService, AppSettingService>();
            container.Register<IAuthenticationService, AuthenticationService>();
            container.Register<IUserFileManagerService, UserFileManagerService>();
            container.Register<ICommandLineArgsService, CommandLineArgsService>();
            container.Register<ILocalFileManagerService, LocalFileManagerService>();
            container.Register<IStorageHostingManagerService, StorageHostingManagerService>();
            container.RegisterSingleton<IWindowsShellCommandService, WindowsShellCommandService>();
        }
    }
}
