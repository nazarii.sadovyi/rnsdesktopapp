﻿using RnsApp.Common.Enums;
using System;
using System.Linq;

namespace RnsApp.Core.Services
{
    public interface ICommandLineArgsService
    {
        bool ArgumentExist(ComandLineArgKind lineArgKind);
        bool TryGetArgumentValue(ComandLineArgKind lineArgKind, out string value);
    }

    public class CommandLineArgsService: ICommandLineArgsService
    {
        private readonly string[] _commandLineArgs;

        public CommandLineArgsService()
        {
            _commandLineArgs = Environment.GetCommandLineArgs();
        }

        public bool ArgumentExist(ComandLineArgKind lineArgKind)
        {
            switch (lineArgKind)
            {
                case ComandLineArgKind.ApplicationExePath:
                    return _commandLineArgs.Length == 1;
                case ComandLineArgKind.FileToUploadPath:
                    return _commandLineArgs.Length == 2;
                default:
                    throw new NotImplementedException(lineArgKind.ToString());
            }
        }

        public bool TryGetArgumentValue(ComandLineArgKind lineArgKind, out string value)
        {
            value = string.Empty;
            if (!ArgumentExist(lineArgKind))
            {
                return false;
            }

            switch (lineArgKind)
            {
                case ComandLineArgKind.ApplicationExePath:
                    value = _commandLineArgs.ElementAt(0);
                    break;
                case ComandLineArgKind.FileToUploadPath:
                    value = _commandLineArgs.ElementAt(1);
                    break;
                default:
                    throw new NotImplementedException(lineArgKind.ToString());
            }

            return true;
        }
    }
}
