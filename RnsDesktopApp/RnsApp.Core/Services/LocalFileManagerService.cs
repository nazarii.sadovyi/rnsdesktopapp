﻿using RnsApp.Common.Enums;
using RnsApp.Common.EventArguments;
using System;
using System.Diagnostics;
using System.IO;
using Unity;
using RnsApp.Common.Helpers;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace RnsApp.Core.Services
{
    public interface ILocalFileManagerService
    {
        /// <summary>
        /// Return empty string if directory was not selected
        /// </summary>
        string SelectDirectory();
       
        void ShowFileInExploer(string fileName);

        string ChooseFile(string fileFilter = default);

        event EventHandler<FileWatchEventArgs> FileDirectoryWatcher;

        void InitLocalFileWatcher();

        void RemoveFile(string filePath);
    }
    public class LocalFileManagerService: ILocalFileManagerService
    {
        public event EventHandler<FileWatchEventArgs> FileDirectoryWatcher;

        #region Dependency
        [Dependency]
        public IAppSettingService AppSettingService { get; set; }
        #endregion

        public void InitLocalFileWatcher()
        {
            var _fileSystemWatcher = new FileSystemWatcher(AppSettingService.FilesDirectory)
            {
                Filter = "*.*"
            };
            _fileSystemWatcher.Changed += new FileSystemEventHandler(OnChanged);
            _fileSystemWatcher.Deleted += new FileSystemEventHandler(OnChanged);
            _fileSystemWatcher.Created += new FileSystemEventHandler(OnChanged);
            _fileSystemWatcher.Renamed += new RenamedEventHandler(OnRenamed);
            _fileSystemWatcher.EnableRaisingEvents = true;
        }

        private void OnRenamed(object sender, RenamedEventArgs e)
        {
            var args = new FileWatchEventArgs()
            {
                ChangeType = FileChangeType.Renamed,
                FileNames = new string[] { e.OldName, e.Name }
            };
            FileDirectoryWatcher?.Invoke(this, args);
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            var args = new FileWatchEventArgs()
            {
                ChangeType = e.ChangeType.ToFileChangeType(),
                FileNames = new string[] { e.Name }
            };
            FileDirectoryWatcher?.Invoke(this, args);
        }

        public string SelectDirectory()
        {
            var dlg = new CommonOpenFileDialog
            {
                Title = "New files location directory",
                IsFolderPicker = true,
                InitialDirectory = AppSettingService.FilesDirectory,

                AddToMostRecentlyUsedList = false,
                AllowNonFileSystemItems = false,
                EnsureFileExists = true,
                EnsurePathExists = true,
                EnsureReadOnly = false,
                EnsureValidNames = true,
                Multiselect = false,
                ShowPlacesList = true
            };

            var folderPath = string.Empty;
            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                folderPath = dlg.FileName;
            }

            return folderPath;
        }

        public string ChooseFile(string fileFilter = "")
        {
            var dlg = new CommonOpenFileDialog
            {
                Title = "Select file to upload",
                IsFolderPicker = false,
                InitialDirectory = AppSettingService.FilesDirectory,

                AddToMostRecentlyUsedList = false,
                AllowNonFileSystemItems = false,
                EnsureFileExists = true,
                EnsurePathExists = true,
                EnsureReadOnly = false,
                EnsureValidNames = true,
                Multiselect = false,
                ShowPlacesList = true
            };

            var filePath = string.Empty;
            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                filePath = dlg.FileName;
            }

            return filePath;
        }

        public void ShowFileInExploer(string fileName)
        {
            var fullFileLocation = Path.Combine(AppSettingService.FilesDirectory, fileName);
            if (File.Exists(fullFileLocation))
            {
                Process.Start("explorer.exe", $"/select, \"{fullFileLocation}\"");
            }
        }

        public void RemoveFile(string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }
    }
}
