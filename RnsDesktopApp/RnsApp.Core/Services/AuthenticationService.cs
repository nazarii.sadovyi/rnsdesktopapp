﻿using RnsApp.Common.Entities;
using RnsApp.Configuration;
using RnsApp.Configuration.Entities;
using RnsApp.DAL.WorkerInterfaces;
using System.Threading.Tasks;
using Unity;

namespace RnsApp.Core.Services
{
    public interface IAuthenticationService
    {
        Task<bool> TryToLogin();
        Task<LoginResult> TryToLogin(string email, string password, bool rememberMe = true);
        Task Logout();
    }
    
    public class AuthenticationService: IAuthenticationService
    {
        #region Dependency
        [Dependency]
        public IUserCredentialManager CredentialManager { get; set; }
        [Dependency]
        public IAuthorizeWorker AuthorizeWorker { get; set; }
        [Dependency]
        public IAppSettingUpdateService AppSettingUpdateService { get; set; }

        public async Task Logout()
        {
            await AuthorizeWorker.Logout();
            CredentialManager.Delete();
        }
        #endregion

        public async Task<bool> TryToLogin()
        {
            if (CredentialManager.CredentialExist())
            {
                var userCredential = CredentialManager.GetCredential();
                var loginResult = await AuthorizeWorker.TryToLogin(userCredential.Email, userCredential.Password);
                return loginResult.IsSuccess;
            }
            return false;
        }

        public async Task<LoginResult> TryToLogin(string email, string password, bool rememberMe = true)
        {
            var loginResult = await AuthorizeWorker.TryToLogin(email, password);
            if (loginResult.IsSuccess)
            {
                CredentialManager.SaveCredential(new UserCredential(email, password));
                AppSettingUpdateService.UpdateCurrentUserEmail(email);
            }
            return loginResult;
        }
    }
}
