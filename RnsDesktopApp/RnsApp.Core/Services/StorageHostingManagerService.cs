﻿using RnsApp.Common.Domain;
using RnsApp.Common.Entities;
using RnsApp.Common.Enums;
using RnsApp.Common.Helpers;
using RnsApp.Common.Models;
using RnsApp.DAL.WorkerInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity;

namespace RnsApp.Core.Services
{
    public interface IStorageHostingManagerService
    {
        Task<IEnumerable<StorageHosting>> GetAll(Hosting? storageHostingType = null);
        Task Remove(Hosting storageHostingType, Guid connectionId);
        Task<AddCredentialResult> AddNewCredential(MegaCredential megaCredential);
        Task<AddCredentialResult> AddNewCredential(DropboxCredential dropboxCredential);
        Task<AddCredentialResult> AddNewCredential(GoogleDriveCredentialModel googleDriveCredential);
    }
    public class StorageHostingManagerService: IStorageHostingManagerService
    {
        #region Dependency
        [Dependency]
        public IMegaCredentialRepository MegaCredentialRepository { get; set; }

        [Dependency]
        public IDropboxCredentialRepository DropboxCredentialRepository { get; set; }

        [Dependency]
        public IGoogleDriveCredentialRepository GoogleDriveCredentialRepository { get; set; }
        #endregion

        public async Task<IEnumerable<StorageHosting>> GetAll(Hosting? storageHostingType = null)
        {
            var result = new List<StorageHosting>();
            switch (storageHostingType)
            {
                case Hosting.Mega:
                    result.AddRange((await MegaCredentialRepository.GetAll())
                        .Where(x => x.Type == CredentialType.Custom)
                        .Select(x => x.ToDomainStorageHosting(Hosting.Mega)));
                    break;
                case Hosting.GoogleDrive:
                    result.AddRange((await GoogleDriveCredentialRepository.GetAll())
                        .Where(x => x.Type == CredentialType.Custom)
                        .Select(x => x.ToDomainStorageHosting(Hosting.GoogleDrive)));
                    break;
                case Hosting.Dropbox:
                    result.AddRange((await DropboxCredentialRepository.GetAll())
                        .Where(x => x.Type == CredentialType.Custom)
                        .Select(x => x.ToDomainStorageHosting(Hosting.Dropbox)));
                    break;
                case null:
                    result.AddRange((await MegaCredentialRepository.GetAll())
                        .Where(x => x.Type == CredentialType.Custom)
                        .Select(x => x.ToDomainStorageHosting(Hosting.Mega)));
                    result.AddRange((await GoogleDriveCredentialRepository.GetAll())
                        .Where(x => x.Type == CredentialType.Custom)
                        .Select(x => x.ToDomainStorageHosting(Hosting.GoogleDrive)));
                    result.AddRange((await DropboxCredentialRepository.GetAll())
                        .Where(x => x.Type == CredentialType.Custom)
                        .Select(x => x.ToDomainStorageHosting(Hosting.Dropbox)));
                    break;
                default:
                    throw new NotImplementedException(storageHostingType.ToString());
            }
            return result;
        }

        public async Task Remove(Hosting storageHostingType, Guid connectionId)
        {
            switch (storageHostingType)
            {
                case Hosting.Mega:
                    await MegaCredentialRepository.Remove(connectionId);
                    break;
                case Hosting.GoogleDrive:
                    await GoogleDriveCredentialRepository.Remove(connectionId);
                    break;
                case Hosting.Dropbox:
                    await DropboxCredentialRepository.Remove(connectionId);
                    break;
                default:
                    throw new NotImplementedException(storageHostingType.ToString());
            }
        }

        public async Task<AddCredentialResult> AddNewCredential(MegaCredential megaCredential)
        {
            return await MegaCredentialRepository.Add(megaCredential);
        }

        public async Task<AddCredentialResult> AddNewCredential(DropboxCredential dropboxCredential)
        {
            return await DropboxCredentialRepository.Add(dropboxCredential);
        }

        public async Task<AddCredentialResult> AddNewCredential(GoogleDriveCredentialModel googleDriveCredential)
        {
            return await GoogleDriveCredentialRepository.Add(googleDriveCredential);
        }
    }
}
