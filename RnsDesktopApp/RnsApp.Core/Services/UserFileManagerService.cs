﻿using RnsApp.Common.Domain;
using RnsApp.Common.Entities;
using RnsApp.Common.Enums;
using RnsApp.Common.Models;
using RnsApp.DAL.WorkerInterfaces;
using RnsApp.ServerNotification;
using RnsApp.ServerNotification.EventArgsEntities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Unity;

namespace RnsApp.Core.Services
{
    public interface IUserFileManagerService
    {
        Task<IEnumerable<UserFile>> GetAll(bool checkFileLocal = true);
        Task<UserFile> GetById(Guid fileId);
        Task<DownloadFileResult> DownloadFile(Guid fileId, string fileName, DownloadFileProgress progresses = null);
        Task Remove(Guid fileId);
        Task<UploadFileResult> UploadFile(UploadFileModel uploadFileModel, UploadFileProgresses progresses = null);
    }
    public class UserFileManagerService: IUserFileManagerService
    {
        #region Dependency
        [Dependency]
        public IUserFileWorker UserFileWorker { get; set; }
        [Dependency]
        public IAppSettingService AppSettingService { get; set; }
        [Dependency]
        public IDownloadFileNotification DownloadFileNotification { get; set; }
        [Dependency]
        public IUploadFileNotification UploadFileNotification { get; set; }
        #endregion

        public async Task<IEnumerable<UserFile>> GetAll(bool checkFileLocal = true)
        {
            var userFiles = new Collection<UserFile>();
            
            foreach (var userFile in await UserFileWorker.GetAll())
            {
                if (checkFileLocal)
                {
                    var localFilePath = Path.Combine(AppSettingService.FilesDirectory, userFile.FullName);
                    if (File.Exists(localFilePath))
                    {
                        userFile.IsLocal = true;
                    }
                }
                userFiles.Add(userFile);
            } 

            return userFiles;
        }

        public async Task<UserFile> GetById(Guid fileId)
        {
            return (await UserFileWorker.GetAll()).FirstOrDefault(x => x.FileId == fileId);
        }
        
        public async Task<DownloadFileResult> DownloadFile(Guid fileId, string fileName, DownloadFileProgress progresses = null)
        {
            if (progresses != null)
            {
                #region Download progress
                var downloadArgs = new DownloadProgressArgs();
                progresses.DownloadProgress?.Report(downloadArgs);

                void handler(object sender, int e)
                {
                    if (e == 100)
                    {
                        UserFileWorker.DownloadFileProccessEvent -= handler;
                    }
                    downloadArgs.Value = e;
                    progresses.DownloadProgress?.Report(downloadArgs);
                }

                UserFileWorker.DownloadFileProccessEvent += handler;
                #endregion

                #region Convert progress
                var convertArgs = new BaseProgressArgs();
                progresses.ConvertProgress?.Report(convertArgs);

                void convertHandler(object sender, ConvertFileEventArgs e)
                {
                    if (e.SpentTime != TimeSpan.Zero)
                    {
                        convertArgs.Message = $"Spent time: {e.SpentTime.Minutes} minutes, {e.SpentTime.Seconds} seconds";
                        DownloadFileNotification.ConvertFromRNSProgressEvent -= convertHandler;
                    }
                    convertArgs.Value = e.ProgressPercent;
                    progresses.ConvertProgress?.Report(convertArgs);
                }

                DownloadFileNotification.ConvertFromRNSProgressEvent += convertHandler;
                #endregion

                #region Storage hosting progress
                void storageHostingHandler(object sender, DownloadFilePartsEventArgs e)
                {
                    var hostingArgs = new StorageHostingArgs
                    {
                        Value = e.ProgressPercent,
                        Id = e.FilePartId,
                        Message = $"Spent time: {e.SpentTime.Minutes} minutes, {e.SpentTime.Seconds} seconds"
                    };
                    progresses.StoarageHostingProgress?.Report(hostingArgs);
                }

                DownloadFileNotification.FilePartsProgresEvent += storageHostingHandler;
                #endregion

                #region Storage hosting crash progress
                DownloadFileNotification.PartDownloadFailedEvent += (sender, args) =>
                {
                    var crashArgs = new StorageHostingArgs()
                    {
                        Id = args.FilePartId,
                        State = ProgressState.Failed,
                        Message = args.ErrorMessage
                    };
                    progresses.StoarageHostingProgress?.Report(crashArgs);
                };
                #endregion
            }
            var result = new DownloadFileResult();
            try
            {
                var totalStopwatch = new Stopwatch();
                totalStopwatch.Start();
                var newFilePath = Path.Combine(AppSettingService.FilesDirectory, fileName);
                bool downloadResult;
                using (var resultFileStream = new FileStream(newFilePath, FileMode.OpenOrCreate))
                {
                    File.SetAttributes(newFilePath, FileAttributes.Hidden);
                    downloadResult = await UserFileWorker.Download(fileId, resultFileStream);
                }

                totalStopwatch.Stop();
                if (downloadResult)
                {
                    File.SetAttributes(newFilePath, FileAttributes.Normal);
                    result.Success = true;
                    result.TotalSpentTime = totalStopwatch.Elapsed;
                    return result;
                }

                File.Delete(newFilePath);
                result.Success = false;
                result.ErrorMessage = "Can't download file, too many file parts are damaged.";
            }
            catch (Exception e)
            {
                result.Success = false;
                result.ErrorMessage = e.Message;
            }
            return result;
        }

        public async Task Remove(Guid fileId)
        {
            await UserFileWorker.Delete(fileId);
        }

        public async Task<UploadFileResult> UploadFile(UploadFileModel uploadFileModel, UploadFileProgresses progresses = null)
        {
            var result = new UploadFileResult();
            var uploadProgress = new Progress<int>();
            if (progresses != null)
            {
                #region Upload file
                var uploadArgs = new UploadProgressArgs();
                progresses.UploadProgress?.Report(uploadArgs);

                uploadProgress.ProgressChanged += (sender, e) => 
                {
                    uploadArgs.Value = e;
                    progresses.UploadProgress?.Report(uploadArgs);
                };
                #endregion

                #region File part upload progress
                var storageHostingArgs = new StorageHostingArgs();
                progresses.StorageHostingProgress?.Report(storageHostingArgs);
                void FilePartHandler(object sender, UploadFilePartsEventArgs e)
                {
                    storageHostingArgs.OrderNumber = e.FilePartOrder;
                    storageHostingArgs.Value = e.ProgressPercent;
                    if (e.SpentTime != TimeSpan.Zero)
                    {
                        storageHostingArgs.Message = $"Spent time: {e.SpentTime.Minutes} minutes, {e.SpentTime.Seconds} seconds";
                    }
                    progresses.StorageHostingProgress?.Report(storageHostingArgs);
                }
                UploadFileNotification.FilePartsProgresEvent += FilePartHandler;
                #endregion

                #region Convert progress
                var convertProgressArgs = new BaseProgressArgs();
                progresses.ConvertProgress?.Report(convertProgressArgs);
                void ConvertHandler(object sender, ConvertFileEventArgs e)
                {
                    convertProgressArgs.Value = e.ProgressPercent;
                    if (e.SpentTime != TimeSpan.Zero)
                    {
                        convertProgressArgs.Message = $"Spent time: {e.SpentTime.Minutes} minutes, {e.SpentTime.Seconds} seconds";
                    }
                    progresses.ConvertProgress?.Report(convertProgressArgs);
                }
                UploadFileNotification.ConvertFromDecimalProgressEvent += ConvertHandler;
                #endregion

                #region File part upload crash
                UploadFileNotification.PartUploadFailedEvent += (sender, args) =>
                {
                    var crashArgs = new StorageHostingArgs()
                    {
                        OrderNumber = args.PartOrder,
                        State = ProgressState.Failed,
                        Message = args.ErrorMessage
                    };
                    progresses.StorageHostingProgress?.Report(crashArgs);
                };
                #endregion
            }

            try
            {
                var uploadStopwatcher = new Stopwatch();
                uploadStopwatcher.Start();
                
                await UserFileWorker.Upload(uploadFileModel, uploadProgress);
                
                uploadStopwatcher.Stop();

                result.SpentTime = uploadStopwatcher.Elapsed;
                result.Success = true;
            }
            catch (Exception e)
            {
                result.ErrorMessage = e.Message;
            }

            return result;
        }
    }
}
