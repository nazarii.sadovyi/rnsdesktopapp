﻿using RnsApp.Common.Entities;
using RnsApp.DAL.WorkerInterfaces;
using System.Threading.Tasks;
using Unity;

namespace RnsApp.Core.Services
{
    public interface IRegisterUserService
    {
        Task<RegisterUserResult> Register(RegisterUserModel model);
    }
    public class RegisterUserService : IRegisterUserService
    {
        [Dependency]
        public IAuthorizeWorker AuthorizeWorker { get; set; }

        public async Task<RegisterUserResult> Register(RegisterUserModel model)
        {
            return await AuthorizeWorker.TryToRegister(
                model.FirstName,
                model.SecondName,
                model.Email,
                model.Password);
        }
    }
}
