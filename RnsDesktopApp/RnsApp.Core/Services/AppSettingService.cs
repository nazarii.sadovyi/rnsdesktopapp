﻿using RnsApp.Configuration;
using RnsApp.DAL;
using System;
using System.IO;
using Unity;

namespace RnsApp.Core.Services
{
    public interface IAppSettingService
    {
        string ServerUrl { get; }
        string FilesDirectory { get; }
        string CurrentUserLogin { get; }
        bool IsContextMenuUploadCommandInit { get; set; }
    }

    public interface IAppSettingUpdateService: IAppSettingService
    {
        bool UpdateServerUrl(string url, ref string errorMessage);
        bool UpdateFilesDirectory(string directoryPath, ref string errorMessage);
        void UpdateCurrentUserEmail(string email);
    }

    public class AppSettingService: IAppSettingUpdateService
    {
        #region Dependency
        [Dependency]
        public IWebHostConfiguration WebHostConfiguration { get; set; }
        [Dependency]
        public ILocalToolConfiguration LocalToolConfiguration { get; set; }
        [Dependency]
        public IApiClient APIClient { get; set; }
        #endregion

        public string CurrentUserLogin
        {
            get
            {
                return WebHostConfiguration.CurrentUserEmail;
            }
            set
            {
                WebHostConfiguration.CurrentUserEmail = value;
            }
        }

        public string ServerUrl 
        {
            get 
            {
                return WebHostConfiguration.ServerUrl;
            }
            set
            {
                WebHostConfiguration.ServerUrl = value;
            }
        }

        public bool IsContextMenuUploadCommandInit
        {
            get
            {
                return LocalToolConfiguration.IsContextMenuUploadCommandInit;
            }
            set
            {
                LocalToolConfiguration.IsContextMenuUploadCommandInit = value;
            }
        }

        public string FilesDirectory
        {
            get
            {
                return LocalToolConfiguration.FolderToSaveFile ?? Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }
            set
            {
                LocalToolConfiguration.FolderToSaveFile = value;
            }
        }

        public void UpdateCurrentUserEmail(string email)
        {
            CurrentUserLogin = email;
        }

        public bool UpdateFilesDirectory(string directoryPath, ref string errorMessage)
        {
            if (directoryPath == FilesDirectory)
            {
                return true;
            }

            if (!Directory.Exists(directoryPath))
            {
                errorMessage = "Directory not exist";
                return false;
            }

            if (!IsDirectoryWritable(directoryPath))
            {
                errorMessage = "Directory is not writable";
                return false;
            }

            FilesDirectory = directoryPath;

            return true;
        }

        public bool UpdateServerUrl(string url, ref string errorMessage)
        {
            if (url == ServerUrl)
            {
                return true;
            }

            try
            {
                if (!url.EndsWith('/'))
                {
                    url += "/";
                }

                APIClient.UpdateBaseUri(new Uri(url));
                ServerUrl = url;
            }
            catch
            {
                errorMessage = $"Can't update base client Uri: '{url}'";
                return false;
            }

            return true;
        }

        private bool IsDirectoryWritable(string dirPath)
        {
            try
            {
                using (FileStream fs = File.Create(
                    Path.Combine(
                        dirPath,
                        Path.GetRandomFileName()
                    ),
                    1,
                    FileOptions.DeleteOnClose)
                )
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
