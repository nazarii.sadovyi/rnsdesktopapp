﻿using System.ComponentModel.DataAnnotations;

namespace RnsApp.Desktop.UserInputModels
{
    public class NewMegaConnection: BaseInputModel
    {
		private string _connectionName;
		private string _accountEmail;
		private string _accountPassword;

		[Required]
		[StringLength(100, MinimumLength = 3)]
		public string ConnectionName
		{
			get { return _connectionName; }
			set
			{
				SetProperty(ref _connectionName, value);
				ValidatePropperty(value);
			}
		}

		[Required]
		[EmailAddress]
		public string AccountEmail
		{
			get { return _accountEmail; }
			set
			{
				SetProperty(ref _accountEmail, value);
				ValidatePropperty(value);
			}
		}

		[Required]
		public string AccountPassword
		{
			get { return _accountPassword; }
			set
			{
				SetProperty(ref _accountPassword, value);
				ValidatePropperty(value);
			}
		}
	}
}
