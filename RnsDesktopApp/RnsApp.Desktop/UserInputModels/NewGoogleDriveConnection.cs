﻿using System.ComponentModel.DataAnnotations;

namespace RnsApp.Desktop.UserInputModels
{
    public class NewGoogleDriveConnection: BaseInputModel
    {
		private string _connectionName;
		private string _filePath;

		[Required]
		[StringLength(100, MinimumLength = 3)]
		public string ConnectionName
		{
			get { return _connectionName; }
			set
			{
				SetProperty(ref _connectionName, value);
				ValidatePropperty(value);
			}
		}

		[Required]
		public string FilePath
		{
			get { return _filePath; }
			set
			{
				SetProperty(ref _filePath, value);
				ValidatePropperty(value);
			}
		}
	}
}
