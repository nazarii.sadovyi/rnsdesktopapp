﻿using System.ComponentModel.DataAnnotations;

namespace RnsApp.Desktop.UserInputModels
{
    public class RegisterModel: BaseInputModel
    {
		private string _firstName;
		private string _secondName;
		private string _email;
		private string _password;
		private string _confirmPassword;

		[Required]
		public string FirstName
		{
			get { return _firstName; }
			set 
			{
				SetProperty(ref _firstName, value);
				ValidatePropperty(value);
			}
		}

		[Required]
		public string SecondName
		{
			get { return _secondName; }
			set
			{
				SetProperty(ref _secondName, value);
				ValidatePropperty(value);
			}
		}

		[Required]
		[EmailAddress]
		public string Email
		{
			get { return _email; }
			set
			{
				SetProperty(ref _email, value);
				ValidatePropperty(value);
			}
		}

		[Required]
		[RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$",
			ErrorMessage = "Not allowed password")]
		public string Password
		{
			get { return _password; }
			set
			{
				SetProperty(ref _password, value);
				ValidatePropperty(value);
			}
		}

		[Required]
		public string ConfirmPassword
		{
			get { return _confirmPassword; }
			set
			{
				SetProperty(ref _confirmPassword, value);
				ValidatePropperty(value);
				if (value != Password)
				{
					throw new ValidationException("Password not match");
				}
			}
		}
	}
}
