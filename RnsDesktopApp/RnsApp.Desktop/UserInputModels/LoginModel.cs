﻿using System.ComponentModel.DataAnnotations;

namespace RnsApp.Desktop.UserInputModels
{
	public class LoginModel: BaseInputModel
	{
		private string _email;
		private string _password;

		[Required]
		[EmailAddress]
		public string Email
		{
			get { return _email; }
			set
			{
				SetProperty(ref _email, value);
				ValidatePropperty(value);
			}
		}

		[Required]
		[StringLength(100, MinimumLength = 5)]
		public string Password
		{
			get { return _password; }
			set 
			{
				SetProperty(ref _password, value);
				ValidatePropperty(value);
			}
		}
	}
}
