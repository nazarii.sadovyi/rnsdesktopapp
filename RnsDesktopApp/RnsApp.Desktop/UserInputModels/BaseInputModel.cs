﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Reflection;
using Prism.Mvvm;

namespace RnsApp.Desktop.UserInputModels
{
    public abstract class BaseInputModel: BindableBase
    {
		protected void ValidatePropperty<T>(T value, [CallerMemberName]string name = "")
		{
			Validator.ValidateProperty(value, new ValidationContext(this, null, null)
			{
				MemberName = name
			});
		}

		public bool IsAllPropertyValid
		{
			get
			{
				foreach (var property in GetType().GetProperties())
				{
					var validationAttributes = property.GetCustomAttributes<ValidationAttribute>();
					foreach (var validationAttribute in validationAttributes)
					{
						var isValid = validationAttribute.IsValid(property.GetValue(this));
						if (!isValid)
						{
							return false;
						}
					}
				}
				return true;
			}
		}
	}
}
