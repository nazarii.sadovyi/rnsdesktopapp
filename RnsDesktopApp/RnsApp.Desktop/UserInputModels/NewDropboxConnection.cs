﻿using System.ComponentModel.DataAnnotations;

namespace RnsApp.Desktop.UserInputModels
{
	public class NewDropboxConnection: BaseInputModel
    {
		private string _connectionName;
		private string _accessToken;

		[Required]
		[StringLength(100, MinimumLength = 3)]
		public string ConnectionName
		{
			get { return _connectionName; }
			set
			{
				SetProperty(ref _connectionName, value);
				ValidatePropperty(value);
			}
		}

		[Required]
		public string AccessToken
		{
			get { return _accessToken; }
			set
			{
				SetProperty(ref _accessToken, value);
				ValidatePropperty(value);
			}
		}
	}
}
