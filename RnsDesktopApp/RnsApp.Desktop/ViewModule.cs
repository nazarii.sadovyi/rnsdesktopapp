﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using RnsApp.Core.Services;
using RnsApp.Desktop.Views;
using RnsApp.Desktop.Views.CredentialManagerPage;
using RnsApp.Desktop.Views.Dialogs;
using RnsApp.Desktop.Views.SettingsPage;
using RnsApp.Desktop.Views.UploadFilePage;
using RnsApp.Desktop.Views.UserFilesPage;

namespace RnsApp.Desktop
{
    public class AuthModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion("ContentRegion", typeof(LoginUserControl));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<LoginUserControl>();
            containerRegistry.RegisterForNavigation<RegisterUserControl>();
            containerRegistry.RegisterForNavigation<UpdateServerUrlDialog>();
        }
    }

    public class MainModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            var regionManager = containerProvider.Resolve<IRegionManager>();
            var commandLineArgsService = containerProvider.Resolve<ICommandLineArgsService>();
            
            if (commandLineArgsService.ArgumentExist(Common.Enums.ComandLineArgKind.FileToUploadPath))
            {
                regionManager.RegisterViewWithRegion("ContentRegion2", typeof(UploadFileControl));
            }
            else
            {
                regionManager.RegisterViewWithRegion("ContentRegion2", typeof(UserFilesControl));
            }
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<UserFilesControl>();
            containerRegistry.RegisterForNavigation<UploadFileControl>();
            containerRegistry.RegisterForNavigation<DownloadFileDialog>();
            containerRegistry.RegisterForNavigation<InformationDialog>();
            containerRegistry.RegisterForNavigation<CredentialManager>();
            containerRegistry.RegisterForNavigation<SettingsControl>();
            containerRegistry.RegisterForNavigation<DeleteFileDialog>();
        }
    }
}
