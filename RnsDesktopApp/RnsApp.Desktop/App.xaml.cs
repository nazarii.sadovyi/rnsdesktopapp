﻿using NLog;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Unity;
using RnsApp.Common.Exceptions;
using RnsApp.Core;
using RnsApp.Core.Services;
using RnsApp.Desktop.Services;
using RnsApp.Desktop.Utilities;
using RnsApp.Desktop.ViewModels.Dialogs;
using RnsApp.Desktop.Views;
using RnsApp.Desktop.Views.Dialogs;
using System;
using System.Reflection;
using System.Threading;
using System.Windows;

namespace RnsApp.Desktop
{
    public partial class App : PrismApplication
    {
        private static readonly Logger _Logger = LogManager.GetCurrentClassLogger();
        private Mutex applicationMutex;

        protected override void OnStartup(StartupEventArgs e)
        {
            if (IsApplicationAlreadyStarted())
            {
                App.Current.Shutdown();
            }

            _Logger.Info($"App command line args: {string.Join(", ", Environment.GetCommandLineArgs())}");

            base.OnStartup(e);
        }

        protected override Window CreateShell()
        {
            DispatcherUnhandledException += App_DispatcherUnhandledException;
            
            var authenticationService = Container.Resolve<IAuthenticationService>();
            if (authenticationService.TryToLogin().Result)
            {
                return Container.Resolve<MainWindow>();
            }
            else
            {
                return Container.Resolve<AuthenticationWindow>();
            }
        }

        private void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            if (e.Exception is NotCorrectSettingException ex)
            {
                var toastNotificationService = Container.Resolve<IToastNotificationService>();
                toastNotificationService.ShowToast($"Setting '{ex.SettingName}' is incorrect: {ex.ErrorMessage}", UIModels.ToastType.Error);
                e.Handled = true;
                return;
            }

            _Logger.Error(e.Exception, "Dispatcher unhandled exception");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            CoreDependency.Register(containerRegistry);

            containerRegistry.RegisterSingleton<IApplicationCommands, ApplicationCommands>();
            containerRegistry.RegisterSingleton<IToastNotificationCompositeCommand, ToastNotificationCompositeCommand>();
            containerRegistry.Register<IToastNotificationService, ToastNotificationService>();

            #region RegisterDialogs
            containerRegistry.RegisterDialogWindow<BaseDialogWindow>();
            containerRegistry.RegisterDialog<DownloadFileDialog, DownloadFileDialogViewModel>(nameof(BaseDialogWindow));
            containerRegistry.RegisterDialog<InformationDialog, InformationDialogViewModel>(nameof(BaseDialogWindow));
            containerRegistry.RegisterDialog<UpdateServerUrlDialog, UpdateServerUrlDialogViewModel>(nameof(BaseDialogWindow));
            containerRegistry.RegisterDialog<DeleteFileDialog, DeleteFileDialogViewModel>(nameof(BaseDialogWindow));
            #endregion
        }

        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            moduleCatalog.AddModule<AuthModule>();
            moduleCatalog.AddModule<MainModule>();
        }

        protected override void ConfigureViewModelLocator()
        {
            base.ConfigureViewModelLocator();

            ViewModelLocationProvider.SetDefaultViewTypeToViewModelTypeResolver((viewType) =>
            {
                string viewName = string.Empty;
                if (viewType.Namespace.EndsWith("Page"))
                {
                    viewName = $"{viewType.Namespace.Remove(viewType.Namespace.LastIndexOf('.'))}.{viewType.Name}"
                        .Replace(".Views.", ".ViewModels.");
                }
                else
                {
                    viewName = viewType.FullName.Replace(".Views.", ".ViewModels.");
                }
                var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName;
                var viewModelName = $"{viewName}ViewModel, {viewAssemblyName}";
                return Type.GetType(viewModelName);
            });
        }

        private bool IsApplicationAlreadyStarted()
        {
            applicationMutex = new Mutex(true, "RnsApp", out bool aIsNewInstance);
            return !aIsNewInstance;
        }
    }
}
