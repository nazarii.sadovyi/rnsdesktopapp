﻿using Prism.Commands;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace RnsApp.Desktop.Views
{
    /// <summary>
    /// Interaction logic for TitleBar.xaml
    /// </summary>
    public partial class TitleBar : UserControl
    {
        private DelegateCommand closeWindowCommand;
        private DelegateCommand changeWindowStateCommand;
        private Window currentWindow;

        public Window CurrentWindow 
        {
            get 
            {
                if (currentWindow == null)
                {
                    currentWindow = Window.GetWindow(this);
                }
                return currentWindow;
            }
        }

        public DelegateCommand CloseWindowCommand
        {
            get
            {
                return closeWindowCommand ??
                  (closeWindowCommand = new DelegateCommand(
                      () =>
                      {
                          Application.Current.Shutdown();
                      }
                  ));
            }
        }

        public DelegateCommand ChangeWindowStateCommand
        {
            get
            {
                return changeWindowStateCommand ??
                  (changeWindowStateCommand = new DelegateCommand(
                      () =>
                      {
                          if (CurrentWindow.WindowState == WindowState.Normal)
                          {
                              CurrentWindow.WindowState = WindowState.Minimized;
                          }
                          else
                          {
                              CurrentWindow.WindowState = WindowState.Normal;
                          }
                      }
                  ));
            }
        }

        public TitleBar()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            CurrentWindow.DragMove();
        }
    }
}
