﻿using Prism.Regions;
using RnsApp.Core.Services;
using RnsApp.Desktop.Services;
using RnsApp.Desktop.Utilities;
using RnsApp.Desktop.ViewModels;
using RnsApp.ServerNotification;
using RnsApp.WindowsShellCommand;
using System.Windows;

namespace RnsApp.Desktop.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(IApplicationCommands applicationCommands,
            IServerNotificationManager serverNotificationManager,
            IToastNotificationCompositeCommand toastNotificationCompositeCommand,
            IRegionManager regionManager,
            Prism.Modularity.IModuleManager moduleManager,
            IWindowsShellCommandService windowsShellCommandService,
            ICommandLineArgsService commandLineArgsService)
        {
            InitializeComponent();

            moduleManager.LoadModule("MainModule");
            moduleManager.Run();

            var _localScopedRegion = regionManager.CreateRegionManager();
            RegionManager.SetRegionManager((DependencyObject)FindName("ContentRegion2"), _localScopedRegion);

            moduleManager.LoadModule("MainModule");
            moduleManager.Run();

            DataContext = new MainWindowViewModel(
                applicationCommands,
                serverNotificationManager,
                toastNotificationCompositeCommand,
                _localScopedRegion,
                windowsShellCommandService,
                commandLineArgsService);
            
        }
    }
}
