﻿using System.Windows.Controls;

namespace RnsApp.Desktop.Views.SettingsPage
{
    /// <summary>
    /// Interaction logic for SettingsControl.xaml
    /// </summary>
    public partial class SettingsControl : UserControl
    {
        public SettingsControl()
        {
            InitializeComponent();
        }
    }
}
