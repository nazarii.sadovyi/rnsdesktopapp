﻿using System.Windows.Controls;

namespace RnsApp.Desktop.Views.UserFilesPage
{
    /// <summary>
    /// Interaction logic for UserFilesControl.xaml
    /// </summary>
    public partial class UserFilesControl : UserControl
    {
        public UserFilesControl()
        {
            InitializeComponent();
        }
    }
}
