﻿using Prism.Commands;
using RnsApp.Desktop.UIModels;
using System.Windows;
using System.Windows.Controls;

namespace RnsApp.Desktop.Views.UserFilesPage
{
    /// <summary>
    /// Interaction logic for FileInfoControl.xaml
    /// </summary>
    public partial class FileInfoControl : UserControl
    {
        public FileInfo FileInfo
        {
            get { return (FileInfo)GetValue(FileInfoProperty); }
            set { SetValue(FileInfoProperty, value); }
        }

        // Using a DependencyProperty as the backing store for UserFile.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FileInfoProperty = 
            DependencyProperty.Register("FileInfo", typeof(FileInfo), typeof(FileInfoControl), new FrameworkPropertyMetadata(null));



        public DelegateCommand DeleteFileCommand
        {
            get { return (DelegateCommand)GetValue(DeleteFileCommandProperty); }
            set { SetValue(DeleteFileCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DeleteFileCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DeleteFileCommandProperty =
            DependencyProperty.Register("DeleteFileCommand", typeof(DelegateCommand), typeof(FileInfoControl), new PropertyMetadata(null));



        public DelegateCommand ShowInFolderCommand
        {
            get { return (DelegateCommand)GetValue(ShowInFolderCommandProperty); }
            set { SetValue(ShowInFolderCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowInFolderCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowInFolderCommandProperty =
            DependencyProperty.Register("ShowInFolderCommand", typeof(DelegateCommand), typeof(FileInfoControl), new PropertyMetadata(null));



        public DelegateCommand RestoreServerVersionCommand
        {
            get { return (DelegateCommand)GetValue(RestoreServerVersionCommandProperty); }
            set { SetValue(RestoreServerVersionCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RestoreServerVersionCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RestoreServerVersionCommandProperty =
            DependencyProperty.Register("RestoreServerVersionCommand", typeof(DelegateCommand), typeof(FileInfoControl), new PropertyMetadata(null));



        public DelegateCommand DownloadCommand
        {
            get { return (DelegateCommand)GetValue(DownloadCommandProperty); }
            set { SetValue(DownloadCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DownloadCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DownloadCommandProperty =
            DependencyProperty.Register("DownloadCommand", typeof(DelegateCommand), typeof(FileInfoControl), new PropertyMetadata(null));



        public DelegateCommand UpdateFileCommand
        {
            get { return (DelegateCommand)GetValue(UpdateFileCommandProperty); }
            set { SetValue(UpdateFileCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for UpdateFileCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UpdateFileCommandProperty =
            DependencyProperty.Register("UpdateFileCommand", typeof(DelegateCommand), typeof(FileInfoControl), new PropertyMetadata(null));



        public FileInfoControl()
        {
            InitializeComponent();
        }
    }
}
