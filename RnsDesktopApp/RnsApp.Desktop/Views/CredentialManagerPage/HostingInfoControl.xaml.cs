﻿using Prism.Commands;
using RnsApp.Desktop.UIModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace RnsApp.Desktop.Views.CredentialManagerPage
{
    /// <summary>
    /// Interaction logic for HostingInfoControl.xaml
    /// </summary>
    [ContentProperty("AddCredentialContent")]
    public partial class HostingInfoControl : UserControl
    {
        public bool IsBacksideShown
        {
            get { return (bool)GetValue(IsBacksideShownProperty); }
            set { SetValue(IsBacksideShownProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsBacksideShown.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsBacksideShownProperty = DependencyProperty.Register(
            "IsBacksideShown", 
            typeof(bool), 
            typeof(HostingInfoControl),
            new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)
        );

        private DelegateCommand _changeContentVisibilityCommand;
        public DelegateCommand ChangeContentVisibilityCommand
        {
            get
            {
                return _changeContentVisibilityCommand ??
                  (_changeContentVisibilityCommand = new DelegateCommand(
                      () =>
                      {
                          IsBacksideShown = !IsBacksideShown;
                      }
                  ));
            }
        }

        public StorageHostingInfo StorageHosting
        {
            get { return (StorageHostingInfo)GetValue(StorageHostingProperty); }
            set { SetValue(StorageHostingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StorageHosting.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StorageHostingProperty = DependencyProperty.Register(
            "StorageHosting",
            typeof(StorageHostingInfo),
            typeof(HostingInfoControl),
            new PropertyMetadata(null)
        );


        public ICommand RemoveConnectionCommand
        {
            get { return (ICommand)GetValue(RemoveConnectionCommandProperty); }
            set { SetValue(RemoveConnectionCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RemoveConnectionCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RemoveConnectionCommandProperty = DependencyProperty.Register(
            "RemoveConnectionCommand",
            typeof(ICommand),
            typeof(HostingInfoControl),
            new PropertyMetadata(null));


        public FrameworkElement AddCredentialContent
        {
            get { return (FrameworkElement)GetValue(AddCredentialContentProperty); }
            set { SetValue(AddCredentialContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AddCredentialContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AddCredentialContentProperty =
            DependencyProperty.Register(
                "AddCredentialContent",
                typeof(object),
                typeof(HostingInfoControl),
                new PropertyMetadata(null));


        public HostingInfoControl()
        {
            InitializeComponent();
        }
    }
}
