﻿using System.Windows.Controls;

namespace RnsApp.Desktop.Views.CredentialManagerPage
{
    /// <summary>
    /// Interaction logic for CredentialManager.xaml
    /// </summary>
    public partial class CredentialManager : UserControl
    {
        public CredentialManager()
        {
            InitializeComponent();
        }
    }
}
