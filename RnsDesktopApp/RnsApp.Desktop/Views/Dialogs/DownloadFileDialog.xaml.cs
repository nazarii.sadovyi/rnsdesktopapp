﻿using System.Windows.Controls;

namespace RnsApp.Desktop.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for DownloadFileDialog.xaml
    /// </summary>
    public partial class DownloadFileDialog : UserControl
    {
        public DownloadFileDialog()
        {
            InitializeComponent();
        }
    }
}
