﻿using Prism.Services.Dialogs;
using RnsApp.Desktop.Utilities;
using System.Windows;
using Unity;

namespace RnsApp.Desktop.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for BaseDialogWindow.xaml
    /// </summary>
    public partial class BaseDialogWindow : Window, IDialogWindow
    {
        [Dependency]
        public IApplicationCommands ApplicationCommands { get; set; }
        public BaseDialogWindow()
        {
            InitializeComponent();

            Loaded += BaseDialogWindow_Loaded;
            Closing += BaseDialogWindow_Closing;
        }

        private void BaseDialogWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ApplicationCommands.OpenDialogCommand.Execute(false);
        }

        private void BaseDialogWindow_Loaded(object sender, RoutedEventArgs e)
        {
            ApplicationCommands.OpenDialogCommand.Execute(true);
        }

        public IDialogResult Result { get; set; }
    }
}
