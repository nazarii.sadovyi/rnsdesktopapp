﻿using System.Windows.Controls;

namespace RnsApp.Desktop.Views.UploadFilePage
{
    /// <summary>
    /// Interaction logic for UploadFileControl.xaml
    /// </summary>
    public partial class UploadFileControl : UserControl
    {
        public UploadFileControl()
        {
            InitializeComponent();
        }
    }
}
