﻿using MaterialDesignThemes.Wpf;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace RnsApp.Desktop.Views.Controls
{
    /// <summary>
    /// Interaction logic for ToastNotificationControl.xaml
    /// </summary>
    public partial class ToastNotificationControl : UserControl
    {
        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(ToastNotificationControl), new PropertyMetadata(string.Empty));


        public bool IsToastVisible
        {
            get { return (bool)GetValue(IsToastVisibleProperty); }
            set { SetValue(IsToastVisibleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsToastVisible.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsToastVisibleProperty =
            DependencyProperty.Register("IsToastVisible", typeof(bool), typeof(ToastNotificationControl), new PropertyMetadata(false));


        public PackIconKind ToastIcon
        {
            get { return (PackIconKind)GetValue(ToastIconProperty); }
            set { SetValue(ToastIconProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ToastIcon.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ToastIconProperty =
            DependencyProperty.Register("ToastIcon", typeof(PackIconKind), typeof(ToastNotificationControl), new PropertyMetadata(PackIconKind.Message));


        public Brush ToastBackground
        {
            get { return (Brush)GetValue(ToastBackgroundProperty); }
            set { SetValue(ToastBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ToastBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ToastBackgroundProperty =
            DependencyProperty.Register("ToastBackground", typeof(Brush), typeof(ToastNotificationControl), new PropertyMetadata(Brushes.Gray));


        public ToastNotificationControl()
        {
            InitializeComponent();
        }
    }
}
