﻿using System.Windows;
using System.Windows.Controls;

namespace RnsApp.Desktop.Views
{
    /// <summary>
    /// Interaction logic for MenuItem.xaml
    /// </summary>
    public partial class MenuItem : UserControl
    {
        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Label.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(MenuItem), new PropertyMetadata(0));


        public int ItemNumber
        {
            get { return (int)GetValue(ItemNumberProperty); }
            set { SetValue(ItemNumberProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ItemNumber.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemNumberProperty =
            DependencyProperty.Register("ItemNumber", typeof(int), typeof(MenuItem), new PropertyMetadata(0));


        public string Image
        {
            get { return (string)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImagePath.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageProperty =
            DependencyProperty.Register("ImagePath", typeof(string), typeof(MenuItem), new PropertyMetadata(0));


        public MenuItem()
        {
            InitializeComponent();
            DataContext = this;
        }
    }
}
