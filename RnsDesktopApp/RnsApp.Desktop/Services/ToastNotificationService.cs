﻿using Prism.Commands;
using RnsApp.Desktop.UIModels;
using System.Threading;
using System.Threading.Tasks;
using Unity;

namespace RnsApp.Desktop.Services
{
    public interface IToastNotificationCompositeCommand
    {
        CompositeCommand ShowToastCommand { get; }
    }
    public interface IToastNotificationService
    {
        Task ShowToast(string message, ToastType toastStyle = ToastType.Info, int activeMiliseconds = 5000);
    }

    public class ToastNotificationCompositeCommand : IToastNotificationCompositeCommand
    {
        public CompositeCommand ShowToastCommand { get; } = new CompositeCommand(true);
    }

    public class ToastNotificationService: IToastNotificationService
    {
        private int toastCount = 0;
        [Dependency]
        public IToastNotificationCompositeCommand _ToastNotificationCompositeCommand { get; set; }

        public async Task ShowToast(string message, ToastType toastStyle = ToastType.Info, int activeMiliseconds = 5000)
        {
            toastCount++;

            var toastUIModel = new Toast(message, toastStyle);
            toastUIModel.IsVisible = true;

            _ToastNotificationCompositeCommand.ShowToastCommand.Execute(toastUIModel);
            await Task.Run(() => 
            {
                Thread.Sleep(activeMiliseconds);
            });

            toastCount--;
            if (toastCount != 0)
            {
                return;
            }

            toastUIModel.IsVisible = false;
            _ToastNotificationCompositeCommand.ShowToastCommand.Execute(toastUIModel);
        }
    }
}
