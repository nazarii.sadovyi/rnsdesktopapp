﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using RnsApp.Core.Services;
using RnsApp.Desktop.UserInputModels;
using RnsApp.Desktop.Views;
using System.Threading.Tasks;
using System.Windows;
using Unity;

namespace RnsApp.Desktop.ViewModels
{
    public class LoginUserControlViewModel : BindableBase, INavigationAware
    {
        #region Private field
        private DelegateCommand<Window> loginCommand;
        private DelegateCommand goToRegisterCommand;
        private bool _IsLoading;
        private bool _rememberUser;
        private LoginModel _loginModel;
        private string _loginError;
        #endregion

        [Dependency]
        public IAuthenticationService AuthenticationService { get; set; }
        [Dependency]
        public IRegionManager RegionManager { get; set; }
        [Dependency]
        public IUnityContainer UnityContainer { get; set; }

        public DelegateCommand<Window> LoginCommand
        {
            get
            {
                return loginCommand ??
                  (loginCommand = new DelegateCommand<Window>(
                      (w) =>
                      {
                          TryToLogin(w);
                      },
                      (w) =>
                      {
                          return LoginModel.IsAllPropertyValid;
                      }
                  )
                  .ObservesProperty(() => LoginModel.Email)
                  .ObservesProperty(() => LoginModel.Password));
            }
        }
        public DelegateCommand GoToRegisterCommand
        {
            get
            {
                return goToRegisterCommand ??
                  (goToRegisterCommand = new DelegateCommand(
                      () =>
                      {
                          RegionManager.RequestNavigate("ContentRegion", nameof(RegisterUserControl));
                      }
                  ));
            }
        }
        public bool RememberUser
        {
            get { return _rememberUser; }
            set { SetProperty(ref _rememberUser, value); }
        }
        public string LoginError
        {
            get { return _loginError; }
            set { SetProperty(ref _loginError, value); }
        }
        public bool IsLoading
        {
            get { return _IsLoading; }
            set { SetProperty(ref _IsLoading, value); }
        }
        public LoginModel LoginModel
        {
            get { return _loginModel; }
            set { SetProperty(ref _loginModel, value); }
        }

        private void TryToLogin(Window currentWindow)
        {
            IsLoading = true;
            Task.Run(async () =>
            {
                var loginResult = await AuthenticationService.TryToLogin(LoginModel.Email, LoginModel.Password, RememberUser);
                if (loginResult.IsSuccess)
                {
                    Application.Current.Dispatcher.Invoke(delegate
                    {
                        var mainWindow = UnityContainer.Resolve<MainWindow>();
                        mainWindow.Show();
                        currentWindow.Close();
                    });
                }
                else
                {
                    IsLoading = false;
                    LoginError = loginResult.Error ?? "Incorrect email or password";
                }
            });
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            var emailExist = navigationContext.Parameters.TryGetValue("email", out string newUserEmail);
            if (emailExist)
            {
                LoginModel = new LoginModel
                {
                    Email = newUserEmail
                };
            }
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            // logic isn't needed
        }

        public LoginUserControlViewModel(IAppSettingService appSettingService)
        {
            LoginModel = new LoginModel();
            if (!string.IsNullOrEmpty(appSettingService.CurrentUserLogin))
            {
                LoginModel.Email = appSettingService.CurrentUserLogin;
            }
        }
    }
}
