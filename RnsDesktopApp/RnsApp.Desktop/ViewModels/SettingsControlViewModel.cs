﻿using Prism.Commands;
using Prism.Mvvm;
using RnsApp.Core.Services;
using RnsApp.Desktop.Services;
using RnsApp.Desktop.UIModels;
using RnsApp.Desktop.Utilities;
using RnsApp.Desktop.Views;
using RnsApp.WindowsShellCommand;
using RnsApp.WindowsShellCommand.Enums;
using System.Windows;
using Unity;

namespace RnsApp.Desktop.ViewModels
{
    public class SettingsControlViewModel: BindableBase
    {
        #region Private field
        private readonly RegisterWindowsLibraryManager _registerWindowsLibraryManager = new RegisterWindowsLibraryManager();
        private DelegateCommand _changeDirectoryCommand;
        private DelegateCommand _initContextLibraryCommand;
        private DelegateCommand<Window> _logoutCommand;

        private bool _contextMenuUploadCommandState;
        private string _currentUserEmail;
        private string _filesDirectory;
        private string _webSocketUrl;
        private string _serverUrl;
        #endregion

        #region Dependency
        [Dependency]
        public ILocalFileManagerService LocalFileManager { get; set; }
        [Dependency]
        public IApplicationCommands ApplicationCommands { get; set; }
        [Dependency]
        public IAppSettingUpdateService AppSettingUpdateService { get; set; }
        [Dependency]
        public IToastNotificationService ToastNotification { get; set; }
        [Dependency]
        public IAuthenticationService AuthenticationService { get; set; }
        [Dependency]
        public IUnityContainer UnityContainer { get; set; }
        #endregion

        public DelegateCommand ChangeDirectoryCommand
        {
            get
            {
                return _changeDirectoryCommand ??
                  (_changeDirectoryCommand = new DelegateCommand(
                      () =>
                      {
                          var newDirectory = LocalFileManager.SelectDirectory();

                          if (string.IsNullOrEmpty(newDirectory))
                          {
                              ToastNotification.ShowToast("Icorrect directory was selected", ToastType.Warning);
                              return;
                          }

                          var errorMessage = string.Empty;
                          var updateResult = AppSettingUpdateService.UpdateFilesDirectory(newDirectory, ref errorMessage);

                          if (updateResult)
                          {
                              FilesDirectory = newDirectory;
                              ToastNotification.ShowToast("Files directory has been updated", ToastType.Success);
                          }
                          else
                          {
                              ToastNotification.ShowToast($"Update error: {errorMessage}", ToastType.Error);
                          }
                      }
                  ));
            }
        }

        public DelegateCommand<Window> LogoutCommand
        {
            get
            {
                return _logoutCommand ??
                  (_logoutCommand = new DelegateCommand<Window>(
                      (currentWindow) =>
                      {
                          AuthenticationService.Logout();
                          App.Current.Dispatcher.Invoke(() => 
                          {
                              var authWindow = UnityContainer.Resolve<AuthenticationWindow>();
                              authWindow.Show();
                              currentWindow.Close();
                          });
                      }
                  ));
            }
        }

        public DelegateCommand InitContextLibraryCommand
        {
            get
            {
                return _initContextLibraryCommand ??
                  (_initContextLibraryCommand = new DelegateCommand(
                      () =>
                      {
                          var initType = ContextMenuUploadCommandState ? "Register" : "Unregister";
                          ApplicationCommands.ExecuteFetchDataAsync(async () => 
                          {
                              if (ContextMenuUploadCommandState)
                              {
                                  await _registerWindowsLibraryManager.Register(LibraryKind.UploadFileContextMenu);
                                  AppSettingUpdateService.IsContextMenuUploadCommandInit = true;
                              }
                              else
                              {
                                  await _registerWindowsLibraryManager.UnRegister(LibraryKind.UploadFileContextMenu);
                                  AppSettingUpdateService.IsContextMenuUploadCommandInit = false;
                              }
                          }, $"{initType} windows context menu upload file command");
                      }
                  ));
            }
        }

        public bool ContextMenuUploadCommandState
        {
            get { return _contextMenuUploadCommandState; }
            set { SetProperty(ref _contextMenuUploadCommandState, value); }
        }
        
        public string FilesDirectory
        {
            get { return _filesDirectory; }
            set { SetProperty(ref _filesDirectory, value); }
        }

        public string ServerUrl
        {
            get { return _serverUrl; }
            set { SetProperty(ref _serverUrl, value); }
        }

        public string WebSocketUrl
        {
            get { return _webSocketUrl; }
            set { SetProperty(ref _webSocketUrl, value); }
        }

        public string CurrentUserEmail
        {
            get { return _currentUserEmail; }
            set { SetProperty(ref _currentUserEmail, value); }
        }

        public SettingsControlViewModel(IAppSettingService appSettingService)
        {
            ContextMenuUploadCommandState = appSettingService.IsContextMenuUploadCommandInit;
            CurrentUserEmail = appSettingService.CurrentUserLogin;
            FilesDirectory = appSettingService.FilesDirectory;
            ServerUrl = appSettingService.ServerUrl;

            WebSocketUrl = appSettingService.ServerUrl + "chat";
        }
    }
}
