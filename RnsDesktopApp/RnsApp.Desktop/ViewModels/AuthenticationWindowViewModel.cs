﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using Unity;

namespace RnsApp.Desktop.ViewModels
{
    public class AuthenticationWindowViewModel: BindableBase
    {
        [Dependency]
        public IDialogService DialogService { get; set; }

        private DelegateCommand _updateServerUrlCommand;

        public DelegateCommand UpdateServerUrlCommand
        {
            get
            {
                return _updateServerUrlCommand ??
                  (_updateServerUrlCommand = new DelegateCommand(
                      () =>
                      {
                          DialogService.ShowDialog("UpdateServerUrlDialog", default, default);
                      }
                  ));
            }
        }
        
        public AuthenticationWindowViewModel()
        {
            
        }
    }
}
