﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using RnsApp.Common.Entities;
using RnsApp.Core.Services;
using RnsApp.Desktop.UserInputModels;
using RnsApp.Desktop.Views;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Unity;

namespace RnsApp.Desktop.ViewModels
{
    public class RegisterUserControlViewModel: BindableBase, INavigationAware
    {
        #region Private field
        private DelegateCommand registerCommand;
        private DelegateCommand toLoginCommand;
        private bool _IsLoading;
        private string _registerError;
        private RegisterModel _registerModel;
        #endregion

        #region Dependency injection

        [Dependency]
        public IRegisterUserService RegisterUserService { get; set; }
        [Dependency]
        public IRegionManager RegionManager { get; set; }
        #endregion

        public DelegateCommand RegisterCommand
        {
            get
            {
                return registerCommand ??
                  (registerCommand = new DelegateCommand(
                      () => {
                          TryToRegister();
                      },
                      () => {
                          return RegisterModel.IsAllPropertyValid && RegisterModel.Password == RegisterModel.ConfirmPassword;
                      }
                  )).ObservesProperty(() => RegisterModel.FirstName)
                  .ObservesProperty(() => RegisterModel.SecondName)
                  .ObservesProperty(() => RegisterModel.Email)
                  .ObservesProperty(() => RegisterModel.Password)
                  .ObservesProperty(() => RegisterModel.ConfirmPassword);
            }
        }

        public DelegateCommand ToLoginCommand
        {
            get
            {
                return toLoginCommand ??
                  (toLoginCommand = new DelegateCommand(
                      () => {
                          RegionManager.RequestNavigate("ContentRegion", nameof(LoginUserControl));
                      }
                  ));
            }
        }

        private void TryToRegister()
        {
            IsLoading = true;

            Task.Run(async () =>
            {
                var registerResult = await RegisterUserService.Register(new RegisterUserModel()
                {
                    Email = RegisterModel.Email,
                    FirstName = RegisterModel.FirstName,
                    SecondName = RegisterModel.SecondName,
                    Password = RegisterModel.Password
                });

                if (registerResult.IsSuccess)
                {
                    Application.Current.Dispatcher.Invoke(delegate
                    {
                        var parameters = new NavigationParameters
                        {
                            { "email", RegisterModel.Email }
                        };
                        RegionManager.RequestNavigate("ContentRegion", nameof(LoginUserControl), parameters);
                    });
                }
                else
                {
                    RegisterError = registerResult.Errors.ElementAt(0);
                }

                IsLoading = false;
            });
        }

        public string RegisterError
        {
            get { return _registerError; }
            set { SetProperty(ref _registerError, value); }
        }
        public bool IsLoading
        {
            get { return _IsLoading; }
            set { SetProperty(ref _IsLoading, value); }
        }
        public RegisterModel RegisterModel
        {
            get { return _registerModel; }
            set { SetProperty(ref _registerModel, value); }
        }

        public RegisterUserControlViewModel()
        {
            RegisterModel = new RegisterModel();
        }

        #region Navigation
        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            // logic isn't needed
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return false;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            // logic isn't needed
        }
        #endregion
    }
}
