﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using RnsApp.Common.Entities;
using RnsApp.Common.Enums;
using RnsApp.Common.Models;
using RnsApp.Core.Services;
using RnsApp.Desktop.Services;
using RnsApp.Desktop.UIModels;
using RnsApp.Desktop.UserInputModels;
using RnsApp.Desktop.Utilities;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Unity;

namespace RnsApp.Desktop.ViewModels
{
    public class CredentialManagerViewModel: BindableBase, INavigationAware
    {
        private DelegateCommand<Guid?> _removeStorageHostingConnection;
        public DelegateCommand<Guid?> RemoveStorageHostingConnection
        {
            get
            {
                return _removeStorageHostingConnection ??
                  (_removeStorageHostingConnection = new DelegateCommand<Guid?>(
                      async (id) =>
                      {
                          var megaStorageItem = MegaStorageHostingInfo.Connections.FirstOrDefault(x => x.Id == id.Value);
                          if (megaStorageItem != null)
                          {
                              await StorageHostingManagerService.Remove(Hosting.Mega, id.Value);
                              MegaStorageHostingInfo.Connections.Remove(megaStorageItem);
                              return;
                          }

                          var dropboxStorageItem = DropboxStorageHostingInfo.Connections.FirstOrDefault(x => x.Id == id.Value);
                          if (dropboxStorageItem != null)
                          {
                              await StorageHostingManagerService.Remove(Hosting.Dropbox, id.Value);
                              DropboxStorageHostingInfo.Connections.Remove(dropboxStorageItem);
                              return;
                          }

                          var googledriveStorageItem = GoogleDriveStorageHostingInfo.Connections.FirstOrDefault(x => x.Id == id.Value);
                          if (googledriveStorageItem != null)
                          {
                              await StorageHostingManagerService.Remove(Hosting.GoogleDrive, id.Value);
                              GoogleDriveStorageHostingInfo.Connections.Remove(googledriveStorageItem);
                          }
                      }
                  ));
            }
        }
        
        private DelegateCommand<string> _openWebUrlCommand;
        public DelegateCommand<string> OpenWebUrlCommand
        {
            get
            {
                return _openWebUrlCommand ??
                  (_openWebUrlCommand = new DelegateCommand<string>(
                      (url) =>
                      {
                          var fullUrl = Path.Combine(AppSettingService.ServerUrl, url);
                          Process.Start(new ProcessStartInfo
                          {
                              FileName = fullUrl,
                              UseShellExecute = true
                          });
                      }
                  ));
            }
        }


        #region Mega storage hosting
        private StorageHostingInfo _MegaStorageHostingInfo;
        private NewMegaConnection _newMegaConnection;
        private DelegateCommand _addNewMegaConnectionCommand;
        private bool _IsAddMegaConnectionVisible;

        public StorageHostingInfo MegaStorageHostingInfo
        {
            get { return _MegaStorageHostingInfo; }
            set { SetProperty(ref _MegaStorageHostingInfo, value); }
        }

        public NewMegaConnection InputMegaConnection
        {
            get 
            {
                if (_newMegaConnection == null)
                {
                    _newMegaConnection = new NewMegaConnection();
                }
                return _newMegaConnection;
            }
            set { SetProperty(ref _newMegaConnection, value); }
        }

        public DelegateCommand AddNewMegaConnectionCommand
        {
            get
            {
                return _addNewMegaConnectionCommand ??
                  (_addNewMegaConnectionCommand = new DelegateCommand(
                      () =>
                      {
                          Task.Run(async () =>
                          {
                              AddCredentialResult result = default;
                              await ApplicationCommands.ExecuteFetchDataAsync(async () => 
                              {
                                  result = await StorageHostingManagerService.AddNewCredential(
                                  new MegaCredential(
                                      InputMegaConnection.ConnectionName,
                                      InputMegaConnection.AccountEmail,
                                      InputMegaConnection.AccountPassword)
                                  );
                              });

                              await ShowAddNewItemResult(result, "Email or password is not correct", () => 
                              {
                                  Application.Current.Dispatcher.Invoke(delegate
                                  {
                                      MegaStorageHostingInfo.Connections.Add(
                                      new StorageHostingConnection()
                                      {
                                          Id = result.NewCredentialId,
                                          Name = InputMegaConnection.ConnectionName
                                      });
                                      IsAddMegaConnectionVisible = false;
                                      InputMegaConnection = new NewMegaConnection();
                                  });
                              });
                          });
                      },
                      () =>
                      {
                          return InputMegaConnection.IsAllPropertyValid;
                      }
                  )).ObservesProperty(() => InputMegaConnection.ConnectionName)
                  .ObservesProperty(() => InputMegaConnection.AccountEmail)
                  .ObservesProperty(() => InputMegaConnection.AccountPassword);
            }
        }

        public bool IsAddMegaConnectionVisible
        {
            get { return _IsAddMegaConnectionVisible; }
            set { SetProperty(ref _IsAddMegaConnectionVisible, value); }
        }
        #endregion

        #region Dropbox storage hosting
        private NewDropboxConnection _newDropboxConnection;
        private DelegateCommand _addNewDropboxConnectionCommand;
        private StorageHostingInfo _DropboxStorageHostingInfo;
        private bool _IsAddDropboxConnectionVisible;
        
        public bool IsAddDropboxConnectionVisible
        {
            get { return _IsAddDropboxConnectionVisible; }
            set { SetProperty(ref _IsAddDropboxConnectionVisible, value); }
        }

        public StorageHostingInfo DropboxStorageHostingInfo
        {
            get { return _DropboxStorageHostingInfo; }
            set { SetProperty(ref _DropboxStorageHostingInfo, value); }
        }

        public NewDropboxConnection InputDropboxConnection
        {
            get
            {
                if (_newDropboxConnection == null)
                {
                    _newDropboxConnection = new NewDropboxConnection();
                }
                return _newDropboxConnection;
            }
            set { SetProperty(ref _newDropboxConnection, value); }
        }

        public DelegateCommand AddNewDropboxConnectionCommand
        {
            get
            {
                return _addNewDropboxConnectionCommand ??
                  (_addNewDropboxConnectionCommand = new DelegateCommand(
                      () => {
                          Task.Run(async () =>
                          {
                              AddCredentialResult result = default;
                              await ApplicationCommands.ExecuteFetchDataAsync(async () =>
                              {
                                  result = await StorageHostingManagerService.AddNewCredential(
                                      new DropboxCredential(
                                          InputDropboxConnection.ConnectionName,
                                          InputDropboxConnection.AccessToken
                                      )
                                  );
                              });

                              await ShowAddNewItemResult(result, "Access token is not valid", () =>
                              {
                                  Application.Current.Dispatcher.Invoke(delegate
                                  {
                                      DropboxStorageHostingInfo.Connections.Add(
                                      new StorageHostingConnection()
                                      {
                                          Id = result.NewCredentialId,
                                          Name = InputDropboxConnection.ConnectionName
                                      });
                                      IsAddDropboxConnectionVisible = false;
                                      InputDropboxConnection = new NewDropboxConnection();
                                  });
                              });
                          });
                      },
                      () => {
                          return InputDropboxConnection.IsAllPropertyValid;
                      }
                  )).ObservesProperty(() => InputDropboxConnection.ConnectionName)
                  .ObservesProperty(() => InputDropboxConnection.AccessToken);
            }
        }
        #endregion

        #region GoogleDrive storage hosting
        private StorageHostingInfo _GoogleDriveStorageHostingInfo;
        private NewGoogleDriveConnection _newGoogleDriveConnection;
        private DelegateCommand _addNewGoogleDriveConnectionCommand;
        private DelegateCommand _selectCredentialFileCommand;
        private bool _IsAddGoogleDriveConnectionVisible;

        public StorageHostingInfo GoogleDriveStorageHostingInfo
        {
            get { return _GoogleDriveStorageHostingInfo; }
            set { SetProperty(ref _GoogleDriveStorageHostingInfo, value); }
        }

        public NewGoogleDriveConnection InputGoogleDriveConnection
        {
            get
            {
                if (_newGoogleDriveConnection == null)
                {
                    _newGoogleDriveConnection = new NewGoogleDriveConnection();
                }
                return _newGoogleDriveConnection;
            }
            set { SetProperty(ref _newGoogleDriveConnection, value); }
        }

        public DelegateCommand AddNewGoogleDriveConnectionCommand
        {
            get
            {
                return _addNewGoogleDriveConnectionCommand ??
                  (_addNewGoogleDriveConnectionCommand = new DelegateCommand(
                      () =>
                      {
                          Task.Run(async () =>
                          {
                              AddCredentialResult result = default;
                              await ApplicationCommands.ExecuteFetchDataAsync(async () =>
                              {
                                  result = await StorageHostingManagerService.AddNewCredential(
                                    new GoogleDriveCredentialModel(
                                        InputGoogleDriveConnection.ConnectionName,
                                        InputGoogleDriveConnection.FilePath
                                    )
                                  );
                              });

                              await ShowAddNewItemResult(result, "Credential file is not correct", () =>
                              {
                                  Application.Current.Dispatcher.Invoke(delegate
                                  {
                                      GoogleDriveStorageHostingInfo.Connections.Add(
                                      new StorageHostingConnection()
                                      {
                                          Id = result.NewCredentialId,
                                          Name = InputGoogleDriveConnection.ConnectionName
                                      });
                                      IsAddGoogleDriveConnectionVisible = false;
                                      InputGoogleDriveConnection = new NewGoogleDriveConnection();
                                  });
                              });
                          });
                      },
                      () =>
                      {
                          return InputGoogleDriveConnection.IsAllPropertyValid;
                      }
                  )).ObservesProperty(() => InputGoogleDriveConnection.ConnectionName)
                  .ObservesProperty(() => InputGoogleDriveConnection.FilePath);
            }
        }

        public DelegateCommand SelectCredentialFileCommand
        {
            get
            {
                return _selectCredentialFileCommand ??
                  (_selectCredentialFileCommand = new DelegateCommand(
                      () =>
                      {
                          var filePath = LocalFileManagerService.ChooseFile("Json files (*.json)|*.json");
                          if (filePath != string.Empty)
                          {
                              InputGoogleDriveConnection.FilePath = filePath;
                          }
                      }
                  ));
            }
        }

        public bool IsAddGoogleDriveConnectionVisible
        {
            get { return _IsAddGoogleDriveConnectionVisible; }
            set { SetProperty(ref _IsAddGoogleDriveConnectionVisible, value); }
        }
        #endregion


        private async Task ShowAddNewItemResult(AddCredentialResult result, string messageForIncorrectResult, Action addintionSuccessAction)
        {
            switch (result.ResultStatus)
            {
                case CredentialResultStatus.Incorect:
                    await ToastNotificationService.ShowToast(messageForIncorrectResult, ToastType.Error);
                    break;
                case CredentialResultStatus.Exist:
                    await ToastNotificationService.ShowToast("Connection with this name already exist", ToastType.Warning);
                    break;
                case CredentialResultStatus.Created:
                    addintionSuccessAction();
                    await ToastNotificationService.ShowToast("Storage connection was added", ToastType.Success);
                    break;
                default:
                    throw new NotSupportedException(result.ResultStatus.ToString());
            }
        }

        public CredentialManagerViewModel()
        {
            
        }

        #region Dependency    
        [Dependency]
        public IStorageHostingManagerService StorageHostingManagerService { get; set; }

        [Dependency]
        public IToastNotificationService ToastNotificationService { get; set; }

        [Dependency]
        public ILocalFileManagerService LocalFileManagerService{ get; set; }

        [Dependency]
        public IApplicationCommands ApplicationCommands { get; set; }

        [Dependency]
        public IUserFileManagerService UserFileManagerService { get; set; }

        [Dependency]
        public IAppSettingService AppSettingService { get; set; }
        #endregion

        #region INavigationAware
        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            ApplicationCommands.ExecuteFetchDataAsync(async () =>
            {
                MegaStorageHostingInfo = new StorageHostingInfo(Hosting.Mega);
                DropboxStorageHostingInfo = new StorageHostingInfo(Hosting.Dropbox);
                GoogleDriveStorageHostingInfo = new StorageHostingInfo(Hosting.GoogleDrive);

                var userFiles = await UserFileManagerService.GetAll(false);
                
                MegaStorageHostingInfo.Connections = new ObservableCollection<StorageHostingConnection>(
                    (await StorageHostingManagerService.GetAll(Hosting.Mega))
                        .Select(q => new StorageHostingConnection()
                        {
                            FileCount = userFiles.Count(
                                    u => u.Hostings.Any(f => f.CustomName == q.CustomName && f.HostingType == Hosting.Mega)
                                ),
                            Id = q.Id,
                            Name = q.CustomName
                        }
                    )
                );

                DropboxStorageHostingInfo.Connections = new ObservableCollection<StorageHostingConnection>(
                    (await StorageHostingManagerService.GetAll(Hosting.Dropbox))
                        .Select(q => new StorageHostingConnection()
                        {
                            FileCount = userFiles.Count(
                                    u => u.Hostings.Any(f => f.CustomName == q.CustomName && f.HostingType == Hosting.Dropbox)
                                ),
                            Id = q.Id,
                            Name = q.CustomName
                        }
                    )
                );

                GoogleDriveStorageHostingInfo.Connections = new ObservableCollection<StorageHostingConnection>(
                    (await StorageHostingManagerService.GetAll(Hosting.GoogleDrive))
                        .Select(q => new StorageHostingConnection()
                        {
                            FileCount = userFiles.Count(
                                    u => u.Hostings.Any(f => f.CustomName == q.CustomName && f.HostingType == Hosting.GoogleDrive)
                                ),
                            Id = q.Id,
                            Name = q.CustomName
                        }
                    )
                );
            });
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            // logic isn't needed
        }
        #endregion
    }
}
