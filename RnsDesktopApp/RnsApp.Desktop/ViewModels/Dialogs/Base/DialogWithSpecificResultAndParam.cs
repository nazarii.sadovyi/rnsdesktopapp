﻿using Prism.Mvvm;
using Prism.Services.Dialogs;
using RnsApp.Common;
using System;

namespace RnsApp.Desktop.ViewModels.Dialogs.Base
{
    public class DialogWithSpecificResultAndParam<TParameter, TResult> : BindableBase, IDialogAware
    {
        private TParameter _parameter;
        public TParameter Parameter
        {
            get { return _parameter; }
            set { SetProperty(ref _parameter, value); }
        }

        public string Title { get; set; }

        public event Action<IDialogResult> RequestClose;

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {

        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            if (!parameters.TryGetValue(Const.DialogInputParameterKey, out TParameter p))
            {
                throw new ArgumentException(Const.DialogInputParameterKey);
            }

            Parameter = p;
        }

        protected void CloseDialog(TResult result)
        {
            var dialogParams = new DialogParameters
            {
                { Const.DialogResultParameterKey, result }
            };
            RequestClose.Invoke(new DialogResult(ButtonResult.OK, dialogParams));
        }

        protected void CloseDialog()
        {
            RequestClose.Invoke(new DialogResult(ButtonResult.Cancel));
        }
    }
}
