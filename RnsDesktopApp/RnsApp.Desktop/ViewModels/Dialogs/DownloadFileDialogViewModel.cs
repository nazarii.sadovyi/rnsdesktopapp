﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using RnsApp.Common.Domain;
using RnsApp.Common.Entities;
using RnsApp.Common.Enums;
using RnsApp.Core.Services;
using RnsApp.Desktop.UIModels;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Unity;

namespace RnsApp.Desktop.ViewModels.Dialogs
{
    public class DownloadFileDialogViewModel : BindableBase, IDialogAware
    {
        private UserFile fileToDownload;
        public UserFile FileToDownload
        {
            get { return fileToDownload; }
            set { SetProperty(ref fileToDownload, value); }
        }

        private string _resultMessage;
        public string ResultMessage
        {
            get { return _resultMessage; }
            set { SetProperty(ref _resultMessage, value); }
        }

        private string _totalTime;
        public string TotalTime
        {
            get { return _totalTime; }
            set { SetProperty(ref _totalTime, value); }
        }

        private ObservableCollection<ProgressModel> _progresses;
        public ObservableCollection<ProgressModel> Progresses
        {
            get { return _progresses; }
            set { SetProperty(ref _progresses, value); }
        }

        private async Task InitDownloadFileProccess()
        {
            Progresses = new ObservableCollection<ProgressModel>();

            #region File download process
            var fileDownloadProgress = new ProgressModel()
            {
                Label = "Downloading original file:"
            };

            var downloadProgress = new Progress<DownloadProgressArgs>(args =>
            {
                fileDownloadProgress.UpdateProgress(args.State, args.Value);
            });

            App.Current.Dispatcher.Invoke(() =>
            {
                Progresses.Add(fileDownloadProgress);
            });
            #endregion

            #region Converting from RNS proccess
            var convertFromRnsProgressModel = new ProgressModel()
            {
                Label = $"Converting from RNS to decimal:"
            };
            var convertFromRnsProgress = new Progress<BaseProgressArgs>(args =>
            {
                convertFromRnsProgressModel.UpdateProgress(args.State, args.Value, args.Message);
            });

            App.Current.Dispatcher.Invoke(() =>
            {
                Progresses.Add(convertFromRnsProgressModel);
            });
            #endregion

            #region File parts download progress
            var filePartProgress = new Progress<StorageHostingArgs>();
            
            foreach (var filePartHosting in FileToDownload.Hostings)
            {
                var filePartProgressModel = new ProgressModel()
                {
                    Label = $"Downloading file part using connection - {filePartHosting.HostingType.ToString()}({filePartHosting.CustomName}):"
                };
                filePartProgress.ProgressChanged += (sender, args) =>
                {
                    if (filePartHosting.Id == args.Id)
                    {
                        filePartProgressModel.UpdateProgress(args.State, args.Value, args.Message);
                    }
                };

                App.Current.Dispatcher.Invoke(() =>
                {
                    Progresses.Add(filePartProgressModel);
                });
            }
            #endregion

            var downloadFileProgress = new DownloadFileProgress(
                                            downloadProgress,
                                            convertFromRnsProgress,
                                            filePartProgress);

            var result = await UserFileManagerService.DownloadFile(FileToDownload.FileId, FileToDownload.FullName, downloadFileProgress);
            if (!result.Success)
            {
                ResultMessage = result.ErrorMessage;
                convertFromRnsProgressModel.UpdateProgress(ProgressState.Failed);
                fileDownloadProgress.UpdateProgress(ProgressState.Failed);
            }
            else
            {
                TotalTime = $"Total download time: {result.TotalSpentTime.Minutes} minutes, {result.TotalSpentTime.Seconds} seconds";
                foreach (var progress in Progresses.Where(p => p.ProgressState == ProgressState.Loading))
                {
                    progress.Value = 100;
                    progress.ProgressState = ProgressState.Skiped;
                }
                ResultMessage = "File has been downloaded";
            }
        }

        private DelegateCommand _closeDialogCommand;
        public DelegateCommand CloseDialogCommand
        {
            get
            {
                return _closeDialogCommand ??
                  (_closeDialogCommand = new DelegateCommand(
                      () =>
                      {
                          RequestClose.Invoke(new DialogResult(ButtonResult.Cancel));
                      }
                  ));
            }
        }

        private DelegateCommand _okCloseDialogCommand;
        public DelegateCommand OkCloseDialogCommand
        {
            get
            {
                return _okCloseDialogCommand ??
                  (_okCloseDialogCommand = new DelegateCommand(
                      () =>
                      {
                          RequestClose.Invoke(new DialogResult(ButtonResult.OK));
                      }
                  ));
            }
        }

        #region DialogRegion
        public string Title { get; set; }

        public event Action<IDialogResult> RequestClose;

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            // logic isn't needed
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            Task.Run(async () =>
            {
                var fileId = parameters.GetValue<Guid>("DownloadedFile");
                FileToDownload = await UserFileManagerService.GetById(fileId);
                await InitDownloadFileProccess();
            });
        }
        #endregion

        #region Dependency
        [Dependency]
        public IUserFileManagerService UserFileManagerService { get; set; }
        #endregion
    }


}
