﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using RnsApp.Core.Services;
using System;
using Unity;

namespace RnsApp.Desktop.ViewModels.Dialogs
{
    public class UpdateServerUrlDialogViewModel : BindableBase, IDialogAware
    {
        #region Dependency
        [Dependency]
        public IAppSettingUpdateService AppSettingUpdateService { get; set; }
        #endregion

        #region DialogAware
        public string Title { get; set; }

        public event Action<IDialogResult> RequestClose;

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            // logic isn't needed
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            ServerUrl = AppSettingUpdateService.ServerUrl;
        }
        #endregion

        private string _serverUrl;
        public string ServerUrl
        {
            get { return _serverUrl; }
            set { SetProperty(ref _serverUrl, value); }
        }

        private string _error;
        public string Error
        {
            get { return _error; }
            set { SetProperty(ref _error, value); }
        }

        private DelegateCommand _updateUrlCommand;
        public DelegateCommand UpdateUrlCommand
        {
            get
            {
                return _updateUrlCommand ??
                  (_updateUrlCommand = new DelegateCommand(
                      () =>
                      {
                          bool result = Uri.TryCreate(ServerUrl, UriKind.Absolute, out Uri uriResult)
                            && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

                          if (result)
                          {
                              var error = string.Empty;
                              var updateResult = AppSettingUpdateService.UpdateServerUrl(ServerUrl, ref error);
                              if (!updateResult)
                              {
                                  Error = error;
                                  return;
                              }

                              RequestClose.Invoke(new DialogResult(ButtonResult.OK));
                          }
                          else
                          {
                              Error = $"Url '{ServerUrl}' is incorrect, try another one";
                          }
                      }
                  ));
            }
        }

        private DelegateCommand _closeDialogCommand;
        public DelegateCommand CloseDialogCommand
        {
            get
            {
                return _closeDialogCommand ??
                  (_closeDialogCommand = new DelegateCommand(
                      () =>
                      {
                          RequestClose.Invoke(new DialogResult(ButtonResult.Cancel));
                      }
                  ));
            }
        }
    }
}
