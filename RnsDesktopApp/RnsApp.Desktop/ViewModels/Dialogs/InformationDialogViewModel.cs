﻿using MaterialDesignThemes.Wpf;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using RnsApp.Desktop.UIModels;
using System;
using System.Windows.Media;

namespace RnsApp.Desktop.ViewModels.Dialogs
{
    public class InformationDialogViewModel : BindableBase, IDialogAware
    {
        private DelegateCommand _closeDialogCommand;
        public DelegateCommand CloseDialogCommand
        {
            get
            {
                return _closeDialogCommand ??
                  (_closeDialogCommand = new DelegateCommand(
                      () =>
                      {
                          RequestClose.Invoke(new DialogResult(ButtonResult.OK));
                      }
                  ));
            }
        }

        private string message;

        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private bool shouldShowImage;

        public bool ShouldShowImage
        {
            get { return shouldShowImage; }
            set { SetProperty(ref shouldShowImage, value); }
        }

        private PackIconKind icon;

        public PackIconKind Icon
        {
            get { return icon; }
            set { SetProperty(ref icon, value); }
        }

        private Brush iconForeground;

        public Brush IconForeground
        {
            get { return iconForeground; }
            set { SetProperty(ref iconForeground, value); }
        }

        public string Title { get; set; }

        public event Action<IDialogResult> RequestClose;

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            // logic isn't needed
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            var props = parameters.GetValue<InformationDialogProps>("InformationDialogProps");

            if (props == null)
            {
                throw new ArgumentException("InformationDialogProps");
            }

            Message = props.Message;
            if (props.IsSuccessOrFailed.HasValue)
            {
                ShouldShowImage = true;
                if (props.IsSuccessOrFailed.Value)
                {
                    Icon = PackIconKind.CheckCircleOutline;
                    IconForeground = Brushes.Green;
                }
                else
                {
                    Icon = PackIconKind.Close;
                    IconForeground = Brushes.Red;
                }
            }
            else
            {
                ShouldShowImage = false;
            }
        }
    }
}
