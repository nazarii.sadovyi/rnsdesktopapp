﻿using Prism.Commands;
using RnsApp.Desktop.UIModels;
using RnsApp.Desktop.ViewModels.Dialogs.Base;

namespace RnsApp.Desktop.ViewModels.Dialogs
{
    public class DeleteFileDialogViewModel : DialogWithSpecificResultAndParam<FileInfo, DeleteFileActionKind>
    {
        private DelegateCommand<string> _deleteFileCommand;
        private DelegateCommand _closeDialogCommand;

        public DelegateCommand CloseDialogCommand
        {
            get
            {
                return _closeDialogCommand ??
                  (_closeDialogCommand = new DelegateCommand(
                      () =>
                      {
                          CloseDialog();
                      }
                  ));
            }
        }

        public DelegateCommand<string> DeleteFileCommand
        {
            get
            {
                return _deleteFileCommand ??
                  (_deleteFileCommand = new DelegateCommand<string>(
                      obj =>
                      {
                          if (int.TryParse(obj, out int value))
                          {
                               switch (value)
                               {
                                   case 0:
                                      CloseDialog(DeleteFileActionKind.Local);
                                      break;
                                   case 1:
                                      CloseDialog(DeleteFileActionKind.Server);
                                      break;
                                   case 2:
                                      CloseDialog(DeleteFileActionKind.Both);
                                      break;
                                   default:
                                       break;
                               }
                          }
                      }
                  ));
            }
        }
    }
}
