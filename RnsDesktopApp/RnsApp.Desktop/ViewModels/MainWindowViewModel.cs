﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using RnsApp.Desktop.UIModels;
using RnsApp.Desktop.Utilities;
using RnsApp.ServerNotification;
using System.Linq;
using System.Collections.ObjectModel;
using Unity;
using RnsApp.Desktop.Services;
using RnsApp.WindowsShellCommand;
using System.Windows;
using RnsApp.Core.Services;

namespace RnsApp.Desktop.ViewModels
{
    public class MainWindowViewModel: BindableBase
    {
        private DelegateCommand<string> _switchUserControl;
        private bool _isEnabled;
        private string _fetchDataMessage;
        private string _serverNotificationConnectionState;
        private Toast _toastUIModel;

        public IRegionManager RegionManager { get; set; }

        [Dependency]
        public IApplicationCommands ApplicationCommands { get; set; }

        public DelegateCommand<string> SwitchUserControl
        {
            get
            {
                return _switchUserControl ??
                  (_switchUserControl = new DelegateCommand<string>(
                      obj => {
                          NavigateToPage(obj);
                      }
                  ));
            }
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { SetProperty(ref _isEnabled, value); }
        }
        public string ServerNotificationConnectionState
        {
            get { return _serverNotificationConnectionState; }
            set { SetProperty(ref _serverNotificationConnectionState, value); }
        }
        public string FetchDataMessage
        {
            get { return _fetchDataMessage; }
            set { SetProperty(ref _fetchDataMessage, value); }
        }

        public Toast ToastUIModel
        {
            get { return _toastUIModel; }
            set { SetProperty(ref _toastUIModel, value); }
        }

        public ObservableCollection<MenuItem> MenuItems { get; set; }

        public DelegateCommand<bool?> OpenDialogCommand { get; private set; }

        public MainWindowViewModel(IApplicationCommands applicationCommands,
            IServerNotificationManager serverNotificationManager,
            IToastNotificationCompositeCommand toastNotificationCompositeCommand,
            IRegionManager regionManager,
            IWindowsShellCommandService windowsShellCommandService,
            ICommandLineArgsService commandLineArgsService)
        {
            RegionManager = regionManager;

            serverNotificationManager.OnConnectionStateChange += (sender, args) =>
            {
                ServerNotificationConnectionState = args.ConnectionState.ToString();
            };
            serverNotificationManager.TryToInitConnection();

            applicationCommands.OpenDialogCommand.RegisterCommand(
                new DelegateCommand<bool?>((obj) =>
                {
                    if (obj.HasValue)
                    {
                        IsEnabled = obj.Value;
                        FetchDataMessage = string.Empty;
                    }
                })
                {
                    IsActive = true
                }
            );

            applicationCommands.FetchDataCommand.RegisterCommand(
                new DelegateCommand<FetchDataInfo>((obj) =>
                {
                    IsEnabled = obj.ShowControl;
                    FetchDataMessage = obj.Message;
                })
                {
                    IsActive = true
                }
            );

            toastNotificationCompositeCommand.ShowToastCommand.RegisterCommand(
                new DelegateCommand<Toast>((obj) =>
                {
                    if (ToastUIModel == null)
                    {
                        ToastUIModel = new Toast();
                    }
                    ToastUIModel.Background = obj.Background;
                    ToastUIModel.Icon = obj.Icon;
                    ToastUIModel.IsVisible = obj.IsVisible;
                    ToastUIModel.Text = obj.Text;
                })
                {
                    IsActive = true
                }
            );

            MenuItems = new ObservableCollection<MenuItem>(new MenuItem[] 
            { 
                new MenuItem() { Label = "User files", Number = "1", Image = "FileMultipleOutline", NavigationView = "UserFilesControl"},
                new MenuItem() { Label = "Upload files", Number = "2", Image = "FileUpload", NavigationView = "UploadFileControl"},
                new MenuItem() { Label = "Storage connection", Number = "3", Image = "Nas", NavigationView = "CredentialManager"},
                new MenuItem() { Label = "Settings", Number = "4", Image = "CogOutline", NavigationView = "SettingsControl"},
            });

            if (commandLineArgsService.ArgumentExist(Common.Enums.ComandLineArgKind.FileToUploadPath)) 
            {
                MenuItems.ElementAt(1).IsSelected = true;
            }
            else
            {
                MenuItems.ElementAt(0).IsSelected = true;
            }

            windowsShellCommandService.StartCommandWatcher(param => 
            {
                Application.Current.Dispatcher.Invoke(delegate
                {
                    Application.Current.MainWindow.WindowState = WindowState.Normal;
                    if (!IsEnabled)
                    {
                        var navParam = new NavigationParameters
                        {
                            { "fileToUploadPath", param.FileToUploadPath }
                        };
                        NavigateToPage("UploadFileControl", navParam);
                    }
                });
            });
        }

        public void NavigateToPage(string name, NavigationParameters valuePairs = default)
        {
            foreach (var item in MenuItems)
            {
                item.IsSelected = false;
            }
            var selected = MenuItems.First(x => x.NavigationView == name);
            selected.IsSelected = true;
            RegionManager.RequestNavigate("ContentRegion2", name, valuePairs);
        }
    }
}
