﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Services.Dialogs;
using RnsApp.Core.Services;
using RnsApp.Desktop.Services;
using RnsApp.Desktop.UIModels;
using RnsApp.Desktop.Utilities;
using RnsApp.Desktop.Utilities.Helpers;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Unity;

namespace RnsApp.Desktop.ViewModels
{
    public class UserFilesControlViewModel : BindableBase, INavigationAware
    {
        private DelegateCommand<Guid?> _showFileInfo;
        private DelegateCommand _downloadFileCommand;
        private DelegateCommand _showInFolderCommand;
        private DelegateCommand _deleteFileCommand;
        private FileInfo selectedFile;

        #region Dependency
        [Dependency]
        public IDialogService DialogService { get; set; }
        [Dependency]
        public ILocalFileManagerService LocalFileManagerService { get; set; }
        [Dependency]
        public IApplicationCommands ApplicationCommands { get; set; }
        [Dependency]
        public IUserFileManagerService UserFileManagerService { get; set; }
        [Dependency]
        public IToastNotificationService ToastNotificationService { get; set; }
        [Dependency]
        public IAppSettingService AppSettingService { get; set; }
        #endregion

        public FileInfo SelectedFile
        {
            get { return selectedFile; }
            set { SetProperty(ref selectedFile, value); }
        }

        private ObservableCollection<FileInfo> _userFiles;

        public ObservableCollection<FileInfo> UserFiles
        {
            get { return _userFiles; }
            set { SetProperty(ref _userFiles, value); }
        }

        public DelegateCommand<Guid?> ShowFileInfo
        {
            get
            {
                return _showFileInfo ??
                  (_showFileInfo = new DelegateCommand<Guid?>(
                      obj =>
                      {
                          if (obj.HasValue)
                          {
                              SelectedFile = UserFiles.First(f => f.Id == obj.Value);
                          }
                      }
                  ));
            }
        }

        public DelegateCommand DownloadFileCommand
        {
            get
            {
                return _downloadFileCommand ??
                  (_downloadFileCommand = new DelegateCommand(
                      () =>
                      {
                          var dialogParamater = new DialogParameters
                          {
                              { "DownloadedFile", SelectedFile.Id }
                          };
                          DialogService.ShowDialog("DownloadFileDialog", dialogParamater, r =>
                          {
                              switch (r.Result)
                              {
                                  case ButtonResult.Cancel:
                                      ToastNotificationService.ShowToast("File download was canceled", ToastType.Warning);
                                      break;
                                  case ButtonResult.OK:
                                      break;
                                  default:
                                      throw new NotImplementedException(r.Result.ToString());
                              }
                          });
                      }
                  ));
            }
        }

        public DelegateCommand DeleteFileCommand
        {
            get
            {
                return _deleteFileCommand ??
                  (_deleteFileCommand = new DelegateCommand(
                      () =>
                      {
                          if (!SelectedFile.IsLocal)
                          {
                              ApplicationCommands.ExecuteFetchDataAsync(
                                  async () => {
                                      await RemoveSelectedFileFromServer();
                                  },
                                  $"Removing file '{SelectedFile.Name}' from server"
                              );
                              return;
                          }

                          DialogService.ShowDialogWithSpecificResult<DeleteFileActionKind>(
                              "DeleteFileDialog",
                              SelectedFile,
                              result => 
                              {
                                  switch (result)
                                  {
                                      case DeleteFileActionKind.Local:
                                          LocalFileManagerService.RemoveFile(GetSelectedFileLocalPath());
                                          break;
                                      case DeleteFileActionKind.Server:
                                          ApplicationCommands.ExecuteFetchDataAsync(
                                              async () => {
                                                  await RemoveSelectedFileFromServer();
                                              },
                                              $"Removing file '{SelectedFile.Name}' from server"
                                          );
                                          break;
                                      case DeleteFileActionKind.Both:
                                          ApplicationCommands.ExecuteFetchDataAsync(
                                              async () => {
                                                  await RemoveSelectedFileFromServer();
                                                  LocalFileManagerService.RemoveFile(GetSelectedFileLocalPath());
                                              },
                                              $"Removing file '{SelectedFile.Name}' localy and from server"
                                          );
                                          break;
                                      default:
                                          break;
                                  }
                              }
                          );
                      }
                  ));
            }
        }

        public DelegateCommand ShowInFolderCommand
        {
            get
            {
                return _showInFolderCommand ??
                  (_showInFolderCommand = new DelegateCommand(
                      () =>
                      {
                          LocalFileManagerService.ShowFileInExploer(SelectedFile.Name);
                      }
                  ));
            }
        }

        public UserFilesControlViewModel(IUserFileManagerService userFileManagerService, IApplicationCommands applicationCommands,
            ILocalFileManagerService localFileManagerService)
        {
            FetchUserFiles(userFileManagerService, applicationCommands);
            localFileManagerService.InitLocalFileWatcher();
            localFileManagerService.FileDirectoryWatcher += LocalFileManagerService_FileDirectoryWatcher;
        }

        private void LocalFileManagerService_FileDirectoryWatcher(object sender, Common.EventArguments.FileWatchEventArgs e)
        {
            var userFile = UserFiles.FirstOrDefault(_ => e.FileNames.Contains(_.Name));
            if (userFile == default)
            {
                return;
            }

            switch (e.ChangeType)
            {
                case Common.Enums.FileChangeType.Created:
                    userFile.IsLocal = true;
                    break;
                case Common.Enums.FileChangeType.Deleted:
                    userFile.IsLocal = false;
                    break;
                case Common.Enums.FileChangeType.Renamed:
                    userFile.IsLocal = true;
                    break;
                default:
                    return;
            }
        }

        private void FetchUserFiles(IUserFileManagerService userFileManagerService = null, IApplicationCommands applicationCommands = null)
        {
            var loacalApplicationCommands = applicationCommands ?? ApplicationCommands;
            var localUserFileManagerService = userFileManagerService ?? UserFileManagerService;
            loacalApplicationCommands.ExecuteFetchDataAsync(async () =>
            {
                UserFiles = new ObservableCollection<FileInfo>(
                    (await localUserFileManagerService.GetAll())
                    .Select(f => FileInfo.ToUIFileInfo(f))
                );
                SelectedFile = UserFiles.FirstOrDefault();
            }, "Fetching user files info...");
        }

        private string GetSelectedFileLocalPath()
        {
            return System.IO.Path.Combine(AppSettingService.FilesDirectory, SelectedFile.Name);
        }

        private async Task RemoveSelectedFileFromServer()
        {
            await UserFileManagerService.Remove(SelectedFile.Id);
            UserFiles.Remove(SelectedFile);
            SelectedFile = UserFiles.FirstOrDefault();
        }

        #region Navigation
        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            LocalFileManagerService.InitLocalFileWatcher();
            FetchUserFiles();
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            // logic isn't needed
        }
        #endregion
    }
}
