﻿using NLog;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Services.Dialogs;
using RnsApp.Common.Entities;
using RnsApp.Common.Enums;
using RnsApp.Core.Services;
using RnsApp.Desktop.UIModels;
using RnsApp.Desktop.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Unity;

namespace RnsApp.Desktop.ViewModels
{
    public class UploadFileControlViewModel: BindableBase, INavigationAware
    {
        private static readonly ILogger _Logger = LogManager.GetCurrentClassLogger();
        private ObservableCollection<ProgressModel> _progresses;
        private DelegateCommand _ChooseFile;
        private DelegateCommand _UploadFileCommand;
        private DelegateCommand _clearUploadCommand;
        private UploadFile _UploadFileModel;
        private bool _IsUploading;
        private bool _IsUploadEnd;
        private string _totalTime;
        private string _resultMessage;

        public string ResultMessage
        {
            get { return _resultMessage; }
            set { SetProperty(ref _resultMessage, value); }
        }

        public string TotalTime
        {
            get { return _totalTime; }
            set { SetProperty(ref _totalTime, value); }
        }

        public ObservableCollection<ProgressModel> Progresses
        {
            get { return _progresses; }
            set { SetProperty(ref _progresses, value); }
        }

        public bool IsUploading
        {
            get { return _IsUploading; }
            set { SetProperty(ref _IsUploading, value); }
        }

        public bool IsUploadEnd
        {
            get { return _IsUploadEnd; }
            set { SetProperty(ref _IsUploadEnd, value); }
        }

        public UploadFile UploadFileModel
        {
            get { return _UploadFileModel; }
            set { SetProperty(ref _UploadFileModel, value); }
        }

        public DelegateCommand ChooseFile
        {
            get
            {
                return _ChooseFile ??
                  (_ChooseFile = new DelegateCommand(
                      () =>
                      {
                          string fileLocation = LocalFileManagerService.ChooseFile();
                          SetFileInfo(fileLocation);
                      }
                  ));
            }
        }

        public DelegateCommand ClearUploadCommand
        {
            get
            {
                return _clearUploadCommand ??
                  (_clearUploadCommand = new DelegateCommand(
                      () =>
                      {
                          IsUploadEnd = false;
                          TotalTime = string.Empty;
                          ResultMessage = string.Empty;
                          UploadFileModel.LocalFileLocation = default;
                          UploadFileModel.Name = string.Empty;
                          UploadFileModel.Extension = string.Empty;
                          UploadFileModel.Description = string.Empty;
                          Task.Run(() => LoadStorageConnection());
                          IsUploading = false;
                      }
                  ));
            }
        }

        public DelegateCommand UploadFileCommand
        {
            get
            {
                return _UploadFileCommand ??
                  (_UploadFileCommand = new DelegateCommand(
                      () =>
                      {
                          Task.Run(async () => 
                          {
                              ResultMessage = "Uploading...";
                              IsUploading = true;
                              Progresses = new ObservableCollection<ProgressModel>();

                              var progresses = new UploadFileProgresses(UploadFileProgress(), ConvertFileProgress(), FilePartsProgress());

                              var result = await UserFileManagerService.UploadFile(new Common.Models.UploadFileModel()
                              {
                                  Description = UploadFileModel.Description,
                                  FileName = $"{UploadFileModel.Name}{UploadFileModel.Extension}",
                                  Path = UploadFileModel.LocalFileLocation,
                                  StorageConnections = UploadFileModel.SelectedItems.Select(x => new Common.Models.StorageConnectionModel()
                                  {
                                      Id = new Guid(x.Item.Key),
                                      StorageType = Enum.Parse<Hosting>(x.Item.GroupTitle)
                                  })
                              }, progresses);


                              IsUploadEnd = true;
                              if (result.Success)
                              {
                                  TotalTime = $"Total upload time: {result.SpentTime.Minutes} minutes, {result.SpentTime.Seconds} seconds";
                                  ResultMessage = "File has been uploaded";
                              }
                              else
                              {
                                  ResultMessage = "File wasn't uploaded";
                              }
                          });
                      },
                      () =>
                      {
                          var isAllSelected = UploadFileModel.SelectedItems.All(x => x.Item != null && (x.Item.Key != null));
                          return isAllSelected && !string.IsNullOrEmpty(UploadFileModel.LocalFileLocation);
                      }
                  )).ObservesProperty(() => UploadFileModel.Name)
                  .ObservesProperty(() => UploadFileModel.LocalFileLocation)
                  .ObservesProperty(() => UploadFileModel.RnsPartCount);
            }
        }

        public UploadFileControlViewModel(IApplicationCommands applicationCommands, IStorageHostingManagerService storageHostingManagerService,
            ICommandLineArgsService commandLineArgsService)
        {
            Task.Run(async () =>
            {
                _UploadFileModel = new UploadFile();
                SetFilePathFromCommandLineIfExist(commandLineArgsService);
                _UploadFileModel.PropertyChanged += (object sender, System.ComponentModel.PropertyChangedEventArgs e) => 
                {
                    if (e.PropertyName == "SelectedItemsProperty")
                    {
                        UploadFileCommand.RaiseCanExecuteChanged();
                    }
                };
                _UploadFileModel.RnsPartCount = 4;
                _UploadFileModel.AvailableRnsPartCount = new int[] { 4, 7 };
                await LoadStorageConnection(applicationCommands, storageHostingManagerService);
            });

        }

        private async Task LoadStorageConnection(IApplicationCommands applicationCommands = default, IStorageHostingManagerService storageHostingManagerService = default)
        {
            if (IsUploading)
            {
                return;
            }

            var localApplicationCommands = applicationCommands ?? ApplicationCommands;
            await localApplicationCommands.ExecuteFetchDataAsync(async () =>
            {
                var localStorageHostingManagerService = storageHostingManagerService ?? StorageHostingManagerService;
                var comboBoxGroups = new Collection<ComboBoxGroup>();
                foreach (Hosting storageHosting in (Hosting[])Enum.GetValues(typeof(Hosting)))
                {
                    comboBoxGroups.Add(new ComboBoxGroup()
                    {
                        Caption = storageHosting.ToString(),
                        Image = $"/images/hostingicon/{storageHosting}.png",
                        Items = new List<KeyValuePair<string, string>>(
                            (await localStorageHostingManagerService.GetAll(storageHosting))
                                .Select(x => new KeyValuePair<string, string>(x.Id.ToString(), x.CustomName))
                        )
                    });
                }
                foreach (var selectedItem in UploadFileModel.SelectedItems)
                {
                    selectedItem.Item = null;
                }
                _UploadFileModel.UploadStorageConnections = new GroupedComboBox(comboBoxGroups);
            }, "Fetching storage hosting connections ...");
        }

        private void SetFileInfo(string fileToUploadPath)
        {
            if (!string.IsNullOrEmpty(fileToUploadPath))
            {
                UploadFileModel.LocalFileLocation = fileToUploadPath;
                UploadFileModel.Name = Path.GetFileNameWithoutExtension(fileToUploadPath);
                UploadFileModel.Extension = Path.GetExtension(fileToUploadPath);
            }
        }

        private void SetFilePathFromCommandLineIfExist(ICommandLineArgsService commandLineArgsService)
        {
            if (commandLineArgsService.TryGetArgumentValue(ComandLineArgKind.FileToUploadPath, out string uploadFilePath))
            {
                if (!File.Exists(uploadFilePath))
                {
                    return;
                }

                SetFileInfo(uploadFilePath);
            }
        }

        #region Dependency
        [Dependency]
        public ILocalFileManagerService LocalFileManagerService { get; set; }
        [Dependency]
        public IStorageHostingManagerService StorageHostingManagerService { get; set; }
        [Dependency]
        public IUserFileManagerService UserFileManagerService { get; set; }
        [Dependency]
        public IDialogService DialogService { get; set; }
        [Dependency]
        public IApplicationCommands ApplicationCommands { get; set; }
        #endregion

        #region Navigation
        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            navigationContext.Parameters.TryGetValue("fileToUploadPath", out string fileToUploadPath);
            SetFileInfo(fileToUploadPath);
            Task.Run(() => LoadStorageConnection());
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            // logic isn't needed
        }
        #endregion

        #region UploadProgresses
        private Progress<UploadProgressArgs> UploadFileProgress()
        {
            var fileUploadProgress = new ProgressModel()
            {
                Label = "Uploading file to server:"
            };

            var uploadValue = 0;
            var uploadProgress = new Progress<UploadProgressArgs>(args =>
            {
                if (args.Value != uploadValue)
                {
                    fileUploadProgress.UpdateProgress(args.State, args.Value, args.Message);
                    _Logger.Info($"upload progress {args.Value}");
                    uploadValue = args.Value;
                }
            });

            App.Current.Dispatcher.Invoke(() => 
            {
                Progresses.Add(fileUploadProgress);
            });

            return uploadProgress;
        }

        private Progress<BaseProgressArgs> ConvertFileProgress()
        {
            var fileConvertProgress = new ProgressModel()
            {
                Label = "Conversion from decimal to RNS:",
            };
            var convertProgress = new Progress<BaseProgressArgs>(args =>
            {
                fileConvertProgress.UpdateProgress(args.State, args.Value, args.Message);
            });

            App.Current.Dispatcher.Invoke(() =>
            {
                Progresses.Add(fileConvertProgress);
            });

            return convertProgress;
        }

        private Progress<StorageHostingArgs> FilePartsProgress()
        {
            var filePartUploadProgress = new Progress<StorageHostingArgs>();

            for (int i = 0; i < UploadFileModel.RnsPartCount; i++)
            {
                var partOrder = i;
                var item = UploadFileModel.SelectedItems.ElementAt(i);
                var filePartProgressModel = new ProgressModel()
                {
                    Label = $"Uploading part {partOrder + 1} using connection {item.Item.GroupTitle}({item.Item.Caption}):"
                };
                filePartUploadProgress.ProgressChanged += (sender, args) =>
                {
                    if (args.OrderNumber == partOrder)
                    {
                        filePartProgressModel.UpdateProgress(args.State, args.Value, args.Message);
                    }
                };
                App.Current.Dispatcher.Invoke(() =>
                {
                    Progresses.Add(filePartProgressModel);
                });
            }

            return filePartUploadProgress;
        }
        #endregion
    }
}
