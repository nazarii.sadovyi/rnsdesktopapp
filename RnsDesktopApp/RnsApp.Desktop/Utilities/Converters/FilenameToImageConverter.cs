﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows.Data;

namespace RnsApp.Desktop.Utilities.Converters
{
    public class FilenameToImageConverter : IValueConverter
    {
        private readonly List<string> IconsName = new List<string>
        {
            "accdb", "avi", "bmp", "css", "doc", "docx", "eml", "eps", "fla", "gif", "html", "ind", "ini", "jpg", "jsf", "midi", "mov", "mp", "mpeg", "pdf", "png", "ppt", "pptx", "proj", "psd", "pst", "pub", "rar", "readme", "set", "text", "tiff", "url", "vsd", "wav", "wma", "wmv", "zip"
        };
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var fileName = ((string)value) ?? string.Empty;
            var fileExtension = Path.GetExtension(fileName).Replace(".", "");
            if (IconsName.Contains(fileExtension))
            {
                return $"/images/fileicons/{fileExtension}.png";
            }
            return "/images/fileicons/ini.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
