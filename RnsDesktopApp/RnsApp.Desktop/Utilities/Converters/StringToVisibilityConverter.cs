﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace RnsApp.Desktop.Utilities.Converters
{
    [ValueConversion(typeof(string), typeof(Visibility), ParameterType = typeof(string))]
    public class StringToVisibilityConverter : IValueConverter
    {
        public Visibility TrueValue { get; set; }
        public Visibility FalseValue { get; set; }

        public StringToVisibilityConverter()
        {
            FalseValue = Visibility.Hidden;
            TrueValue = Visibility.Visible;
        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (string)value == (string)parameter ? TrueValue : FalseValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
