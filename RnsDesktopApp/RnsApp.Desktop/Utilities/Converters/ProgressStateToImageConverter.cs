﻿using RnsApp.Common.Enums;
using System;
using System.Globalization;
using System.Windows.Data;

namespace RnsApp.Desktop.Utilities.Converters
{
    [ValueConversion(typeof(ProgressState), typeof(string))]
    public class ProgressStateToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((ProgressState)value)
            {
                case ProgressState.Loading:
                    return "ProgressDownload";
                case ProgressState.Success:
                    return "Done";
                case ProgressState.Failed:
                    return "WarningCircleOutline";
                case ProgressState.Skiped:
                    return "DebugStepOver";
                default:
                    throw new NotImplementedException(((ProgressState)value).ToString());
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}