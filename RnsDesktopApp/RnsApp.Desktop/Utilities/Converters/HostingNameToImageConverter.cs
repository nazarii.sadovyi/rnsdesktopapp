﻿using RnsApp.Common.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace RnsApp.Desktop.Utilities.Converters
{
    public class HostingNameToImageConverter : IValueConverter
    {
        private readonly List<string> IconsName = new List<string>
        {
            "Dropbox", "Mega", "GoogleDrive"
        };
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var hostingName = Enum.GetName(typeof(Hosting), value);
            if (IconsName.Contains(hostingName))
            {
                return $"/images/HostingIcon/{hostingName}.png";
            }
            return "/images/fileicons/ini.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
