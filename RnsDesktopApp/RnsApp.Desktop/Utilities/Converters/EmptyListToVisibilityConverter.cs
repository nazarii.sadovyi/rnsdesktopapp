﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace RnsApp.Desktop.Utilities.Converters
{
    [ValueConversion(typeof(bool), typeof(IEnumerable<object>))]
    public class EmptyListToVisibilityConverter : IValueConverter
    {
        public bool IfListEmptyThenVisibilityHidden { get; set; }
        public EmptyListToVisibilityConverter()
        {
            IfListEmptyThenVisibilityHidden = true;
        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return !IfListEmptyThenVisibilityHidden ? Visibility.Hidden : Visibility.Visible;
            }
            var list = (IEnumerable<object>)value;
            if (!IfListEmptyThenVisibilityHidden)
            {
                return !list.Any() ? Visibility.Visible : Visibility.Hidden;
            }
            return !list.Any() ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
