﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace RnsApp.Desktop.Utilities.Converters
{
    public class IsEqualMultiValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var firstValue = values.ElementAt(0);
            if (values.All(value => value.ToString() == firstValue.ToString()))
            {
                return true;
            }
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
