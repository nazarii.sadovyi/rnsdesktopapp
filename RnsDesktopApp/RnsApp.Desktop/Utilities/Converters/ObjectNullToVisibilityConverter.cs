﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace RnsApp.Desktop.Utilities.Converters
{
    [ValueConversion(typeof(bool), typeof(object))]
    public class ObjectNullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
