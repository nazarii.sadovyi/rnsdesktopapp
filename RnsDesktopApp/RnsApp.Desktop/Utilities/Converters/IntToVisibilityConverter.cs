﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace RnsApp.Desktop.Utilities.Converters
{
    [ValueConversion(typeof(int), typeof(Visibility), ParameterType = typeof(int))]
    public class IntToVisibilityConverter: IValueConverter
    {
        public Visibility TrueValue { get; set; }
        public Visibility FalseValue { get; set; }

        public IntToVisibilityConverter()
        {
            FalseValue = Visibility.Hidden;
            TrueValue = Visibility.Visible;
        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var input = (int)value;
            var param = int.Parse((string)parameter);
            return input == param ? TrueValue : FalseValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
