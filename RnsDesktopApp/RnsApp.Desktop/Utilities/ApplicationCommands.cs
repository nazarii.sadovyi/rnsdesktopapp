﻿using Prism.Commands;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace RnsApp.Desktop.Utilities
{
    public interface IApplicationCommands
    {
        CompositeCommand OpenDialogCommand { get; }
        CompositeCommand FetchDataCommand { get; }
        Task ExecuteFetchDataAsync(Func<Task> action, string message = default, bool showAtLeastSecond = true);
    }

    public class ApplicationCommands : IApplicationCommands
    {
        private readonly CompositeCommand _openDialogCommand = new CompositeCommand(true);
        public CompositeCommand OpenDialogCommand
        {
            get { return _openDialogCommand; }
        }

        private readonly CompositeCommand _fetchDataCommand = new CompositeCommand(true);
        public CompositeCommand FetchDataCommand
        {
            get { return _fetchDataCommand; }
        }

        public async Task ExecuteFetchDataAsync(Func<Task> action, string message = default, bool showAtLeastSecond = true)
        {
            var fetchDataInfo = new FetchDataInfo()
            {
                ShowControl = true,
                Message = string.IsNullOrEmpty(message) ? "Fetching data..." : message
            };

            FetchDataCommand.Execute(fetchDataInfo);

            var mainActionTask = action();
            var oneSecondTask = Task.Delay(1000);

            var tasksToWait = new Collection<Task>()
            {
                mainActionTask
            };

            if (showAtLeastSecond)
            {
                tasksToWait.Add(oneSecondTask);
            }

            await Task.WhenAll(tasksToWait);

            fetchDataInfo.ShowControl = false;
            FetchDataCommand.Execute(fetchDataInfo);
        }
    }

    public class FetchDataInfo
    {
        public string Message { get; set; }
        public bool ShowControl { get; set; }
    }
}
