﻿using Prism.Services.Dialogs;
using RnsApp.Common;
using RnsApp.Desktop.UIModels;
using System;

namespace RnsApp.Desktop.Utilities.Helpers
{
    public static class DialogServiceHelper
    {
        public static void ShowInformationDialog(this IDialogService dialogService, string message, Action afterAction = default, bool? isSuccesOrFailed = default)
        {
            var paramater = new DialogParameters
            {
                {
                    "InformationDialogProps",
                    new InformationDialogProps(message, afterAction, isSuccesOrFailed)
                }
            };
            dialogService.ShowDialog("InformationDialog", paramater, r =>
            {
                switch (r.Result)
                {
                    case ButtonResult.OK:
                        afterAction?.Invoke();
                        break;
                    default:
                        throw new NotImplementedException(r.Result.ToString());
                }
            });
        }

        public static void ShowDialogWithSpecificResult<T>(this IDialogService dialogService,
            string name, object parameter, Action<T> action)
        {
            var parameters = new DialogParameters()
            {
                { Const.DialogInputParameterKey, parameter }
            };

            dialogService.ShowDialog(name, parameters, e => 
            {
                if (e.Result != ButtonResult.OK)
                {
                    return;
                }

                if (e.Parameters.TryGetValue(Const.DialogResultParameterKey, out T result))
                {
                    action.Invoke(result);
                }
            });
        }
    }
}
