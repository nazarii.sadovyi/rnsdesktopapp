﻿using MaterialDesignThemes.Wpf;
using Prism.Mvvm;
using System;
using System.Windows.Media;

namespace RnsApp.Desktop.UIModels
{
    public class Toast : BindableBase
    {
        private Brush _Background;
        private PackIconKind _Icon;
        private string _Text;
        private bool _IsVisible;
        public Brush Background
        {
            get { return _Background; }
            set { SetProperty(ref _Background, value); }
        }
        public PackIconKind Icon
        {
            get { return _Icon; }
            set { SetProperty(ref _Icon, value); }
        }
        public string Text
        {
            get { return _Text; }
            set { SetProperty(ref _Text, value); }
        }
        public bool IsVisible
        {
            get { return _IsVisible; }
            set { SetProperty(ref _IsVisible, value); }
        }

        public Toast(string message = "", ToastType toastType = ToastType.Info)
        {
            Text = message;
            switch (toastType)
            {
                case ToastType.Info:
                    Icon = PackIconKind.Information;
                    Background = Brushes.Gray;
                    break;
                case ToastType.Success:
                    Icon = PackIconKind.CheckboxMarkedCircle;
                    Background = Brushes.Green;
                    break;
                case ToastType.Error:
                    Icon = PackIconKind.CloseCircle;
                    Background = Brushes.Red;
                    break;
                case ToastType.Warning:
                    Icon = PackIconKind.AlertOutline;
                    Background = Brushes.Orange;
                    break;
                default:
                    throw new NotImplementedException(toastType.ToString());
            }
        }
    }

    public enum ToastType
    {
        Info,
        Success,
        Error,
        Warning
    }
}
