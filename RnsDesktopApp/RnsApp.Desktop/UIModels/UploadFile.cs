﻿using Prism.Mvvm;
using System.Collections.ObjectModel;

namespace RnsApp.Desktop.UIModels
{
    public class UploadFile : BindableBase
    {
        private string _LocalFileLocation;

        public string LocalFileLocation
        {
            get { return _LocalFileLocation; }
            set { SetProperty(ref _LocalFileLocation, value); }
        }

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { SetProperty(ref _Name, value); }
        }

        private string _Extension;

        public string Extension
        {
            get { return _Extension; }
            set { SetProperty(ref _Extension, value); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description; }
            set { SetProperty(ref _Description, value); }
        }

        private int _RnsPartCount;

        public int RnsPartCount
        {
            get { return _RnsPartCount; }
            set
            {
                ResizeSelectedItemsCollection(_RnsPartCount, value);
                SetProperty(ref _RnsPartCount, value);
            }
        }

        private int[] _AvailableRnsPartCount;

        public int[] AvailableRnsPartCount
        {
            get { return _AvailableRnsPartCount; }
            set { SetProperty(ref _AvailableRnsPartCount, value); }
        }

        private GroupedComboBox _UploadStorageConnections;

        public GroupedComboBox UploadStorageConnections
        {
            get { return _UploadStorageConnections; }
            set { SetProperty(ref _UploadStorageConnections, value); }
        }

        private ObservableCollection<SelectedStorageHostingConnections> _SelectedItems;

        public ObservableCollection<SelectedStorageHostingConnections> SelectedItems
        {
            get { return _SelectedItems; }
            set { SetProperty(ref _SelectedItems, value); }
        }

        public UploadFile()
        {

        }

        private void ResizeSelectedItemsCollection(int oldCount, int newCount)
        {
            if (SelectedItems == null)
            {
                SelectedItems = new ObservableCollection<SelectedStorageHostingConnections>();
            }

            if (oldCount == newCount)
            {
                return;
            }

            if (newCount > oldCount)
            {
                var itemsToAdd = newCount - oldCount;
                for (int i = 0; i < itemsToAdd; i++)
                {
                    var item = new SelectedStorageHostingConnections()
                    {
                        Title = $"File part storage connection {SelectedItems.Count + 1}:"
                    };
                    item.PropertyChanged += (semder, e) =>
                    {
                        RaisePropertyChanged("SelectedItemsProperty");
                    };
                    SelectedItems.Add(item);
                }
            }
            else
            {
                var itemsToRemove = oldCount - newCount;
                for (int i = 0; i < itemsToRemove; i++)
                {
                    SelectedItems.RemoveAt(SelectedItems.Count - 1);
                }
            }
        }
    }

    public class SelectedStorageHostingConnections: BindableBase
    {
        private ComboBoxItem _Item;
        public ComboBoxItem Item
        {
            get { return _Item; }
            set { SetProperty(ref _Item, value); }
        }

        public string Title { get; set; }

        public SelectedStorageHostingConnections()
        {
            Item = new ComboBoxItem();
        }
    }
}
