﻿namespace RnsApp.Desktop.UIModels
{
    public enum DeleteFileActionKind
    {
        Local,
        Server,
        Both
    }
}
