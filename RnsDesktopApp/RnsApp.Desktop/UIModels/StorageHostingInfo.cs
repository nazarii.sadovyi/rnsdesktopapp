﻿using Prism.Mvvm;
using RnsApp.Common.Enums;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RnsApp.Desktop.UIModels
{
    public class StorageHostingInfo : BindableBase
    {
        private ObservableCollection<StorageHostingConnection> _connections;
        public ObservableCollection<StorageHostingConnection> Connections 
        {
            get { return _connections; }
            set { SetProperty(ref _connections, value); }
        }
        public Hosting HostingKind { get; set; }
        public StorageHostingInfo(Hosting hostingKind, IEnumerable<StorageHostingConnection> connections)
        {
            HostingKind = hostingKind;
            Connections = new ObservableCollection<StorageHostingConnection>(connections);
        }

        public StorageHostingInfo(Hosting hostingKind)
        {
            HostingKind = hostingKind;
        }
    }
}
