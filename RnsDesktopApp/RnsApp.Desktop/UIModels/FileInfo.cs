﻿using Prism.Mvvm;
using RnsApp.Common.Domain;
using RnsApp.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RnsApp.Desktop.UIModels
{
    public class FileInfo: BindableBase
    {
        private bool _isLocal;

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public string Description { get; set; }

        public bool IsLocal
        {
            get { return _isLocal; }
            set { SetProperty(ref _isLocal, value); }
        }

        public string CreateDate { get; set; }
        public IEnumerable<KeyValuePair<Hosting, string>> FilePartLocation { get; set; }

        public static FileInfo ToUIFileInfo(UserFile file)
        {
            return new FileInfo()
            {
                Id = file.FileId,
                Name = file.FullName,
                CreateDate = file.DateCreated.ToString(),
                Description = file.Description,
                Size = file.Size,
                IsLocal = file.IsLocal,
                FilePartLocation = new List<KeyValuePair<Hosting, string>>(
                    file.Hostings.Select(h => new KeyValuePair<Hosting, string>(h.HostingType, $"{h.HostingType.ToString()}: {h.CustomName}")))
            };
        }
    }
}
