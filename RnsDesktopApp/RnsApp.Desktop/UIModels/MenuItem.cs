﻿using Prism.Mvvm;

namespace RnsApp.Desktop.UIModels
{
    public class MenuItem: BindableBase
    {
        private bool isSelected;
        public bool IsSelected
        {
            get { return isSelected; }
            set { SetProperty(ref isSelected, value); }
        }

        public string Label { get; set; }
        public string Image { get; set; }
        public string Number { get; set; }
        public string NavigationView { get; set; }
    }
}
