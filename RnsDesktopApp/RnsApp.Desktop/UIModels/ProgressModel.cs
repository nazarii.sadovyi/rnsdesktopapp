﻿using Prism.Mvvm;
using RnsApp.Common.Enums;
using System.Linq;

namespace RnsApp.Desktop.UIModels
{
	public class ProgressModel: BindableBase
    {
		private readonly int _successProgressValue = 100;
		private string _label;

		public string Label
		{
			get { return _label; }
			set { SetProperty(ref _label, value); }
		}

		private int _value;

		public int Value
		{
			get { return _value; }
			set { SetProperty( ref _value, value); }
		}

		private ProgressState _progressState;

		public ProgressState ProgressState
		{
			get { return _progressState; }
			set { SetProperty(ref _progressState, value); }
		}

		private string _message;

		public string Message
		{
			get { return _message; }
			set { SetProperty(ref _message, value); }
		}

		public void UpdateProgress(ProgressState newState, int value = default, string message = default)
		{
            var skippedStates = new ProgressState[]
            {
                ProgressState.Failed,
				ProgressState.Success
			};

            if (skippedStates.Contains(ProgressState))
            {
				Message = message;
				return;
            }

            ProgressState = newState;
			if (Value != value)
			{
				Value = value;
				if (Value == _successProgressValue)
				{
					ProgressState = ProgressState.Success;
				}
			}

            if (skippedStates.Contains(ProgressState))
            {
                Message = message;
            }
        }
	}
}
