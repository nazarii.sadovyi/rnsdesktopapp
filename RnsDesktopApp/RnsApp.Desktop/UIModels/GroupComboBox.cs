﻿using Prism.Mvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace RnsApp.Desktop.UIModels
{
    public class GroupedComboBox: BindableBase
    {
        private ObservableCollection<ComboBoxItem> _Items;

        public ObservableCollection<ComboBoxItem> Items
        {
            get { return _Items; }
            private set { SetProperty(ref _Items, value); }
        }

        public GroupedComboBox(IEnumerable<ComboBoxGroup> comboBoxGroups)
        {
            Items = new ObservableCollection<ComboBoxItem>();
            foreach (var comboBoxGroup in comboBoxGroups)
            {
                if (!comboBoxGroup.Items.Any())
                {
                    continue;
                }

                Items.Add(new ComboBoxItem()
                {
                    ItemType = ComboBoxItemType.Group,
                    Caption = $"{comboBoxGroup.Caption}:",
                    Image = comboBoxGroup.Image,
                    GroupTitle = string.Empty,
                    Key = string.Empty
                });

                foreach (var item in comboBoxGroup.Items)
                {
                    Items.Add(new ComboBoxItem()
                    {
                        ItemType = ComboBoxItemType.Item,
                        GroupTitle = comboBoxGroup.Caption,
                        Caption = item.Value,
                        Image = null,
                        Key = item.Key
                    });
                }
            }
        }
    }

    public class ComboBoxGroup
    {
        public string Image { get; set; }
        public string Caption { get; set; }
        public IEnumerable<KeyValuePair<string, string>> Items { get; set; }
        public ComboBoxGroup()
        {
            Items = new List<KeyValuePair<string, string>>();
        }
    }

    public class ComboBoxItem
    {
        public string Key { get; set; }
        public string Image { get; set; }
        public string Caption { get; set; }
        public string GroupTitle { get; set; }
        public ComboBoxItemType ItemType { get; set; }
    }

    public enum ComboBoxItemType
    {
        Group,
        Item
    }
}
