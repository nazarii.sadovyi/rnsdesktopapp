﻿using System;

namespace RnsApp.Desktop.UIModels
{
    public class StorageHostingConnection
    {
        public int FileCount { get; set; }
        public string Name { get; set; }
        public Guid Id { get; set; }
    }
}
