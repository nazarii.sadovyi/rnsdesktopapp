﻿using System;

namespace RnsApp.Desktop.UIModels
{
    public class InformationDialogProps
    {
        public string Message { get; private set; }
        public string Title { get; private set; }
        public bool? IsSuccessOrFailed { get; private set; }
        public InformationDialogProps(string message, Action afterCloseAction = default, bool? isSuccessOrFailed = default):
            this(message, string.Empty, afterCloseAction, isSuccessOrFailed) { }

        public InformationDialogProps(string message, string title, Action afterCloseAction = default, bool? isSuccessOrFailed = default)
        {
            Message = message;
            Title = title;
            IsSuccessOrFailed = isSuccessOrFailed;
        }
    }
}
