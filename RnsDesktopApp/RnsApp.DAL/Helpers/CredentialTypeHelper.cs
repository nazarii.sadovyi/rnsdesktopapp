﻿using System;

namespace RnsApp.DAL.Helpers
{
    public static class CredentialTypeHelper
    {
        public static Common.Enums.CredentialType ToDomainCredentialType(this API.Enums.CredentialType credentialType)
        {
            switch (credentialType)
            {
                case API.Enums.CredentialType.Default:
                    return Common.Enums.CredentialType.Default;
                case API.Enums.CredentialType.Custom:
                    return Common.Enums.CredentialType.Custom;
                default:
                    throw new NotImplementedException(credentialType.ToString());
            }
        }
    }
}
