﻿using System;

namespace RnsApp.DAL.Helpers
{
    public static class StorageHostingHelper
    {
        public static API.Enums.Hosting FromDomainToRestAPIHosting(this Common.Enums.Hosting hostingType)
        {
            switch (hostingType)
            {
                case Common.Enums.Hosting.Mega:
                    return API.Enums.Hosting.Mega;
                case Common.Enums.Hosting.GoogleDrive:
                    return API.Enums.Hosting.GoogleDrive;
                case Common.Enums.Hosting.Dropbox:
                    return API.Enums.Hosting.Dropbox;
                default:
                    throw new NotImplementedException(hostingType.ToString());
            }
        }
    }
}
