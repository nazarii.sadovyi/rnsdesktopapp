﻿using RnsApp.API.Core;
using RnsApp.API.Requests;
using RnsApp.API.Responces;
using RnsApp.Common.Domain;
using RnsApp.Common.Enums;
using RnsApp.Common.Models;
using RnsApp.DAL.Helpers;
using RnsApp.DAL.WorkerInterfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RnsApp.DAL.RestAPIWorkers
{
    public class UserFileRestApi : BaseRestApiWorker, IUserFileWorker
    {
        public event EventHandler<int> DownloadFileProccessEvent;

        public UserFileRestApi(IApiClient apiClient) : base(apiClient) { }

        public async Task Delete(Guid fileId)
        {
            var request = new DeleteFileRequest()
            {
                Id = fileId
            };
            await _apiClient.APISender.Send<DeleteFileRequest, string>(request);
        }

        public async Task<bool> Download(Guid fileId, Stream resultFileStream)
        {
            var request = new DownloadFileGetRequest();
            request.Id = fileId;
            var responce = new FileResponce();
            responce.ContentStream = resultFileStream;
            responce.DownloadProgressEvent += DownloadFileProccessEvent;
            var response = await _apiClient.APISender.GetFile(request, responce);
            switch (response)
            {
                case System.Net.HttpStatusCode.OK:
                    return true;
                case System.Net.HttpStatusCode.Conflict:
                    return false;
                case System.Net.HttpStatusCode.NotFound:
                    return false;
                default:
                    throw new NotImplementedException(response.ToString());
            }
        }

        public async Task<IEnumerable<UserFile>> GetAll()
        {
            var request = new UserFilesGetRequest();
            var result = await _apiClient.APISender.Send<UserFilesGetRequest, IEnumerable<RnsApp.API.Entities.UserFile>>(request);

            var userFiles = new List<UserFile>();

            foreach (var userFile in result.ResultData)
            {
                var restData = result.ResultData.First(_ => _.FileId == userFile.FileId);
                var newHostings = new List<StorageHosting>();
                for (int j = 0; j < userFile.Hostings.Count(); j++)
                {
                    var newHostingId = restData.HostingIds.ElementAt(j);
                    var hosting = ToHostingBase(restData.Hostings.ElementAt(j));
                    hosting.Id = newHostingId;
                    newHostings.Add(hosting);
                }
                userFiles.Add(new UserFile()
                {
                    DateCreated = userFile.DateCreated,
                    FileId = userFile.FileId,
                    FullName = userFile.FullName,
                    Description = userFile.Description,
                    Size = userFile.Size,
                    Hostings = newHostings
                });
            }

            return userFiles;
        }

        public async Task Upload(UploadFileModel uploadFileModel, Progress<int> progress = null)
        {
            var request = new UploadFilePostRequest();
            request.Description = uploadFileModel.Description ?? string.Empty;
            request.Hostings = uploadFileModel.StorageConnections
                .Select(x => new API.Entities.HostingCredential()
                {
                    Id = x.Id,
                    Hosting = x.StorageType.FromDomainToRestAPIHosting()
                })
                .ToList();

            request.FileStreamContent = new ProgressableStreamContent(
                new FileStream(uploadFileModel.Path, FileMode.Open),
                progress)
            {
                FileName = uploadFileModel.FileName
            };

            await _apiClient.APISender.Send<UploadFilePostRequest, string>(request);
        }

        private StorageHosting ToHostingBase(string value)
        {
            var separatorIndex = value.IndexOf(':');
            var hostingAsString = value.Substring(0, separatorIndex);
            var hostingCustomName = value.Substring(separatorIndex + 1);
            return new StorageHosting(
                Guid.Empty,
                (Hosting)Enum.Parse(typeof(Hosting), hostingAsString),
                hostingCustomName.Trim());
        }
    }
}
