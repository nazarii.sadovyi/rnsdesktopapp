﻿namespace RnsApp.DAL.RestAPIWorkers
{
    public abstract class BaseRestApiWorker
    {
        protected readonly IApiClient _apiClient;
        protected BaseRestApiWorker(IApiClient apiClient)
        {
            _apiClient = apiClient;
        }
    }
}
