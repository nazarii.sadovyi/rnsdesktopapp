﻿using RnsApp.API.Requests;
using RnsApp.API.Responces;
using RnsApp.Common.Entities;
using RnsApp.DAL.WorkerInterfaces;
using System.Net;
using System.Threading.Tasks;

namespace RnsApp.DAL.RestAPIWorkers
{
    public class AuthorizeRestApiWorker : BaseRestApiWorker, IAuthorizeWorker
    {
        public AuthorizeRestApiWorker(IApiClient apiClient) : base(apiClient) { }

        public async Task Logout()
        {
            await _apiClient.APISender.Send<LogoutGetRequest, string>(new LogoutGetRequest());
        }

        public async Task<LoginResult> TryToLogin(string email, string password)
        {
            var loginResult = new LoginResult();
            var request = new LoginPostRequest() 
            {
                Email = email,
                Password = password,
                RememberMe = true
            };

            var result = await _apiClient.APISender.Send<LoginPostRequest, LoginPostResponce>(request);
            switch (result.StatusCode)
            {
                case HttpStatusCode.OK:
                    loginResult.IsSuccess = result.ResultData.IsSuccess;
                    loginResult.Error = result.ResultData.Error ?? "Email or password is incorrect";
                    break;
                case 0:
                    loginResult.IsSuccess = false;
                    loginResult.Error = "Couldn't connect to server, check is Server Url accessible and try to change it";
                    break;
                default:
                    loginResult.IsSuccess = false;
                    loginResult.Error = "Something went wrong, contact administrator";
                    break;
            }

            return loginResult;
        }

        public async Task<RegisterUserResult> TryToRegister(string fName, string sName, string email, string password)
        {
            var loginResult = new RegisterUserResult();
            var request = new RegisterPostRequest()
            {
                Email = email,
                Password = password,
                FirstName = fName,
                SecondName = sName
            };

            var result = await _apiClient.APISender.Send<RegisterPostRequest, RegisterPostResponce>(request);
            if (result.IsSuccess)
            {
                loginResult.IsSuccess = result.ResultData.IsSuccess;
                loginResult.Errors = result.ResultData.Errors;
            }
            else
            {
                loginResult.IsSuccess = false;
                loginResult.Errors = new string[] { result.ErrorMessage };
            }

            return loginResult;
        }
    }
}
