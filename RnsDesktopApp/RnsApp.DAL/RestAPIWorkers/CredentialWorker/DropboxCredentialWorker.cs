﻿using RnsApp.API.Requests.DropboxCredentialRequests;
using RnsApp.API.Responces;
using RnsApp.Common.Entities;
using RnsApp.Common.Enums;
using RnsApp.Common.Models;
using RnsApp.DAL.Helpers;
using RnsApp.DAL.WorkerInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RnsApp.DAL.RestAPIWorkers.CredentialWorker
{
    public class DropboxCredentialWorker : BaseRestApiWorker, IDropboxCredentialRepository
    {
        public DropboxCredentialWorker(IApiClient apiClient) : base(apiClient) { }
        public async Task<AddCredentialResult> Add(DropboxCredential inputData)
        {
            var addCredentialResult = new AddCredentialResult();
            var request = new DropboxCredentialPostRequest
            {
                Name = inputData.Name,
                AccessToken = inputData.AccessToken
            };

            var result = await _apiClient.APISender.Send<DropboxCredentialPostRequest, DropboxCredentialPostResponse>(request);

            switch (result.StatusCode)
            {
                case HttpStatusCode.BadRequest:
                    addCredentialResult.ResultStatus = CredentialResultStatus.Incorect;
                    break;
                case HttpStatusCode.Conflict:
                    addCredentialResult.ResultStatus = CredentialResultStatus.Exist;
                    break;
                case HttpStatusCode.OK:
                    addCredentialResult.ResultStatus = CredentialResultStatus.Created;
                    addCredentialResult.NewCredentialId = result.ResultData.Id;
                    break;
                default:
                    throw new NotSupportedException(result.StatusCode.ToString());
            }

            return addCredentialResult;
        }

        public async Task<IEnumerable<HostingCredential>> GetAll()
        {
            var request = new DropboxCredentialGetRequest();
            var result = await _apiClient.APISender.Send<DropboxCredentialGetRequest, IEnumerable<API.Entities.HostingCredential>>(request);
            return result.ResultData
                .Select(c => new HostingCredential()
                {
                    Id = c.Id,
                    Name = c.Name,
                    Type = c.Type.ToDomainCredentialType()
                });
        }

        public async Task Remove(Guid id)
        {
            var request = new DropboxCredentialDeleteRequest
            {
                Id = id
            };
            await _apiClient.APISender.Send<DropboxCredentialDeleteRequest, string>(request);
        }
    }
}
