﻿using RnsApp.API.Requests.GoogleDriveCredentialRequests;
using RnsApp.API.Responces;
using RnsApp.Common.Entities;
using RnsApp.Common.Enums;
using RnsApp.Common.Models;
using RnsApp.DAL.Helpers;
using RnsApp.DAL.WorkerInterfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RnsApp.DAL.RestAPIWorkers.CredentialWorker
{
    public class GoogleDriveCredentialWorker : BaseRestApiWorker, IGoogleDriveCredentialRepository
    {
        public GoogleDriveCredentialWorker(IApiClient apiClient) : base(apiClient) { }

        public async Task<AddCredentialResult> Add(GoogleDriveCredentialModel inputData)
        {
            AddCredentialResult addCredentialResult = new AddCredentialResult();

            var request = new GoogleDriveCredentialPostRequest
            {
                Name = inputData.Name,
                CredentialFile = File.OpenRead(inputData.ServiceAccountCredentialFile)
            };

            var result = await _apiClient.APISender.Send<GoogleDriveCredentialPostRequest, GoogleDriveCredentialPostResponse>(request);

            switch (result.StatusCode)
            {
                case HttpStatusCode.BadRequest:
                    addCredentialResult.ResultStatus = CredentialResultStatus.Incorect;
                    break;
                case HttpStatusCode.Conflict:
                    addCredentialResult.ResultStatus = CredentialResultStatus.Exist;
                    break;
                case HttpStatusCode.OK:
                    addCredentialResult.ResultStatus = CredentialResultStatus.Created;
                    addCredentialResult.NewCredentialId = result.ResultData.Id;
                    break;
                default:
                    throw new NotImplementedException(result.StatusCode.ToString());
            }

            return addCredentialResult;
        }

        public async Task<IEnumerable<HostingCredential>> GetAll()
        {
            var request = new GoogleDriveCredentialGetRequest();
            var result = await _apiClient.APISender.Send<GoogleDriveCredentialGetRequest, IEnumerable<API.Entities.HostingCredential>>(request);
            return result.ResultData
                .Select(c => new HostingCredential()
                {
                    Id = c.Id,
                    Name = c.Name,
                    Type = c.Type.ToDomainCredentialType()
                });
        }

        public async Task Remove(Guid id)
        {
            var request = new GoogleDriveCredentialDeleteRequest
            {
                Id = id
            };
            await _apiClient.APISender.Send<GoogleDriveCredentialDeleteRequest, string>(request);
        }
    }
}
