﻿using Prism.Ioc;
using RnsApp.DAL.RestAPIWorkers;
using RnsApp.DAL.RestAPIWorkers.CredentialWorker;
using RnsApp.DAL.WorkerInterfaces;

namespace RnsApp.DAL
{
    public static class DalRegister
    {
        public static void Register(IContainerRegistry container)
        {
            container.RegisterSingleton<IApiClient, ApiClient>();

            container.Register<IMegaCredentialRepository, MegaCredentialWorker>();
            container.Register<IDropboxCredentialRepository, DropboxCredentialWorker>();
            container.Register<IGoogleDriveCredentialRepository, GoogleDriveCredentialWorker>();
            container.Register<IUserFileWorker, UserFileRestApi>();
            container.Register<IAuthorizeWorker, AuthorizeRestApiWorker>();
        }
    }
}
