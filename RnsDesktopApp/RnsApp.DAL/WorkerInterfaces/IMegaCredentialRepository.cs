﻿using RnsApp.Common.Models;

namespace RnsApp.DAL.WorkerInterfaces
{
    public interface IMegaCredentialRepository: ICredentialRepository<MegaCredential>
    {

    }
}
