﻿using RnsApp.Common.Entities;
using System.Threading.Tasks;

namespace RnsApp.DAL.WorkerInterfaces
{
    public interface IAuthorizeWorker
    {
        Task<LoginResult> TryToLogin(string email, string password);
        Task<RegisterUserResult> TryToRegister(string fName, string sName, string email, string password);
        Task Logout();
    }
}
