﻿using RnsApp.Common.Models;

namespace RnsApp.DAL.WorkerInterfaces
{
    public interface IGoogleDriveCredentialRepository: ICredentialRepository<GoogleDriveCredentialModel>
    {

    }
}
