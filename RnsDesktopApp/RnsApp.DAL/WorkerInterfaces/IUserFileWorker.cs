﻿using RnsApp.Common.Domain;
using RnsApp.Common.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RnsApp.DAL.WorkerInterfaces
{
    public interface IUserFileWorker
    {
        Task<IEnumerable<UserFile>> GetAll();
        Task Delete(Guid fileId);
        Task Upload(UploadFileModel uploadFileModel, Progress<int> progress = null);
        Task<bool> Download(Guid fileId, Stream resultFileStream);
        event EventHandler<int> DownloadFileProccessEvent;
    }
}
