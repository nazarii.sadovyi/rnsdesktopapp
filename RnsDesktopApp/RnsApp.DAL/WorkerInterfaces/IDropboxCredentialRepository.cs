﻿using RnsApp.Common.Models;

namespace RnsApp.DAL.WorkerInterfaces
{
    public interface IDropboxCredentialRepository: ICredentialRepository<DropboxCredential>
    {

    }
}
