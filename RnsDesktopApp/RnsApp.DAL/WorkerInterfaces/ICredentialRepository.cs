﻿using RnsApp.Common.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RnsApp.DAL.WorkerInterfaces
{
    public interface ICredentialRepository<T>
    {
        Task<IEnumerable<HostingCredential>> GetAll();
        Task Remove(Guid id);
        Task<AddCredentialResult> Add(T inputData);
    }
}
