﻿using RnsApp.API;
using RnsApp.Configuration;
using System;
using System.Net.Http;

namespace RnsApp.DAL
{
    public interface IApiClient
    {
        IApiSender APISender { get; }
        HttpClient Client { get; }
        HttpClientHandler ClientHandler { get; }

        void UpdateBaseUri(Uri uri);
    }
    public class ApiClient: IApiClient
    {
        private TimeSpan _timeout = TimeSpan.FromMinutes(15);

        public IApiSender APISender { get; private set; }
        public HttpClient Client { get; private set; }
        public HttpClientHandler ClientHandler { get; private set; }

        public ApiClient(IWebHostConfiguration webHostConfiguration)
        {
            ClientHandler = new HttpClientHandler
            {
                UseCookies = true
            };
            Client = new HttpClient(ClientHandler);
            if (!string.IsNullOrEmpty(webHostConfiguration.ServerUrl))
            {
                Client.BaseAddress = new Uri(webHostConfiguration.ServerUrl);
            }
            Client.Timeout = _timeout;
            APISender = new ApiSender(Client);
        }

        public void UpdateBaseUri(Uri uri)
        {
            ClientHandler = new HttpClientHandler
            {
                UseCookies = true
            };
            Client = new HttpClient(ClientHandler)
            {
                Timeout = _timeout,
                BaseAddress = uri
            };
            APISender = new ApiSender(Client);
        }
    }
}
