﻿using RnsApp.WindowsShellCommand;
using SharpShell.Attributes;
using SharpShell.SharpContextMenu;
using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ContextMenuExtension
{
    [ComVisible(true)]
    [COMServerAssociation(AssociationType.AllFiles)]
    public class UploadFileContextMenuCommand : SharpContextMenu
    {
        protected override bool CanShowMenu()
        {
            return true;
        }

        protected override ContextMenuStrip CreateMenu()
        {
            try
            {
                var windowsShellCommandService = new WindowsShellCommandService();

                var menu = new ContextMenuStrip();

                var itemCountLines = new ToolStripMenuItem
                {
                    Text = "Upload via RNS tool",
                    Image = Properties.Resources.cloud_storage_uploading_option
                };

                itemCountLines.Click += (sender, args) =>
                {
                    var selectedItemPath = SelectedItemPaths.First();
                    if (IsAppOpen())
                    {
                        windowsShellCommandService.RaiseCommand(new WindowsCommandParameter()
                        {
                            UpdateTime = DateTime.Now,
                            FileToUploadPath = selectedItemPath
                        });
                    }
                    else
                    {
                        windowsShellCommandService.RunApp($"\"{selectedItemPath}\"");
                    }
                };

                menu.Items.Add(itemCountLines);

                return menu;
            }
            catch (Exception e)
            {
                MessageBox.Show($"Something went wrong, try to reinit RNS context menu. Error message: {e.Message}");
            }

            return null;
        }

        private bool IsAppOpen()
        {
            var processes = Process.GetProcessesByName("RnsApp.Desktop");
            var appProccess = processes.FirstOrDefault();
            return appProccess != default;
        }
    }
}
