﻿using RnsApp.WindowsShellCommand.Enums;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace RnsApp.WindowsShellCommand
{
    public class RegisterWindowsLibraryManager
    {
        private readonly string _currentDirectoryPath;
        private readonly string _regasmPath;
        
        public RegisterWindowsLibraryManager()
        {
            _currentDirectoryPath = Environment.CurrentDirectory;
            _regasmPath = GetRegasmPath();
        }

        public async Task UnRegister(LibraryKind libraryKind)
        {
            await Init(GetLibraryKindPath(libraryKind), false);
        }

        public async Task Register(LibraryKind libraryKind)
        {
            await Init(GetLibraryKindPath(libraryKind));
        }

        private Task Init(string libraryPath, bool register = true)
        {
            if (!File.Exists(libraryPath))
            {
                return Task.CompletedTask;
            }

            var registerOrUnregisterParam = register ? string.Empty : @"/u";

            var process = new Process();
            var startInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = _regasmPath,
                UseShellExecute = true,
                Arguments = $@"/codebase {registerOrUnregisterParam} ""{libraryPath}""",
                Verb = "runas"
            };
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();

            return Task.CompletedTask;
        }

        private string GetLibraryKindPath(LibraryKind libraryKind)
        {
            switch (libraryKind)
            {
                case LibraryKind.UploadFileContextMenu:
                    return $@"{_currentDirectoryPath}\ContextMenuExtension.dll";
                default:
                    throw new NotImplementedException(libraryKind.ToString());
            }
        }

        private string GetRegasmPath()
        {
            if (Environment.Is64BitOperatingSystem)
            {
                return @"c:\Windows\Microsoft.NET\Framework64\v4.0.30319\RegAsm.exe";
            }

            throw new NotSupportedException("OS 32 register windows library manager");
        }
    }
}
