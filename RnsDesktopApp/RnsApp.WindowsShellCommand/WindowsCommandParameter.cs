﻿using System;

namespace RnsApp.WindowsShellCommand
{
    public class WindowsCommandParameter
    {
        public string FileToUploadPath { get; set; }
        public DateTime UpdateTime { get; set; }
    }
}
