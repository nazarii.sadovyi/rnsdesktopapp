﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;

namespace RnsApp.WindowsShellCommand
{
    public interface IWindowsShellCommandService
    {
        void StartCommandWatcher(Action<WindowsCommandParameter> onRaiseAction);
        void StopCommandWather();
        void RaiseCommand(WindowsCommandParameter windowsCommandParameter);
    }
    public class WindowsShellCommandService: IWindowsShellCommandService
    {
        private readonly string _fileToWatchName = "rns_watch_file";
        private readonly string _fileToWatchUpdateName = "rns_watch_file_update";
        private readonly string _fileToWatchPath;
        private readonly string _fileToWatchUpdatePath;
        private readonly string _applicationPath;
        private FileSystemWatcher _fileSystemWatcher;

        private WindowsCommandParameter CommandParameter
        {
            get 
            {
                try
                {
                    return JsonConvert.DeserializeObject<WindowsCommandParameter>(File.ReadAllText(_fileToWatchUpdatePath));
                }
                catch
                {
                    return new WindowsCommandParameter();
                }
            }
            set
            {
                File.Delete(_fileToWatchPath);
                File.Delete(_fileToWatchUpdatePath);
                File.WriteAllText(_fileToWatchPath, JsonConvert.SerializeObject(value));
                File.Move(_fileToWatchPath, _fileToWatchUpdatePath);
            }
        }

        public WindowsShellCommandService()
        {
            _fileToWatchPath = Path.Combine(Path.GetTempPath(), _fileToWatchName);
            _fileToWatchUpdatePath = Path.Combine(Path.GetTempPath(), _fileToWatchUpdateName);
            _applicationPath = Path.Combine(
                Path.GetDirectoryName(new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath),
                "RnsApp.Desktop.exe");
        }

        public void StartCommandWatcher(Action<WindowsCommandParameter> onRaiseAction)
        {
            _fileSystemWatcher = new FileSystemWatcher(Path.GetTempPath())
            {
                Filter = _fileToWatchName
            };
            _fileSystemWatcher.Renamed += (sender, args) => 
            {
                if (args.Name == _fileToWatchUpdateName)
                {
                    onRaiseAction.Invoke(new WindowsCommandParameter() 
                    {
                        FileToUploadPath = CommandParameter.FileToUploadPath,
                        UpdateTime = DateTime.Now
                    });
                }
            };
            _fileSystemWatcher.EnableRaisingEvents = true;
        }

        public void StopCommandWather()
        {
            _fileSystemWatcher.EnableRaisingEvents = false;
        }

        public void RaiseCommand(WindowsCommandParameter windowsCommandParameter)
        {
            CommandParameter = windowsCommandParameter;
        }

        public void RunApp(string args = default)
        {
            Process.Start(new ProcessStartInfo()
            {
                FileName = _applicationPath,
                Arguments = args,
                UseShellExecute = true,
                Verb = "runas"
            });
        }
    }
}
