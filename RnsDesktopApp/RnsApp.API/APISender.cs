﻿using NLog;
using RnsApp.API.Core;
using RnsApp.API.Requests;
using RnsApp.API.Responces;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RnsApp.API
{
    public interface IApiSender
    {
        Task<BaseResponce<TResponce>> Send<TRequest, TResponce>(TRequest request) where TRequest : BaseRequest;
        Task<HttpStatusCode> GetFile<TRequest>(TRequest request, FileResponce fileResponce) where TRequest : BaseRequest;
    }
    public class ApiSender: IApiSender
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        private readonly HttpClient _httpClient;
        public ApiSender(HttpClient client)
        {
            _httpClient = client;
        }

        public async Task<BaseResponce<TResponce>> Send<TRequest, TResponce>(TRequest request) where TRequest : BaseRequest
        {
            var result = new BaseResponce<TResponce>();
            try
            {
                HttpResponseMessage response = null;
                string url = BuildUrl(request);
                switch (request.MethodType)
                {
                    case RequestType.Post:
                        var formData = new MultipartFormDataContent();

                        foreach (var item in RequestBuilder.GetDataMember(request))
                        {
                            formData.Add(new StringContent(item.Value), item.Key);
                        }

                        foreach (var item in RequestBuilder.GetFileStreamParameter(request))
                        {
                            var fileName = Path.GetFileName(item.Name);
                            formData.Add(
                                new StreamContent(item),
                                fileName,
                                fileName
                            );
                        }

                        foreach (var progressableFileStream in RequestBuilder.GetProgressableFileStreamContent(request))
                        {
                            formData.Add(
                                progressableFileStream,
                                progressableFileStream.FileName,
                                progressableFileStream.FileName
                            );
                        }

                        response = _httpClient.PostAsync(url, formData).Result;
                        break;
                    case RequestType.Get:
                        response = await _httpClient.GetAsync(url);
                        break;
                    case RequestType.Delete:
                        response = await _httpClient.DeleteAsync(url);
                        break;
                    default:
                        throw new NotSupportedException(request.MethodType.ToString());
                }
                var postContentStream = await response.Content.ReadAsStreamAsync();
                var responceJson = GetResponseText(postContentStream);
                result.StatusCode = response.StatusCode;
                if (typeof(TResponce) == typeof(string))
                {
                    result.ResultData = (TResponce)(object)responceJson;
                }
                else
                {
                    result.ResultData = JsonConvert.DeserializeObject<TResponce>(responceJson);
                }
                result.IsSuccess = true;
            }
            catch (WebException e)
            {
                if (((HttpWebResponse)e.Response).StatusCode == HttpStatusCode.Unauthorized)
                {
                    _logger.Error(e, "Send request Web exception: {0}", e.Message);
                    throw;
                }
                var resultStream = e.Response.GetResponseStream();
                var resultMessage = GetResponseText(resultStream);
                result.StatusCode = ((HttpWebResponse)e.Response).StatusCode;
                result.ErrorMessage = resultMessage;
                _logger.Error("Send request handled web exception: {0}", resultMessage);
                result.IsSuccess = false;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Send request exception: {0}", e.Message);
                result.ErrorMessage = e.Message;
                result.IsSuccess = false;
            }
            return result;
        }

        public async Task<HttpStatusCode> GetFile<TRequest>(TRequest request, FileResponce fileResponce) where TRequest : BaseRequest
        {
            try
            {
                HttpResponseMessage response = null;
                switch (request.MethodType)
                {
                    case RequestType.Get:
                        var getFileContent = new FormUrlEncodedContent(RequestBuilder.GetQueryParameter(request));
                        var getFileQweryParameter = await getFileContent.ReadAsStringAsync();
                        response = await _httpClient.GetAsync($"{request.Url}?{getFileQweryParameter}", HttpCompletionOption.ResponseHeadersRead);

                        if (response.IsSuccessStatusCode)
                        {
                            await FetchFile(response, fileResponce);
                        }

                        return response.StatusCode;
                    default:
                        throw new NotSupportedException(request.MethodType.ToString());
                }
            }
            catch (WebException e)
            {
                if (((HttpWebResponse)e.Response).StatusCode != HttpStatusCode.Unauthorized)
                {
                    _logger.Error(e, "Send request Web exception: {0}", e.Message);
                }
                var resultStream = e.Response.GetResponseStream();
                var resultMessage = GetResponseText(resultStream);
                _logger.Error("Send request handled web exception: {0}", resultMessage);
                throw;
            }
        }

        private string GetResponseText(Stream dataStream)
        {
            var result = string.Empty;
            using (StreamReader reader = new StreamReader(dataStream))
            {
                result = reader.ReadToEnd();
            }
            return result;
        }

        private string BuildUrl<T>(T request) where T: BaseRequest
        {
            string url = request.Url;
            var encodedContent = new FormUrlEncodedContent(RequestBuilder.GetQueryParameter(request));
            var qweryParameter = encodedContent.ReadAsStringAsync().Result;
            if (qweryParameter != string.Empty)
            {
                url = $"{url}?{qweryParameter}";
            }
            return url;
        }

        private async Task FetchFile(HttpResponseMessage response, FileResponce fileResponce)
        {
            var totalDownloadSize = response.Content.Headers.ContentLength;
            var contentStream = await response.Content.ReadAsStreamAsync();

            var totalBytesRead = 0L;
            var readCount = 0L;
            var buffer = new byte[8192];
            var isMoreToRead = true;

            do
            {
                var bytesRead = await contentStream.ReadAsync(buffer, 0, buffer.Length);
                if (bytesRead == 0)
                {
                    isMoreToRead = false;
                    fileResponce.DownloadProgressEvent?.Invoke(this, (int)((double)totalBytesRead / totalDownloadSize.Value * 100));
                    continue;
                }

                await fileResponce.ContentStream.WriteAsync(buffer, 0, bytesRead);

                totalBytesRead += bytesRead;
                readCount += 1;

                if (readCount % 100 == 0)
                    fileResponce.DownloadProgressEvent?.Invoke(this, (int)((double)totalBytesRead / totalDownloadSize.Value * 100));
            }
            while (isMoreToRead);
        }
    }
}
