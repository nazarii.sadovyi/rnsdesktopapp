﻿using RnsApp.API.Core;

namespace RnsApp.API.Requests.MegaCredentialRequests
{
    public class MegaCredentialGetRequest: BaseRequest
    {
        public MegaCredentialGetRequest()
        {
            Url = "MegaCredential/Get";
            MethodType = RequestType.Get;
        }
    }
}
