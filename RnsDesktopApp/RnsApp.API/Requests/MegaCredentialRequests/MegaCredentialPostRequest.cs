﻿using RnsApp.API.Core;
using System.Runtime.Serialization;

namespace RnsApp.API.Requests.MegaCredentialRequests
{
    public class MegaCredentialPostRequest: BaseRequest
    {
        public MegaCredentialPostRequest()
        {
            Url = "MegaCredential/Post";
            MethodType = RequestType.Post;
        }

        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public string Password { get; set; }
    }
}
