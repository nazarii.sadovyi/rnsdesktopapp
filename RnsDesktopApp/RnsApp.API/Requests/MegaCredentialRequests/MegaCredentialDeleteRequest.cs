﻿using RnsApp.API.Core;
using RnsApp.API.Core.ParameterAttributes;
using System;

namespace RnsApp.API.Requests.MegaCredentialRequests
{
    public class MegaCredentialDeleteRequest: BaseRequest
    {
        public MegaCredentialDeleteRequest()
        {
            Url = "MegaCredential/Delete";
            MethodType = RequestType.Delete;
        }

        [QweryParameter("id")]
        public Guid Id { get; set; }
    }
}
