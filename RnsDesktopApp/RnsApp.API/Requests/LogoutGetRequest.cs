﻿using RnsApp.API.Core;

namespace RnsApp.API.Requests
{
    public class LogoutGetRequest: BaseRequest
    {
        public LogoutGetRequest()
        {
            Url = "Home/Logout";
            MethodType = RequestType.Get;
        }
    }
}
