﻿using RnsApp.API.Core;

namespace RnsApp.API.Requests.DropboxCredentialRequests
{
    public class DropboxCredentialGetRequest: BaseRequest
    {
        public DropboxCredentialGetRequest()
        {
            Url = "DropboxCredential/Get";
            MethodType = RequestType.Get;
        }
    }
}
