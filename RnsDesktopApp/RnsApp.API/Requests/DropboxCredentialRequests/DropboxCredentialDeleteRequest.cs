﻿using RnsApp.API.Core;
using RnsApp.API.Core.ParameterAttributes;
using System;

namespace RnsApp.API.Requests.DropboxCredentialRequests
{
    public class DropboxCredentialDeleteRequest: BaseRequest
    {
        public DropboxCredentialDeleteRequest()
        {
            Url = "DropboxCredential/Delete";
            MethodType = RequestType.Delete;
        }

        [QweryParameter("id")]
        public Guid Id { get; set; }
    }
}
