﻿using RnsApp.API.Core;
using System.Runtime.Serialization;

namespace RnsApp.API.Requests.DropboxCredentialRequests
{
    public class DropboxCredentialPostRequest: BaseRequest
    {
        public DropboxCredentialPostRequest()
        {
            Url = "DropboxCredential/Post";
            MethodType = RequestType.Post;
        }

        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string AccessToken { get; set; }
    }
}
