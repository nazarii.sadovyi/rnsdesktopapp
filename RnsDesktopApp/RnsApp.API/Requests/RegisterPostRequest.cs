﻿using RnsApp.API.Core;
using System.Runtime.Serialization;

namespace RnsApp.API.Requests
{
    public class RegisterPostRequest: BaseRequest
    {
        public RegisterPostRequest()
        {
            Url = "Home/Register";
            MethodType = RequestType.Post;
        }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string SecondName { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Password { get; set; }
    }
}
