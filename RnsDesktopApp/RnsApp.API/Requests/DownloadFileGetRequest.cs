﻿using RnsApp.API.Core;
using RnsApp.API.Core.ParameterAttributes;
using System;

namespace RnsApp.API.Requests
{
    public class DownloadFileGetRequest: BaseRequest
    {
        public DownloadFileGetRequest()
        {
            MethodType = RequestType.Get;
            Url = "api/file";
        }

        [QweryParameter("id")]
        public Guid Id { get; set; }
    }
}
