﻿using RnsApp.API.Core;
using RnsApp.API.Core.ParameterAttributes;
using System;

namespace RnsApp.API.Requests.GoogleDriveCredentialRequests
{
    public class GoogleDriveCredentialDeleteRequest: BaseRequest
    {
        public GoogleDriveCredentialDeleteRequest()
        {
            Url = "GoogleDriveCredential/Delete";
            MethodType = RequestType.Delete;
        }

        [QweryParameter("id")]
        public Guid Id { get; set; }
    }
}
