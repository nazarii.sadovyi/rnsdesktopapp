﻿using RnsApp.API.Core;

namespace RnsApp.API.Requests.GoogleDriveCredentialRequests
{
    public class GoogleDriveCredentialGetRequest : BaseRequest
    {
        public GoogleDriveCredentialGetRequest()
        {
            Url = "GoogleDriveCredential/Get";
            MethodType = RequestType.Get;
        }
    }
}
