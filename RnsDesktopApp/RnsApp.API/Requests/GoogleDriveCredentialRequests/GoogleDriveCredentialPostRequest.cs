﻿using RnsApp.API.Core;
using RnsApp.API.Core.ParameterAttributes;
using System.IO;
using System.Runtime.Serialization;

namespace RnsApp.API.Requests.GoogleDriveCredentialRequests
{
    public class GoogleDriveCredentialPostRequest : BaseRequest
    {
        public GoogleDriveCredentialPostRequest()
        {
            Url = "GoogleDriveCredential/Post";
            MethodType = RequestType.Post;
        }

        [DataMember]
        public string Name { get; set; }
        
        [FileStreamParameter]
        public FileStream CredentialFile { get; set; }
    }
}
