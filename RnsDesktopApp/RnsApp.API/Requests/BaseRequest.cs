﻿using Newtonsoft.Json;
using RnsApp.API.Core;

namespace RnsApp.API.Requests
{
    public class BaseRequest
    {
        [JsonIgnore]
        public string ContentType { get; protected set; } = "application/json";

        [JsonIgnore]
        public RequestType MethodType { get; protected set; } = RequestType.Get;

        [JsonIgnore]
        public string Url { get; protected set; }
    }
}
