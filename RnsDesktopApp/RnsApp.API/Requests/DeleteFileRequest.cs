﻿using RnsApp.API.Core.ParameterAttributes;
using System;

namespace RnsApp.API.Requests
{
    public class DeleteFileRequest: BaseRequest
    {
        public DeleteFileRequest()
        {
            MethodType = Core.RequestType.Delete;
            Url = "api/file";
        }

        [QweryParameter("id")]
        public Guid Id { get; set; }
    }
}
