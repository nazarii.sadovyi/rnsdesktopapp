﻿using Newtonsoft.Json;
using RnsApp.API.Core;
using RnsApp.API.Core.ParameterAttributes;
using RnsApp.API.Entities;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RnsApp.API.Requests
{
    public class UploadFilePostRequest: BaseRequest
    {
        public UploadFilePostRequest()
        {
            Url = "api/file";
            MethodType = RequestType.Post;
        }

        [DataMember]
        public string Description { get; set; }

        [JsonProperty("hosting")]
        public List<HostingCredential> Hostings { get; set; }

        [ProgressableFileStreamParameter]
        public ProgressableStreamContent FileStreamContent { get; set; }
    }
}
