﻿using RnsApp.API.Core;

namespace RnsApp.API.Requests
{
    public class UserFilesGetRequest: BaseRequest
    {
        public UserFilesGetRequest()
        {
            Url = "Home/UserFiles";
            MethodType = RequestType.Get;
        }
    }
}
