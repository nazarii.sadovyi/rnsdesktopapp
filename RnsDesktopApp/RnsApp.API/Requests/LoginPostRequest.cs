﻿using RnsApp.API.Core;
using System.Runtime.Serialization;

namespace RnsApp.API.Requests
{
    public class LoginPostRequest: BaseRequest
    {
        public LoginPostRequest()
        {
            Url = "Home/Login";
            MethodType = RequestType.Post;
        }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public bool RememberMe { get; set; }
    }
}
