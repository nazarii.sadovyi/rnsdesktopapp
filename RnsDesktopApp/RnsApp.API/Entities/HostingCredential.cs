﻿using Newtonsoft.Json;
using RnsApp.API.Enums;
using System;

namespace RnsApp.API.Entities
{
    public class HostingCredential
    {
        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "type")]
        public CredentialType Type { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "hosting")]
        public Hosting Hosting { get; set; }
    }
}
