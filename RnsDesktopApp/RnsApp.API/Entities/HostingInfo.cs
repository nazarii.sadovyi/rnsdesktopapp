﻿using RnsApp.API.Enums;
using System;

namespace RnsApp.API.Entities
{
    public class HostingInfo
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CredentialType CredentialType { get; set; }
        public Hosting Hosting { get; set; }
    }
}
