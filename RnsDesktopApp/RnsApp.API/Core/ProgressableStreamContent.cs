﻿using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace RnsApp.API.Core
{
    public class ProgressableStreamContent : HttpContent
    {
        public string FileName { get; set; }

        private const int defaultBufferSize = 1024 * 1024;

        private readonly Stream content;
        private readonly int bufferSize;
        private readonly long size;
        private bool contentConsumed;
        private readonly IProgress<int> _uploadProgress;

        public ProgressableStreamContent(Stream content, Progress<int> progress) : this(content, progress, defaultBufferSize) { }

        public ProgressableStreamContent(Stream content, Progress<int> progress, int bufferSize)
        {
            if (content == null)
            {
                throw new ArgumentNullException("content");
            }
            if (bufferSize <= 0)
            {
                throw new ArgumentOutOfRangeException("bufferSize");
            }

            this.content = content;
            this.bufferSize = bufferSize;
            _uploadProgress = progress;
            TryComputeLength(out size);
        }

        protected override Task SerializeToStreamAsync(Stream stream, TransportContext context)
        {
            Contract.Assert(stream != null);
            
            PrepareContent();

            return Task.Run(() =>
            {
                var buffer = new byte[bufferSize];
                var uploaded = 0;

                _uploadProgress?.Report(0);

                using (content) while (true)
                {
                    var length = content.Read(buffer, 0, buffer.Length);
                    if (length <= 0) break;

                    var uploadedLength = uploaded += length;
                    _uploadProgress?.Report(ConverToPercent(uploadedLength));

                    stream.Write(buffer, 0, length);
                }

                _uploadProgress?.Report(100);
            });
        }

        protected override bool TryComputeLength(out long length)
        {
            length = content.Length;
            return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                content.Dispose();
            }
            base.Dispose(disposing);
        }

        private void PrepareContent()
        {
            if (contentConsumed)
            {
                // If the content needs to be written to a target stream a 2nd time, then the stream must support
                // seeking (e.g. a FileStream), otherwise the stream can't be copied a second time to a target 
                // stream (e.g. a NetworkStream).
                if (content.CanSeek)
                {
                    content.Position = 0;
                }
                else
                {
                    throw new InvalidOperationException("SR.net_http_content_stream_already_read");
                }
            }

            contentConsumed = true;
        }

        private int ConverToPercent(long currentSize)
        {
            var r = (int)(((double)currentSize) / size * 100);
            return r;
        }
    }
}
