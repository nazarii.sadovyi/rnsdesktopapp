﻿namespace RnsApp.API.Core
{
    public enum RequestType
    {
        Post,
        Get,
        Delete
    }
}
