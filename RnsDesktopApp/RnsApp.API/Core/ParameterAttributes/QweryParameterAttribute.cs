﻿using System;

namespace RnsApp.API.Core.ParameterAttributes
{
    public class QweryParameterAttribute: Attribute
    {
        public string ParameterName { get; set; }
        public QweryParameterAttribute(string parameterName)
        {
            ParameterName = parameterName;
        }
    }
}
