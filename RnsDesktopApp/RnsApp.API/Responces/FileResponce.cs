﻿using System;
using System.IO;

namespace RnsApp.API.Responces
{
    public class FileResponce
    {
        public Stream ContentStream { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public EventHandler<int> DownloadProgressEvent { get; set; }
    }
}
