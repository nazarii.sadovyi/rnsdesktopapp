﻿using System;

namespace RnsApp.API.Responces
{
    public class DropboxCredentialPostResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string AppFolderName { get; set; }
        public string AccessToken { get; set; }
    }
}
