﻿using System.Net;

namespace RnsApp.API.Responces
{
    public class BaseResponce<T>
    {
        public T ResultData { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
