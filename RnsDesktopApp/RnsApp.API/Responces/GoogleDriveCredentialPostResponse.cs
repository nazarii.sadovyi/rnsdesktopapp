﻿using System;

namespace RnsApp.API.Responces
{
    public class GoogleDriveCredentialPostResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string FileKeyName { get; set; }
    }
}
