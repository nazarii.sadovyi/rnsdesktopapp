﻿using System;

namespace RnsApp.API.Responces
{
    public class MegaCredentialPostResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
