﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RnsApp.API.Responces
{
    public class UploadFilePostResponse
    {
        [JsonProperty(PropertyName = "fullName")]
        public string FullName { get; set; }

        [JsonProperty(PropertyName = "dateCreated")]
        public string DateCreated { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "fileId")]
        public string FileId { get; set; }

        [JsonProperty(PropertyName = "image")]
        public string Image { get; set; }

        [JsonProperty(PropertyName = "hostings")]
        public IEnumerable<string> Hostings { get; set; }

        [JsonProperty(PropertyName = "size")]
        public string Size { get; set; }

        [JsonProperty(PropertyName = "RNSConvertTime")]
        public string ConvertTime { get; set; }

        [JsonProperty(PropertyName = "TotalTime")]
        public string TotalTime { get; set; }
    }
}
