﻿using System.Collections.Generic;

namespace RnsApp.API.Responces
{
    public class RegisterPostResponce
    {
        public bool IsSuccess { get; set; }
        public IEnumerable<string> Errors { get; set; }
    }
}
