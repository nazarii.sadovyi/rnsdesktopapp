﻿using RnsApp.API.Entities;
using System.Collections.Generic;

namespace RnsApp.API.Responces
{
    public class UserFilesGetResponce
    {
        public IEnumerable<UserFile> UserFilesInfo { get; set; }
    }
}
