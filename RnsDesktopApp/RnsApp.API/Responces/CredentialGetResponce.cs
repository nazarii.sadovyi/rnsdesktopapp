﻿using RnsApp.API.Entities;
using System.Collections.Generic;

namespace RnsApp.API.Responces
{
    public class CredentialGetResponce
    {
        public IEnumerable<HostingCredential> HostingCredentials { get; set; }
    }
}
