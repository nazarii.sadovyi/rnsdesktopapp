﻿using Newtonsoft.Json;
using RnsApp.API.Core;
using RnsApp.API.Core.ParameterAttributes;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;

namespace RnsApp.API
{
    public static class RequestBuilder
    {
        public static IEnumerable<KeyValuePair<string, string>> GetDataMember<T>(T data)
        {
            ICollection<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            foreach (var prop in typeof(T).GetProperties())
            {
                var dataMemberAttribute = prop.GetCustomAttribute<DataMemberAttribute>();
                if (dataMemberAttribute != null)
                {
                    var propValueAsString = prop.GetValue(data);
                    if (propValueAsString == null)
                    {
                        continue;
                    }
                    result.Add(new KeyValuePair<string, string>(prop.Name, propValueAsString.ToString()));
                    continue;
                }

                var jsonObjectAttribute = prop.GetCustomAttribute<JsonPropertyAttribute>();
                if (jsonObjectAttribute != null)
                {
                    var jsonObjectValue = prop.GetValue(data);
                    if (jsonObjectValue == null)
                    {
                        continue;
                    }
                    result.Add(new KeyValuePair<string, string>(
                        jsonObjectAttribute.PropertyName,
                        JsonConvert.SerializeObject(jsonObjectValue))
                    );
                }
            }
            return result;
        }

        public static IEnumerable<KeyValuePair<string, string>> GetQueryParameter<T>(T data)
        {
            ICollection<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            foreach (var prop in typeof(T).GetProperties())
            {
                var queryAttribute = prop.GetCustomAttribute<QweryParameterAttribute>();
                if (queryAttribute != null)
                {
                    var propValueAsString = prop.GetValue(data).ToString();
                    result.Add(new KeyValuePair<string, string>(queryAttribute.ParameterName, propValueAsString));
                }
            }
            return result;
        }

        public static IEnumerable<FileStream> GetFileStreamParameter<T>(T data)
        {
            ICollection<FileStream> result = new List<FileStream>();
            foreach (var prop in typeof(T).GetProperties())
            {
                var fileStreamAttribute = prop.GetCustomAttribute<FileStreamParameterAttribute>();
                if (fileStreamAttribute != null)
                {
                    var fileStream = (FileStream)prop.GetValue(data);
                    result.Add(fileStream);
                }
            }
            return result;
        }
        
        public static IEnumerable<ProgressableStreamContent> GetProgressableFileStreamContent<T>(T data)
        {
            ICollection<ProgressableStreamContent> result = new List<ProgressableStreamContent>();
            foreach (var prop in typeof(T).GetProperties())
            {
                var fileStreamAttribute = prop.GetCustomAttribute<ProgressableFileStreamParameterAttribute>();
                if (fileStreamAttribute != null)
                {
                    var fileStream = (ProgressableStreamContent)prop.GetValue(data);
                    result.Add(fileStream);
                }
            }
            return result;
        }
    }
}
