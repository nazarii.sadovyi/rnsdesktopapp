﻿namespace RnsApp.Configuration.Entities
{
    public class UserCredential
    {
        public string Email { get; private set; }
        public string Password { get; private set; }

        public UserCredential(string email, string password)
        {
            Email = email;
            Password = password;
        }
    }
}
