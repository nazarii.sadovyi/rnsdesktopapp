﻿using System;

namespace RnsApp.Configuration.Entities
{
    public class FileHashData
    {
        public Guid FileId { get; set; }
        public string Hash { get; set; }
    }
}
