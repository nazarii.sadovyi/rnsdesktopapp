﻿using System;
using System.Collections.Generic;

namespace RnsApp.Configuration.Entities
{
    public class UserFileHashes
    {
        public IEnumerable<FileHashData> Hashes { get; set; }
        public DateTime UpdateTime { get; set; }
    }
}
