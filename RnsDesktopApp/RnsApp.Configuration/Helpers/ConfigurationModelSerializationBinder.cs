﻿using RnsApp.Configuration.Entities;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace RnsApp.Configuration.Helpers
{
    public class ConfigurationModelSerializationBinder : SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            var suportedTypes = new string[] 
            { 
                nameof(UserFileHashes)
            };
            if (suportedTypes.Contains(typeName))
            {
                return null;
            }
            else
            {
                throw new ArgumentException("Unexpected type", nameof(typeName));
            }
        }
    }
}
