﻿namespace RnsApp.Configuration
{
    public interface IWebHostConfiguration
    {
        string ServerUrl { get; set; }
        string CurrentUserEmail { get; set; }
    }

    public class WebHostConfiguration: BaseRegistryConfiguration, IWebHostConfiguration
    {
        public WebHostConfiguration() : base("RnsFileHosting") { }

        public string ServerUrl
        {
            get { return GetPrimitiveValue<string>("ServerUrl"); }
            set { SetPrimitiveValue("ServerUrl", value); }
        }

        public string CurrentUserEmail
        {
            get { return GetPrimitiveValue<string>("CurrentUserEmail"); }
            set { SetPrimitiveValue("CurrentUserEmail", value); }
        }
    }
}
