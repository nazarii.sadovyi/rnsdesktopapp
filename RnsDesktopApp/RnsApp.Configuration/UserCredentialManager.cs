﻿using CredentialManagement;
using RnsApp.Configuration.Entities;

namespace RnsApp.Configuration
{
    public interface IUserCredentialManager
    {
        void SaveCredential(UserCredential userCredential);
        UserCredential GetCredential();
        bool CredentialExist();
        void Delete();
    }
    public class UserCredentialManager : IUserCredentialManager
    {
        private const string _TargetName = "RnsFileHostingDesktopApp";
        public UserCredentialManager()
        {

        }
        public void SaveCredential(UserCredential userCredential)
        {
            using (var cred = new Credential())
            {
                cred.Username = userCredential.Email;
                cred.Password = userCredential.Password;
                cred.Target = _TargetName;
                cred.Type = CredentialType.Generic;
                cred.PersistanceType = PersistanceType.LocalComputer;
                cred.Save();
            }
        }

        public UserCredential GetCredential()
        {
            using (var cred = new Credential())
            {
                cred.Target = _TargetName;
                cred.Load();
                return new UserCredential(cred.Username, cred.Password);
            }
        }

        public bool CredentialExist()
        {
            using (var cred = new Credential())
            {
                cred.Target = _TargetName;
                cred.Load();
                return cred.Exists();
            }
        }

        public void Delete()
        {
            using (var cred = new Credential())
            {
                cred.Target = _TargetName;
                cred.Load();
                cred.Delete();
            }
        }
    }
}
