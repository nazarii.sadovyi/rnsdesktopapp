﻿using Prism.Ioc;

namespace RnsApp.Configuration
{
    public static class ConfigurationDependency
    {
        public static void Register(IContainerRegistry container)
        {
            container.Register<IWebHostConfiguration, WebHostConfiguration>();
            container.Register<IUserCredentialManager, UserCredentialManager>();
            container.Register<ILocalToolConfiguration, LocalToolConfiguration>();
        }
    }
}
