﻿using Microsoft.Win32;
using RnsApp.Configuration.Helpers;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace RnsApp.Configuration
{
    public abstract class BaseRegistryConfiguration
    {
        private readonly RegistryKey _registryKey;
        protected BaseRegistryConfiguration(string subKey, string parrentSubKey = "Software")
        {
            var softwareKey = Registry.CurrentUser.OpenSubKey(parrentSubKey, true);
            if (softwareKey == null)
            {
                softwareKey = Registry.CurrentUser.CreateSubKey(parrentSubKey, true);
            }

            _registryKey = softwareKey.OpenSubKey(subKey, true);
            if (_registryKey == null)
            {
                _registryKey = softwareKey.CreateSubKey(subKey, true);
            }
        }

        /// <typeparam name="T">Suported T: bool, int, long, string</typeparam>
        protected T GetPrimitiveValue<T>(string key)
        {
            try
            {
                var value = (string)_registryKey.GetValue(key);
                object result = null;
                switch (Type.GetTypeCode(typeof(T)))
                {
                    case TypeCode.Boolean:
                        bool boolResult;
                        bool.TryParse(value, out boolResult);
                        result = boolResult;
                        break;
                    case TypeCode.String:
                        result = value;
                        break;
                    case TypeCode.Int32:
                        int intResult;
                        int.TryParse(value, out intResult);
                        result = intResult;
                        break;
                    case TypeCode.Int64:
                        long longResult;
                        long.TryParse(value, out longResult);
                        result = longResult;
                        break;
                    default:
                        throw new NotSupportedException($"Not suported registry type: {typeof(T).FullName}");
                }
                return (T)result;
            }
            catch (Exception e)
            {
                throw new FormatException($"Incorrect value parse of key: {key}", e);
            }

        }

        /// <param name="value">Suported value types: bool, int, long, string</param>
        protected void SetPrimitiveValue(string key, object value)
        {
            _registryKey.SetValue(key, value, RegistryValueKind.String);
        }

        protected void SetSerializedObject(string key, object data)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, data);
                var serializedData = ms.ToArray();
                _registryKey.SetValue(key, serializedData, RegistryValueKind.Binary);
            }
        }

        protected T GetSerializedObject<T>(string key) where T : class
        {
            var result = (byte[])_registryKey.GetValue(key);
            if (result == null)
            {
                return null;
            }
            using (var ms = new MemoryStream(result))
            {
                var formatter = new BinaryFormatter
                {
                    Binder = new ConfigurationModelSerializationBinder()
                };
                return formatter.Deserialize(ms) as T;
            }
        }
    }
}
