﻿using RnsApp.Configuration.Entities;

namespace RnsApp.Configuration
{
    public interface ILocalToolConfiguration
    {
        string FolderToSaveFile { get; set; }
        UserFileHashes FileHashes { get; set; }
        bool IsContextMenuUploadCommandInit { get; set; }
    }
    public class LocalToolConfiguration: BaseRegistryConfiguration, ILocalToolConfiguration
    {
        public LocalToolConfiguration() : base("RnsFileHosting") { }

        public string FolderToSaveFile
        {
            get { return GetPrimitiveValue<string>("FolderToSaveFile"); }
            set { SetPrimitiveValue("FolderToSaveFile", value); }
        }

        public UserFileHashes FileHashes
        {
            get { return GetSerializedObject<UserFileHashes>("FileHashes"); }
            set { SetSerializedObject("FileHashes", value); }
        }

        public bool IsContextMenuUploadCommandInit
        {
            get { return GetPrimitiveValue<bool>("IsContextMenuUploadCommandInit"); }
            set { SetPrimitiveValue("IsContextMenuUploadCommandInit", value); }
        }
    }
}
