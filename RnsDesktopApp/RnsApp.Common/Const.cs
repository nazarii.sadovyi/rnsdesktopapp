﻿namespace RnsApp.Common
{
    public static class Const
    {
        public static readonly string DialogInputParameterKey = "DialogWithSpecificResultInputParameter";
        public static readonly string DialogResultParameterKey = "DialogWithSpecificResultOutputParameter";
    }
}
