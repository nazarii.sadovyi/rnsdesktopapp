﻿using RnsApp.Common.Enums;
using System;

namespace RnsApp.Common.Models
{
    public class StorageConnectionModel
    {
        public Guid Id { get; set; }
        public Hosting StorageType { get; set; }
    }
}
