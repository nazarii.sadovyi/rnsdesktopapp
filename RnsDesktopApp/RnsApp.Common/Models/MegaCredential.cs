﻿namespace RnsApp.Common.Models
{
    public class MegaCredential
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public MegaCredential(string name, string login, string password)
        {
            Name = name;
            Login = login;
            Password = password;
        }
    }
}
