﻿using System.Collections.Generic;

namespace RnsApp.Common.Models
{
    public class UploadFileModel
    {
        public string FileName { get; set; }
        public string Path { get; set; }
        public string Description { get; set; }
        public IEnumerable<StorageConnectionModel> StorageConnections { get; set; }
    }
}
