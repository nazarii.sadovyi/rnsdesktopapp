﻿namespace RnsApp.Common.Models
{
    public class DropboxCredential
    {
        public string Name { get; private set; }
        public string AccessToken { get; private set; }
        public DropboxCredential(string connectionName, string accessToken)
        {
            Name = connectionName;
            AccessToken = accessToken;
        }
    }
}
