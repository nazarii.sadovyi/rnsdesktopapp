﻿namespace RnsApp.Common.Models
{
    public class GoogleDriveCredentialModel
    {
        public string Name { get; private set; }
        public string ServiceAccountCredentialFile { get; private set; }
        public GoogleDriveCredentialModel(string connectionName, string credentialFilePath)
        {
            Name = connectionName;
            ServiceAccountCredentialFile = credentialFilePath;
        }
    }
}
