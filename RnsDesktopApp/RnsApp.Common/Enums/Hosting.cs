﻿namespace RnsApp.Common.Enums
{
    public enum Hosting
    {
        Mega = 1,
        GoogleDrive = 2,
        Dropbox = 3
    }
}
