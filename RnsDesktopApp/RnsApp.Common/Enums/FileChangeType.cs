﻿namespace RnsApp.Common.Enums
{
    public enum FileChangeType
    {
        Created,
        Deleted,
        Changed,
        Renamed
    }
}
