﻿namespace RnsApp.Common.Enums
{
    public enum ServerConnectionState
    {
        NotConnected,
        Connected,
        Connecting,
        Reconnected,
        Failed,
        Closed
    }
}
