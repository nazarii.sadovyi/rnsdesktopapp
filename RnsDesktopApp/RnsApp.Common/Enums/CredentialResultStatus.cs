﻿namespace RnsApp.Common.Enums
{
    public enum CredentialResultStatus
    {
        Incorect, // invalid hosting credential
        Exist, // this credential already exist
        Created // credential was successfully created
    }
}
