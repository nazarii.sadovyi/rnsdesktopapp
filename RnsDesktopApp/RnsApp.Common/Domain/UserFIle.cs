﻿using System;
using System.Collections.Generic;

namespace RnsApp.Common.Domain
{
    public class UserFile
    {
        public string FullName { get; set; }
        public string DateCreated { get; set; }
        public string Description { get; set; }
        public Guid FileId { get; set; }
        public IEnumerable<StorageHosting> Hostings { get; set; }
        public string Size { get; set; }
        public bool IsLocal { get; set; }
    }
}
