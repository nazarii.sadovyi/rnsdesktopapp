﻿using RnsApp.Common.Enums;
using System;

namespace RnsApp.Common.Domain
{
    public class StorageHosting
    {
        public Guid Id { get; set; }
        public string CustomName { get; set; }
        public Hosting HostingType { get; set; }

        public StorageHosting(Guid id, Hosting hostingType, string customName)
        {
            Id = id;
            HostingType = hostingType;
            CustomName = customName;
        }
    }
}
