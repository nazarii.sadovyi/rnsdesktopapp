﻿namespace RnsApp.Common.Entities
{
    public class LoginResult
    {
        public bool IsSuccess { get; set; }
        public string Error { get; set; }
    }
}
