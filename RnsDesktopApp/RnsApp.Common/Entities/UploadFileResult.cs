﻿using System;

namespace RnsApp.Common.Entities
{
    public class UploadFileResult
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public TimeSpan SpentTime { get; set; }
    }
}
