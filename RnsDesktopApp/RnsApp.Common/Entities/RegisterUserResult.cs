﻿using System.Collections.Generic;

namespace RnsApp.Common.Entities
{
    public class RegisterUserResult
    {
        public bool IsSuccess { get; set; }
        public IEnumerable<string> Errors { get; set; }
    }
}
