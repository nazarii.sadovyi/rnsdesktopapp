﻿using System;

namespace RnsApp.Common.Entities
{
    public class UploadFileProgresses
    {
        public IProgress<UploadProgressArgs> UploadProgress { get; private set; }
        public IProgress<BaseProgressArgs> ConvertProgress { get; private set; }
        public IProgress<StorageHostingArgs> StorageHostingProgress { get; private set; }
        public UploadFileProgresses(Progress<UploadProgressArgs> uploadFileProgress,
            Progress<BaseProgressArgs> convertFileProgress,
            Progress<StorageHostingArgs> storageHostingProgress)
        {
            UploadProgress = uploadFileProgress;
            ConvertProgress = convertFileProgress;
            StorageHostingProgress = storageHostingProgress;
        }
    }

    public class StorageHostingArgs: BaseProgressArgs
    {
        public Guid Id { get; set; }
        public int OrderNumber { get; set; }
    }

    public class UploadProgressArgs: BaseProgressArgs
    {
        
    }
}
