﻿using RnsApp.Common.Enums;

namespace RnsApp.Common.Entities
{
    public class BaseProgressArgs
    {
        public ProgressState State { get; set; }
        public int Value { get; set; }
        public string Message { get; set; }
    }
}
