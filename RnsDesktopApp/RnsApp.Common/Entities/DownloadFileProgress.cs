﻿using System;

namespace RnsApp.Common.Entities
{
    public class DownloadFileProgress
    {
        public IProgress<DownloadProgressArgs> DownloadProgress { get; private set; }
        public IProgress<BaseProgressArgs> ConvertProgress { get; private set; }
        public IProgress<StorageHostingArgs> StoarageHostingProgress { get; private set; }
        public DownloadFileProgress(Progress<DownloadProgressArgs> downloadFileProgress,
            Progress<BaseProgressArgs> convertFileProgress,
            Progress<StorageHostingArgs> storageHostingProgress)
        {
            DownloadProgress = downloadFileProgress;
            ConvertProgress = convertFileProgress;
            StoarageHostingProgress = storageHostingProgress;
        }
    }

    public class DownloadProgressArgs : BaseProgressArgs
    {

    }
}
