﻿using RnsApp.Common.Enums;
using System;

namespace RnsApp.Common.Entities
{
    public class HostingCredential
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CredentialType Type { get; set; }
    }
}
