﻿using RnsApp.Common.Enums;
using System;

namespace RnsApp.Common.Entities
{
    public class AddCredentialResult
    {
        public CredentialResultStatus ResultStatus { get; set; }
        public Guid NewCredentialId { get; set; }
    }
}
