﻿using System;

namespace RnsApp.Common.Entities
{
    public class DownloadFileResult
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public TimeSpan TotalSpentTime { get; set; }
    }
}
