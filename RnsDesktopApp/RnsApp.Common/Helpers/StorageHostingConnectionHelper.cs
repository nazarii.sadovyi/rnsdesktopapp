﻿using RnsApp.Common.Domain;
using RnsApp.Common.Entities;
using RnsApp.Common.Enums;

namespace RnsApp.Common.Helpers
{
    public static class StorageHostingConnectionHelper
    {
        public static StorageHosting ToDomainStorageHosting(this HostingCredential hostingCredential, Hosting hostingKind)
        {
            return new StorageHosting(
                hostingCredential.Id,
                hostingKind,
                hostingCredential.Name);
        }
    }
}
