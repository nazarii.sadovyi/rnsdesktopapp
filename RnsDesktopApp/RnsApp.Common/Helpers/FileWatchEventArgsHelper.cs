﻿using RnsApp.Common.Enums;
using System.IO;

namespace RnsApp.Common.Helpers
{
    public static class FileWatchEventArgsHelper
    {
        public static FileChangeType ToFileChangeType(this WatcherChangeTypes watcherChangeType)
        {
            switch (watcherChangeType)
            {
                case WatcherChangeTypes.Changed:
                    return FileChangeType.Changed;
                case WatcherChangeTypes.Created:
                    return FileChangeType.Created;
                case WatcherChangeTypes.Deleted:
                    return FileChangeType.Deleted;
                case WatcherChangeTypes.Renamed:
                    return FileChangeType.Renamed;
                default:
                    return FileChangeType.Changed;
            }
        }
    }
}
