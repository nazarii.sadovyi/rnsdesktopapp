﻿using RnsApp.Common.Enums;
using System;

namespace RnsApp.Common.EventArguments
{
    public class FileWatchEventArgs: EventArgs
    {
        public FileChangeType ChangeType { get; set; }
        public string[] FileNames { get; set; }
    }
}
