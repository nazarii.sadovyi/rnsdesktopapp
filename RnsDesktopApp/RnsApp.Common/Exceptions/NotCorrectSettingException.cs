﻿using System;
using System.Runtime.Serialization;

namespace RnsApp.Common.Exceptions
{
    [Serializable]
    public class NotCorrectSettingException: Exception
    {
        public string ErrorMessage { get; private set; }
        public string SettingName { get; private set; }
        public NotCorrectSettingException(string settingName, string errorMessage = "")
        {
            ErrorMessage = errorMessage;
            SettingName = settingName;
        }

        protected NotCorrectSettingException(SerializationInfo info, StreamingContext context)
            : base (info, context) { }
    }
}
