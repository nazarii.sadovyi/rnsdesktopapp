﻿using Microsoft.AspNetCore.SignalR.Client;
using NLog;
using RnsApp.Common.Enums;
using RnsApp.ServerNotification.EventArgsEntities;
using System;

namespace RnsApp.ServerNotification
{
    public class UploadFileNotification : IUploadFileNotification
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        public event EventHandler<UploadFilePartsEventArgs> FilePartsProgresEvent;
        public event EventHandler<ConvertFileEventArgs> ConvertFromDecimalProgressEvent;
        public event EventHandler<UploadFilePartCrashArgs> PartUploadFailedEvent;

        public UploadFileNotification(IServerNotificationManager serverNotificationManager)
        {
            serverNotificationManager.HubConnection?.On<Guid, string, int, int, long>("UploadStatus", UploadStatus);
            serverNotificationManager.HubConnection?.On<Guid, int, string>("UploadFilePartCrashInfo", UploadFilePartCrashInfo);

        }

        private void UploadFilePartCrashInfo(Guid fileId, int filePartId, string message)
        {
            _logger.Error("File part {0} upload crashed - {1}", filePartId, message);
            try
            {
                PartUploadFailedEvent?.Invoke(this, new UploadFilePartCrashArgs()
                {
                    PartOrder = filePartId,
                    ErrorMessage = message
                });
            }
            catch (Exception e)
            {
                _logger.Error(e, $"File part {filePartId} upload crashed event error");
                throw;
            }
        }

        private void UploadStatus(Guid fileId, string message, int value, int storageHostingOrder, long ticks)
        {
            try
            {
                // Message template "Converting to RNS..."
                if (message[0] == 'C')
                {
                    _logger.Info("Converting to RNS - {0}%", value);
                    ConvertFromDecimalProgressEvent?.Invoke(this, new ConvertFileEventArgs()
                    {
                        ProgressPercent = value,
                        SpentTime = new TimeSpan(ticks)
                    });
                    return;
                }

                // Message template "Upload to {hosting type}: {hosting cutom name} ..."
                var hostingTypeAsStringEnd = message.IndexOf(":");
                var hostingTypeAsString = message.Substring(10, hostingTypeAsStringEnd - 10);
                var hosting = (Hosting)Enum.Parse(typeof(Hosting), hostingTypeAsString);

                // Value is comming in range from 0 to 25 for zero hosting, then from 25 to 50 for first, etc"
                int validValue = value - 25 * storageHostingOrder;
                var valueInPercent = validValue * 100 / 25;

                _logger.Info("Uploading file parts {0} - {1}%", storageHostingOrder, valueInPercent);
                FilePartsProgresEvent?.Invoke(this, new UploadFilePartsEventArgs()
                {
                    HostingType = hosting,
                    ProgressPercent = valueInPercent,
                    FilePartOrder = storageHostingOrder,
                    SpentTime = new TimeSpan(ticks)
                });
            }
            catch (Exception e)
            {
                _logger.Error(e, "File parts Upload State event error");
                throw;
            }
        }
    }
}
