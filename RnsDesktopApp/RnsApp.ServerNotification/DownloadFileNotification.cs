﻿using Microsoft.AspNetCore.SignalR.Client;
using NLog;
using RnsApp.Common.Enums;
using RnsApp.ServerNotification.EventArgsEntities;
using System;

namespace RnsApp.ServerNotification
{

    public class DownloadFileNotification: IDownloadFileNotification
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        public event EventHandler<DownloadFilePartsEventArgs> FilePartsProgresEvent;
        public event EventHandler<ConvertFileEventArgs> ConvertFromRNSProgressEvent;
        public event EventHandler<FilePartCrashArgs> PartDownloadFailedEvent;

        public DownloadFileNotification(IServerNotificationManager serverNotificationManager)
        {
            serverNotificationManager.HubConnection?.On<Guid, string, int, Guid, long>("DownloadStatus", DownloadStatus);
            serverNotificationManager.HubConnection?.On<Guid, Guid, string>("FilePartCrashInfo", FilePartCrashInfo);
            serverNotificationManager.HubConnection?.On<string>("TotalTime", TotalTime);
            serverNotificationManager.HubConnection?.On<string>("ConvertTime", ConvertTime);
        }

        private void DownloadStatus(Guid fileId, string message, int value, Guid storageHostingId, long ticks)
        {
            try
            {
                _logger.Info($"{fileId},{message}, {value}, {storageHostingId}, {ticks}");

                if (storageHostingId == default)
                {
                    ConvertFromRNSProgressEvent?.Invoke(this, new ConvertFileEventArgs()
                    {
                        ProgressPercent = value,
                        SpentTime = new TimeSpan(ticks)
                    });
                    return;
                }

                // Message template "Downloading from {hosting type} ..."
                var hostingTypeAsStringEnd = message.IndexOf(" ...");
                var hostingTypeAsString = message.Substring(17, hostingTypeAsStringEnd - 17);
                var hosting = (Hosting)Enum.Parse(typeof(Hosting), hostingTypeAsString);

                // Value is comming in range from 0 to 25 for first hosting, then from 25 to 50, etc"
                int validValue = value;
                while (validValue > 25)
                {
                    validValue -= 25;
                }
                var valueInPercent = validValue * 100 / 25;

                _logger.Info("Downloading file parts status update: {0} - {1}", hostingTypeAsString, valueInPercent);
                FilePartsProgresEvent?.Invoke(this, new DownloadFilePartsEventArgs()
                {
                    HostingType = hosting,
                    ProgressPercent = valueInPercent,
                    FilePartId = storageHostingId,
                    SpentTime = new TimeSpan(ticks)
                });
            }
            catch (Exception e)
            {
                _logger.Error(e, "File parts Download State event error");
                throw;
            }
        }

        private void FilePartCrashInfo(Guid fileId, Guid filePartId, string message)
        {
            _logger.Error("File part {0} download crashed - {1}", filePartId, message);
            try
            {
                PartDownloadFailedEvent?.Invoke(this, new FilePartCrashArgs()
                {
                    FilePartId = filePartId,
                    ErrorMessage = message
                });
            }
            catch (Exception e)
            {
                _logger.Error(e, $"File part {filePartId} download crashed event error");
                throw;
            }
        }

        private void TotalTime(string time)
        {
            // logic isn't needed
        }

        private void ConvertTime(string time)
        {
            // logic isn't needed
        }
    }
}
