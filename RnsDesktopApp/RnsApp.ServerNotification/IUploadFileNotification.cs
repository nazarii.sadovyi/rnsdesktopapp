﻿using RnsApp.ServerNotification.EventArgsEntities;
using System;

namespace RnsApp.ServerNotification
{
    public interface IUploadFileNotification
    {
        event EventHandler<UploadFilePartsEventArgs> FilePartsProgresEvent;
        event EventHandler<ConvertFileEventArgs> ConvertFromDecimalProgressEvent;
        event EventHandler<UploadFilePartCrashArgs> PartUploadFailedEvent;
    }
}
