﻿using RnsApp.Common.Enums;
using System;

namespace RnsApp.ServerNotification.EventArgsEntities
{
    public class ServerConnectionStatusEventArgs: EventArgs
    {
        public ServerConnectionState ConnectionState { get; set; }
        public string Message { get; set; }
    }
}
