﻿using System;

namespace RnsApp.ServerNotification.EventArgsEntities
{
    public class ConvertFileEventArgs: EventArgs
    {
        public int ProgressPercent { get; set; }
        public TimeSpan SpentTime { get; set; }
    }
}
