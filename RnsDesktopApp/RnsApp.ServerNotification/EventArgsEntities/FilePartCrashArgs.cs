﻿using System;

namespace RnsApp.ServerNotification.EventArgsEntities
{ 
    public class FilePartCrashArgs
    {
        public string ErrorMessage { get; set; }
        public Guid FilePartId { get; set; }
    }
}
