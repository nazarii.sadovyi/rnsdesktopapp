﻿namespace RnsApp.ServerNotification.EventArgsEntities
{ 
    public class UploadFilePartCrashArgs
    {
        public string ErrorMessage { get; set; }
        public int PartOrder { get; set; }
    }
}
