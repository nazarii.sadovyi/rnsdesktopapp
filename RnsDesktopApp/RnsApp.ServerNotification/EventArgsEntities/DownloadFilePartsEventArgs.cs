﻿using RnsApp.Common.Enums;
using System;

namespace RnsApp.ServerNotification.EventArgsEntities
{
    public class DownloadFilePartsEventArgs: EventArgs
    {
        public Guid FilePartId { get; set; }
        public Hosting HostingType { get; set; }
        public int ProgressPercent { get; set; }
        public TimeSpan SpentTime { get; set; }
    }
}
