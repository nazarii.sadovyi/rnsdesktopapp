﻿using Microsoft.AspNetCore.SignalR.Client;
using NLog;
using RnsApp.Common.Enums;
using RnsApp.Configuration;
using RnsApp.DAL;
using RnsApp.ServerNotification.EventArgsEntities;
using System;
using System.IO;
using System.Threading.Tasks;

namespace RnsApp.ServerNotification
{
    public interface IServerNotificationManager
    {
        event EventHandler<ServerConnectionStatusEventArgs> OnConnectionStateChange;
        HubConnection HubConnection { get; }
        Task<bool> TryToInitConnection();
    }
    public class ServerNotificationManager : IServerNotificationManager
    {
        public event EventHandler<ServerConnectionStatusEventArgs> OnConnectionStateChange;
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        private readonly string ConnectionUrl = "chat";
        public HubConnection HubConnection { get; private set; }
        public ServerNotificationManager(IWebHostConfiguration webHostConfiguration, IApiClient apiClient)
        {
            try
            {
                HubConnection = new HubConnectionBuilder()
                .WithUrl(Path.Combine(webHostConfiguration.ServerUrl, ConnectionUrl), option =>
                {
                    option.Cookies = apiClient.ClientHandler.CookieContainer;
                })
                .Build();
                HubConnection.ServerTimeout = TimeSpan.FromDays(1);
                HubConnection.KeepAliveInterval = TimeSpan.FromDays(1);
                HubConnection.Closed += _Connection_Closed;
                HubConnection.Reconnecting += _Connection_Reconnecting;
                HubConnection.Reconnected += _Connection_Reconnected;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Can't init websocket connection due to exception");
            }
        }

        public async Task<bool> TryToInitConnection()
        {
            try
            {
                _logger.Info("Trying to start web socket connection");
                OnConnectionStateChange?.Invoke(
                    this,
                    new ServerConnectionStatusEventArgs()
                    {
                        ConnectionState = ServerConnectionState.Connecting,
                    }
                );
                if (HubConnection != null)
                {
                    await HubConnection.StartAsync();
                }
                OnConnectionStateChange?.Invoke(
                    this,
                    new ServerConnectionStatusEventArgs()
                    {
                        ConnectionState = ServerConnectionState.Connected,
                    }
                );
                _logger.Info("Web socket connection was successfully started");
            }
            catch (Exception e)
            {
                _logger.Error(e, "Can't start websocket connection due to exception");
                return false;
            }
            return true;
        }

        private Task _Connection_Reconnected(string arg)
        {
            _logger.Warn("Reconnection to web server was invoked");
            OnConnectionStateChange?.Invoke(
                this,
                new ServerConnectionStatusEventArgs()
                {
                    ConnectionState = ServerConnectionState.Reconnected,
                    Message = arg
                }
            );
            return Task.CompletedTask;
        }
        private async Task _Connection_Closed(Exception arg)
        {
            _logger.Warn(arg, "Web server connection was closed");
            OnConnectionStateChange?.Invoke(
                this,
                new ServerConnectionStatusEventArgs()
                {
                    ConnectionState = ServerConnectionState.Closed,
                    Message = arg.Message
                }
            );

            await TryToInitConnection();
        }
        private Task _Connection_Reconnecting(Exception arg)
        {
            _logger.Warn("Trying to reconnect");
            OnConnectionStateChange?.Invoke(
                this,
                new ServerConnectionStatusEventArgs()
                {
                    ConnectionState = ServerConnectionState.Connecting,
                    Message = arg.Message
                }
            );
            return Task.CompletedTask;
        }
    }
}
