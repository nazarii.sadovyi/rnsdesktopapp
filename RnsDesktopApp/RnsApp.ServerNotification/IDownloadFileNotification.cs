﻿using RnsApp.ServerNotification.EventArgsEntities;
using System;

namespace RnsApp.ServerNotification
{
    public interface IDownloadFileNotification
    {
        event EventHandler<DownloadFilePartsEventArgs> FilePartsProgresEvent;
        event EventHandler<ConvertFileEventArgs> ConvertFromRNSProgressEvent;
        event EventHandler<FilePartCrashArgs> PartDownloadFailedEvent;
    }
}
