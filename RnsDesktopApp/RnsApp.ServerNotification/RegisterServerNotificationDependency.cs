﻿using Prism.Ioc;

namespace RnsApp.ServerNotification
{
    public static class RegisterServerNotificationDependency
    {
        public static void Register(IContainerRegistry container)
        {
            container.RegisterSingleton<IServerNotificationManager, ServerNotificationManager>();
            container.RegisterSingleton<IDownloadFileNotification, DownloadFileNotification>();
            container.RegisterSingleton<IUploadFileNotification, UploadFileNotification>();
        }
    }
}
